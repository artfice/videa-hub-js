declare module 'async-eventemitter' {
    var _q: any;
    export = _q;
}

declare module 'check-types' {
    var _q: any;
    export = _q;
}

declare module 'event-stream' {
    var _q: any;
    export = _q;
}

declare module 'json-schema-build' {
    var _q: any;
    export = _q;
}

declare module 'node-uuid' {
    var _q: any;
    export = _q;
}

declare module 'pmx' {
    var _q: any;
    export = _q;
}

declare module 'streamifier' {
    var _q: any;
    export = _q;
}

declare module 'uplynk-api' {
    var _q: any;
    export = _q;
}

declare module 'winston-logentries' {
    var _q: any;
    export = _q;
}

declare module 'pow-mongodb-fixtures' {
    var _q: any;
    export = _q;    
}