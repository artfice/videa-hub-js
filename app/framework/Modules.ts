/**
 * Temporary file to list the modules we have running on videa while we wait for the framework to be in place.
 * Modules are: appstudio, cms and core.
 * The modules list can be used by services like WorkerService to load worker handlers from all modules.
 */

var modules = [
    {
        id: "core",
        name: "Core"
    },
    {
        id: "appstudio",
        name: "App Studio"
    },
    {
        id: "cms",
        name: "CMS"
    }
];

export = modules;
