module.exports = {
    "BaseDTO": {
        "properties": {
            "id": {
                "type": "string"
            },
            "createdDate": {
                "type": "string",
                "format": "dateTime"
            },
            "modifiedDate": {
                "type": "string",
                "format": "dateTime"
            }
        }
    }
};