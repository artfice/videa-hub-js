module.exports = {
	"MappingDefinition": {
		"properties": {
			"sourceField": {
				"type": "string"
			},
			"targetField" : {
				"type" : "string"
			},
			"defaultValue" : {
				"$ref": "#/definitions/MappingDefaultValue"
			}
		}
	}
};
