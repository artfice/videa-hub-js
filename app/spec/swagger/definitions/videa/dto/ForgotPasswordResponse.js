module.exports = {
    "ForgotPasswordResponse": {
        "properties": {
            "token": {
                "type": "string"
            }
        }
    }
};