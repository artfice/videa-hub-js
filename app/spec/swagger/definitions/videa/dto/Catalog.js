module.exports = {
    "Catalog": {
        "allOf": [
            {
                "$ref": "#/definitions/BaseDTO"
            },
            {
                "properties": {
                    "name": {
                        "type": "string"
                    }
                },
                "required": ["name"]
            }
        ]
    }
};
