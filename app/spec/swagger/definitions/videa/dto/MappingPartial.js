module.exports = {
	"MappingPartial": {
		"properties": {
			"name": {
				"type": "string"
			},
			"definitions" : {
				"type" : "array",
				"items" : {
					"$ref": "#/definitions/MappingDefinition"
				}
			}
		}
	}
};
