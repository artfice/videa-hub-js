module.exports = {
    "ContentField": {
        "allOf": [
            {
                "$ref": "#/definitions/BaseDTO"
            },
            {
                "properties": {
                    "name": {
                        "type": "string",
                        "description": "The name identifies this field and must be unique within the same account."
                    },
                    "title": {
                        "type": "string",
                        "description": "Label displayed in the front end."
                    },
                    "fieldType": {
                        "type": "string"
                    },
                    "size": {
                        "type": "number",
                        "description": "The relative size of the form field in a scale of 1 to 12 to be displayed in the UI."
                    },
                    "isMandatory": {
                        "type": "boolean"
                    },
                    "initialValue": {
                        "type": "string"
                    },
                    "possibleValues": {
                        "type": "array",
                        "items": {
                            "type": "string"
                        }
                    }
                },
                "required": [ "name", "fieldType" ]
            }
        ]
    }
};