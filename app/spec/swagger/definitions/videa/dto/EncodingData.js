module.exports = {
	"EncodingData": {
		"properties": {
			"videoUrl": {
				"type": "string"
			},
			"fieldId": {
				"type": "string"
			}
		}
	}
};
