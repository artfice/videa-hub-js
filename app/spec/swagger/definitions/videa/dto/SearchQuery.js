module.exports = {
    "SearchQuery": {
        "properties": {
            "field": {
                "type": "string",
                "description": "field name to perform the query. Some operators support _all to specify a search in all available fields"
            },
            "operator": {
                "type": "integer",
                "description": " 0 = In operator and checks if the current field is equal to any of the given values. 1 = Match operator and performs a full text search in the current field. 2 = MatchAll operator and retrieves all results"
            },
            "value": {
                "type": "string",
                "description": "Depends on the selected SearchOperator."
            }
        }
    }
};
