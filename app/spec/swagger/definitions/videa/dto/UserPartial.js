module.exports = {
    "UserPartial": {
        "properties": {
            "username": {
                "type": "string"
            },
            "email": {
                "type": "string"
            },
            "firstName": {
                "type": "string"
            },
            "lastName": {
                "type": "string"
            },
            "password": {
                "type": "string"
            }
        }
    }
};
