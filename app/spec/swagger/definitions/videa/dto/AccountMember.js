module.exports = {
    "AccountMember": {
        "properties": {
            "user": {
                "$ref": "#/definitions/UserPartial"
            },
            "roles": {
                "type": "array",
                "items": {
                    "type": "string"
                }
            }
        },
        "required": ["user"]
    }
};