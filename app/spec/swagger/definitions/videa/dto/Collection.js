module.exports = {
    "Collection": {
        "allOf": [
            {
                "$ref": "#/definitions/BaseDTO"
            },
            {
                "properties": {
                    "name": {
                        "type": "string"
                    },
                    "query" :
                    {
                        "type" : "string"
                    },
                    "references": {
                        "type" : "array",
                        "items": {
                            "type": "string"
                        }
                    }
                },
                "required": ["name"]
            }
        ]
    }
};
