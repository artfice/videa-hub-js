module.exports = {
    "ResultSet" : {
        "properties" : {
            "data": {
                "type": "string",
                "description": "Array of objects"
            }
        },
        required: ["data"]
    }
}