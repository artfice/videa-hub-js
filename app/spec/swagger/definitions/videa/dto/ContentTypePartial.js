module.exports = {
    "ContentTypePartial": {
        "properties": {
            "visible": {
                "type": "boolean"
            },
            "designationFieldName": {
                "type": "string"
            },
            "summaryFieldName": {
                "type": "array",
                "items": {
                    "type": "string"
                }
            },
            "fieldGroups": {
                "type": "array",
                "items": {
                    "$ref": "#/definitions/ContentFieldGroup"
                }
            }
        }
    }
};