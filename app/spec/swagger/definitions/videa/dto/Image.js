module.exports = {
    "Image": {
        "allOf": [
            {
                "$ref": "#/definitions/BaseDTO"
            },
            {
                "properties": {
                    "name": {
                        "type": "string",
                        "description": "Name of the asset."
                    },
                    "fullName": {
                        "type": "string",
                        "description": "Name of the asset in the form of /path/assetname.extension. The name is unique."
                    },
                    "imageType": {
                        "type": "string",
                        "description": "Name of the associated image type."
                    },
                    "width": {
                        "type": "integer",
                        "description": "Image width in pixels."
                    },
                    "height": {
                        "type": "integer",
                        "description": "Image height in pixels."
                    },
                    "url": {
                        "type": "string",
                        "description": "Direct Uri for getting the asset using HTTP."
                    },
                    "secure_url": {
                        "type": "string",
                        "description": "Secure direct Uri for getting the asset using HTTP."
                    }
                },
                "required": ["name", "fullName", "imageType"]
            }
        ]
    }
};