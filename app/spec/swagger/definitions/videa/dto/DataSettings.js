module.exports = {
	"DataSettings": {
		"properties": {
			"totalNodesToAdd": {
				"type": "integer"
			},
			"nodesToAddEachTime" :{
				"type" : "integer"
			}
		}
	}
};
