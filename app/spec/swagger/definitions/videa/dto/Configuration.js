module.exports = {
    "Configuration": {
        "allOf": [
            {
                "$ref": "#/definitions/BaseDTO"
            },
            {
                "properties": {
                    "name": {
                        "type": "string"
                    },
                    "deviceId": {
                        "type": "string"
                    },
                    "isActive": {
                        "type": "boolean"
                    },
                    "asset": {
                        "$ref": "#/definitions/RemoteAsset"
                    }
                },
                "required": ["name", "deviceId", "asset"]
            }
        ]
    }
};