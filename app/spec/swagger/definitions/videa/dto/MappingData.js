module.exports = {
    "MappingData": {
        "properties": {
			"source": {
				"$ref": "#/definitions/MappingSource"
			},
			"mapping" : {
				"$ref": "#/definitions/Mapping"
			}
		},
		"required": ["source", "mapping"]
    }
};
