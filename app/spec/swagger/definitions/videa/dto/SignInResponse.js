module.exports = {
    "SignInResponse": {
        "properties": {
            "token": {
                "type": "string"
            },
            "duration": {
                "type": "integer"
            },
            "userId": {
                "type": "string"
            }
        }
    }
};