module.exports = {
    "RemoteAsset": {
        "properties": {
            "containerId": {
                "type": "string"
            },
            "assetId": {
                "type": "string"
            },
            "url": {
                "type": "string"
            }
        },
        "required": ["containerId", "assetId", "url"]
    }
};