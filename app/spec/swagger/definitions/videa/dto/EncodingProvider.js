module.exports = {
    "EncodingProvider": {
        "allOf": [
            {
                "$ref": "#/definitions/BaseDTO"
            },
            {
                "properties": {
                    "provider": {
						"$ref": "#/definitions/EncodingProviderValues"
					}
                },
                "required": ["provider"]
            }
        ]
    }
};
