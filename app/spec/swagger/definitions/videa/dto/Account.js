/**
 * Created by bardiakhosravi on 15-05-13.
 */
module.exports = {
    "Account": {
        "properties": {
            "id": {
                "type": "string"
            },
            "name": {
                "type": "string"
            }
        }
    }
};