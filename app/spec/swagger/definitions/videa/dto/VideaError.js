module.exports = {
    "VideaError": {
        "properties": {
            "name": {
                "type": "string"
            },
            "message": {
                "type": "string"
            }
        },
        "required": [
            "name",
            "message"
        ]
    }
}
