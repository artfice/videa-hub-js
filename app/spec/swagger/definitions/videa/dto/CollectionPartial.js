module.exports = {
    "CollectionPartial": {
        "properties": {
            "name": {
                "type": "string"
            },
            "query" :
            {
                "type" : "string"
            },
            "references": {
                "type" : "array",
                "items": {
                    "type": "string"
                }
            }
        }
    }
};
