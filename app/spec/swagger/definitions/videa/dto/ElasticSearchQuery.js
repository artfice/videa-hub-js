module.exports = {
    "ElasticSearchQuery": {
        "type": "object",
        "properties": {
            "query_string": {
                "type": "object",
                "properties": {
                    "fields": {
                        "type" : "array",
                        "items": {
                            "type": "string"
                        },
                        "description": ""
                    },
                    "query": {
                        "type": "string",
                        "description": ""
                    }
                }
            }
        }
    }
};
