module.exports = {
    "User": {
        "allOf": [
            {
                "$ref": "#/definitions/BaseDTO"
            },
            {
                "properties": {
                    "username": {
                        "type": "string"
                    },
                    "email": {
                        "type": "string"
                    },
                    "firstName": {
                        "type": "string"
                    },
                    "lastName": {
                        "type": "string"
                    },
                    "password": {
                        "type": "string"
                    },
                    roles : {
                        "type" : "array",
                        "items": {
                            "type": "string"
                        }
                    }
                },
                "required": ["username", "email", "password"]
            }
        ]
    }
};
