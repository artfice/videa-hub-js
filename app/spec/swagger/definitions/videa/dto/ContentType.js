module.exports = {
    "ContentType": {
        "allOf": [
            {
                "$ref": "#/definitions/BaseDTO"
            },
            {
                "properties": {
                    "name": {
                        "type": "string"
                    },
                    "visible": {
                        "type": "boolean"
                    },
                    "designationFieldName": {
                        "type": "string"
                    },
                    "summaryFieldName": {
                        "type": "array",
                        "items": {
                            "type": "string"
                        }
                    },
                    "fieldGroups": {
                        "type": "array",
                        "items": {
                            "$ref": "#/definitions/ContentFieldGroup"
                        }
                    }
                },
                "required": ["name"]
            }
        ]
    }
};