/**
 * Created by ryanwong on 2015-11-06.
 */
module.exports = {
    "ScreenTemplate": {
        "allOf": [
            {
                "$ref": "#/definitions/BaseDTO"
            },
            {
                "properties": {
                    "name": {
                        "type": "string"
                    },
                    "type": {
                        "type": "string"
                    }
                }
            }
        ]
    }
};