module.exports = {
    "ContentNode": {
        "allOf": [
            {
                "$ref": "#/definitions/BaseDTO"
            },
            {
                "properties": {
                    "designation": {
                        "type": "string"
                    },
                    "parentId": {
                        "type": "string"
                    },
                    "contentTypeId": {
                        "type": "string"
                    },
                    "workflowState": {
                        "type": "string"
                    },
                    "availabilities": {
                        "type" : "array",
                        "items": {
                            "$ref": "#/definitions/Availability"
                        }
                    },
                    "values": {
                        "$ref": "#/definitions/ContentNodeValues"
                    }
                },
                "required": ["designation", "contentTypeId", "workflowState", "values"]
            }
        ]
    }
};