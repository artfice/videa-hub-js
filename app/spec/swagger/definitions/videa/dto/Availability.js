module.exports = {
    "Availability": {
        "properties": {
            "id": {
                "type": "string"
            },
            "fromDate": {
                "type": "string",
                "format": "date"
            },
            "expireDate": {
                "type": "string",
                "format": "date"
            }
        }
    }
};