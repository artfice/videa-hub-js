module.exports = {
    "Mapping": {
        "allOf": [
            {
                "$ref": "#/definitions/BaseDTO"
            },
            {
                "properties": {
                    "title": {
                        "type": "string"
                    },
					"definitions" : {
						"type" : "array",
						"items" : {
							"$ref": "#/definitions/MappingDefinition"
						}
					}
                },
                "required": ["title", "definitions"]
            }
        ]
    }
};
