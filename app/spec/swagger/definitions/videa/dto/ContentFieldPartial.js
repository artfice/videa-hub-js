module.exports = {
    "ContentFieldPartial": {
        "properties": {
            "title": {
                "type": "string",
                "description": "Label displayed in the front end."
            },
            "size": {
                "type": "number",
                "description": "The relative size of the form field in a scale of 1 to 12 to be displayed in the UI."
            },
            "isMandatory": {
                "type": "boolean"
            },
            "initialValue": {
                "type": "string"
            },
            "possibleValues": {
                "type": "array",
                "items": {
                    "type": "string"
                }
            }
        }
    }
};