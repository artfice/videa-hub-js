module.exports = {
    "Brand": {
        "allOf": [
            {
                "$ref": "#/definitions/BaseDTO"
            },
            {
                "properties": {
                    "name": {
                        "type": "string"
                    },
                    "image": {
                        "$ref": "#/definitions/Image"
                    }
                },
                "required": ["name"]
            }
        ]
    }
};