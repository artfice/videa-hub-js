/**
 * Created by ryanwong on 2015-12-02.
 */
module.exports = {
    "ThemeFonts": {
        "properties": {
            "main": {
                "$ref": "#/definitions/ColorFont"
            },
            "title": {
                "$ref": "#/definitions/ColorFont"
            },
            "menu": {
                "$ref": "#/definitions/ColorFont"
            },
            "body": {
                "$ref": "#/definitions/ColorFont"
            },
            "title2": {
                "$ref": "#/definitions/ColorFont"
            }
        }
    }
};