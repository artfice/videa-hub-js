/**
 * Created by ryanwong on 2015-01-20.
 */

module.exports = {
    "Configuration": {
        "properties": {
            "id": {
              "type": "string"  
            },
            "_metadata": {
              "type": "string"  
            },
            "authentication": {
                "$ref": "#/definitions/Authentication"
            },
            "analytic": {
                "items": {
                    "$ref": "#/definitions/Analytic"
                },
                "type": "array"
            },
            "cms": {
                "items": {
                    "$ref": "#/definitions/Cms"
                },
                "type": "array"
            },            
            "ads": {
                "$ref": "#/definitions/Ads"
            },
            "uiConfig": {
                "items": {
                    "$ref": "#/definitions/UiConfig"
                },
                "type": "array"
            }
        }
    }
};