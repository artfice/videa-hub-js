/**
 * Created by ryanwong on 2015-10-29.
 */
module.exports = {
    "NavType": {
        "properties": {
            "name": {
                "type": "string"
            },
            "value": {
                "type": "string"
            }
        }
    }
};