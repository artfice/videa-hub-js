/**
 * Created by ryanwong on 2015-10-29.
 */

module.exports = {
    "Theme": {
        "properties": {
            "assets": {
                "$ref": "#/definitions/Assets"
            },
            "colors": {
                "$ref": "#/definitions/ThemeColors"
            },
            "fonts": {
                "$ref": "#/definitions/ThemeFonts"
            }
        }
    }
};