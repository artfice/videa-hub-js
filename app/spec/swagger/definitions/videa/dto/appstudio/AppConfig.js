/**
 * Created by ryanwong on 2015-10-20.
 */
module.exports = {
    "AppConfig": {
        "allOf": [
            {
                "$ref": "#/definitions/BaseDTO"
            },
            {
                "properties": {
                    "name": {
                        "type": "string"
                    }
                },
                "required": ["name"]
            }
        ]
    }
};