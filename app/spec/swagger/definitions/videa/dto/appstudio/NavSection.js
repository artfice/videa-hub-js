/**
 * Created by ryanwong on 2015-10-29.
 */
module.exports = {
    "NavSection": {
        "properties": {
            "name": {
                "type": "string"
            },
            "icon": {
                "$ref": "#/definitions/Image"
            },
            "screen": {
                "type": "string"
            }
        }
    }
};