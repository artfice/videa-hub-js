module.exports = {
  "ChromecastTheme": {
    "properties": {
      "accentColor": {
        "$ref": "#/definitions/Color"
      },
      "secondaryColor": {
        "$ref": "#/definitions/Color"
      },
      "backgroundColor": {
        "$ref": "#/definitions/Color"
      },
      "foregroundColor": {
        "$ref": "#/definitions/Color"
      }
    }
  }
};