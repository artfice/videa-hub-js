/**
 * Created by ryanwong on 2015-12-02.
 */
module.exports = {
    "ColorGalleryImageReference": {
        "properties": {
            "galleryImageId": {
                "type": "string"
            },
            "color": {
                "$ref": "#/definitions/Color"
            }
        }
    }
};