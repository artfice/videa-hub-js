module.exports = {
  "Edition": {
    "allOf": [{
      "$ref": "#/definitions/BaseDTO"
    }, {
      "properties": {
        "name": {
          "type": "string"
        },
        "_metadata": {
          "type": "string"
        },
        "activeConfig": {
          "type": "string"
        },
        "clientConfig": {
          "type": "string"
        },
        "state": {
          "type": "string"
        },
        "cms": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/AppstudioCMS"
          }
        },
        "uiConfig": {
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "customData": {
          "$ref": "#/definitions/CustomData"
        },
        "chromecast": {
          "$ref": "#/definitions/Chromecast"
        },
        "watchHistory": {
          "$ref": "#/definitions/WatchHistory"
        },
        "authentication": {
          "$ref": "#/definitions/Authentication"
        },
      }
    }]
  }
};