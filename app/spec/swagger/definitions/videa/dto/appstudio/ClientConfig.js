/**
 * Created by bardiakhosravi on 2015-11-30.
 */

module.exports = {
    "ClientConfig": {
        "allOf": [
            {
                "$ref": "#/definitions/BaseDTO"
            }
        ]
    }
};