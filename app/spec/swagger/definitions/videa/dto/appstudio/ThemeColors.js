/**
 * Created by ryanwong on 2015-12-02.
 */
module.exports = {
    "ThemeColors": {
        "properties": {
            "primarySpot": {
                "$ref": "#/definitions/Color"
            },
            "secondarySpot": {
                "$ref": "#/definitions/Color"
            },
            "title": {
                "$ref": "#/definitions/Color"
            },
            "accent": {
                "$ref": "#/definitions/Color"
            },
            "flag": {
                "$ref": "#/definitions/Color"
            },
            "player": {
                "$ref": "#/definitions/Color"
            }
        }
    }
};