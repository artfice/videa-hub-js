/**
 * Created by ryanwong on 2015-01-20.
 */

module.exports = {
  "ClientEdition": {
    "properties": {
      "id": {
        "type": "string"
      },
      "name": {
        "type": "string"
      },
      "app": {
        "type": "string"
      },
      "brand": {
        "type": "string"
      },
      "state": {
        "type": "string"
      },
      "type": {
        "type": "string"
      },
      "clientConfig": {
        "type": "string"
      }
    }
  }
};