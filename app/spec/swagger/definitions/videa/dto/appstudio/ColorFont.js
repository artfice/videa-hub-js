/**
 * Created by ryanwong on 2015-10-29.
 */
module.exports = {
    "ColorFont": {
        "allOf": [
            {
                "$ref": "#/definitions/Font"
            },
            {
                "properties": {
                    "color": {
                        "$ref": "#/definitions/Color"
                    }
                }
            }
            ]
    }
};