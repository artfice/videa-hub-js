module.exports = {
    "App": {
        "allOf": [{
            "$ref": "#/definitions/BaseDTO"
        }, {
            "properties": {
                "id": {
                    "type": "string"
                },
                "name": {
                    "type": "string"
                },
                "_metadata": {
                    "type": "string"
                },
                "activeConfig": {
                    "type": "string"
                },
                "clientConfig": {
                    "type": "string"
                },
                "edition": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                }
            }
        }]
    }
};