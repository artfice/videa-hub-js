/**
 * Created by ryanwong on 2015-12-02.
 */
module.exports = {
    "Assets": {
        "properties": {
            "logo": {
                "$ref": "#/definitions/GalleryImageReference"
            },
            "splashScreen": {
                "$ref": "#/definitions/GalleryImageReference"
            },
            "background": {
                "$ref": "#/definitions/ColorGalleryImageReference"
            },
            "navBackground": {
                "$ref": "#/definitions/ColorGalleryImageReference"
            },
            "topBar": {
                "$ref": "#/definitions/ColorGalleryImageReference"
            },
            "appIcon": {
                "$ref": "#/definitions/GalleryImageReference"
            }
        }
    }
};