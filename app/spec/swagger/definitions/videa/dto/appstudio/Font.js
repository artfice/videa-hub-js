/**
 * Created by ryanwong on 2015-10-29.
 */
module.exports = {
    "Font": {
        "properties": {
            "family": {
                "type": "string"
            },
            "weight": {
                "type": "string"
            },
            "size": {
                "type": "number"
            }
        }
    }
};
