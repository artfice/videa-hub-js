/**
 * Created by ryanwong on 2015-10-20.
 */

module.exports = {
    "Config": {
        "allOf": [
            {
                "$ref": "#/definitions/BaseDTO"
            },
            {
                "properties": {
                    "id": {
                        "type": "string"
                    },
                    "name": {
                        "type": "string"
                    },
                    "theme": {
                        "$ref": "#/definitions/Theme"
                    },
                    //"screen": {
                    //    "$ref": "#/definitions/Screen"
                    //},
                    //"navigation": {
                    //    "type": "array",
                    //    "items": {
                    //        "$ref": "#/definitions/AppNavigation"
                    //    }
                    //}
                    //"appSetting": {
                    //    "$ref": "#/definitions/AppSetting"
                    //}
                }
            }
        ]
    }
};