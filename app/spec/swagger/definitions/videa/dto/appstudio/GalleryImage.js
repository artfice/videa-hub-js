/**
 * Created by ryanwong on 2015-11-09.
 */
module.exports = {
    "GalleryImage": {
        "allOf": [
            {
                "$ref": "#/definitions/Image"
            },
            {
                "properties": {
                    "title": {
                        "type": "string"
                    },
                    "active": {
                        "type": "boolean"
                    }
                }
            }
        ]
    }
};