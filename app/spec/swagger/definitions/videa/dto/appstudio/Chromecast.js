module.exports = {
  "Chromecast": {
    "properties": {
      "enabled": {
        "type": "boolean"
      },
      "appId": {
        "type": "string"
      },
      "theme": {
        "$ref": "#/definitions/ChromecastTheme"
      },
      "title": {
        "$ref": "#/definitions/ChromecastTitle"
      },
      "reciever": {
        "$ref": "#/definitions/ChromecastReciever"
      }
    }
  }
};