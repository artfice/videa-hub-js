
module.exports = {
    "Worker": {
        "allOf": [
            {
                "$ref": "#/definitions/BaseDTO"
            },
            {
                "properties": {
                    "progress": {
                        "type": "number"
                    },
                    "description": {
                        "type": "string"
                    },
                    "state": {
                        "type": "string"
                    },
                    "adminId" : {
                        "type" : "string"
                    }
                }
            }
        ]
    }
};
