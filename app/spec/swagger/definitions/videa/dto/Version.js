module.exports = {
    "Version": {
        "properties": {
            "label": {
                "type": "string"
            },
            "build": {
                "type": "number"
            }
        }
    }
};