module.exports = {
    "ContentFieldGroup": {
        "properties": {
            "name": {
                "type": "string"
            },
            "fieldIds": {
                "type": "array",
                "items": {
                    "type": "string"
                }
            }
        }
    }
};
