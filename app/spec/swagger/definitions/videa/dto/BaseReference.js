module.exports = {
    "BaseReference": {
        "properties": {
            "value": {
                "type": "string"
            },
            "refId": {
                "type": "string"
            }
        }
    }
};
