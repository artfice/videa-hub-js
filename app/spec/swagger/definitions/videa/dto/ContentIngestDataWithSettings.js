module.exports = {
	"ContentIngestDataWithSettings": {
		"properties": {
			"entries": {
				"type" : "array",
				"items": {
					"$ref": "#/definitions/ContentIngestData"
				}
			}
		},
		"required": ["entries"]
	}
};
