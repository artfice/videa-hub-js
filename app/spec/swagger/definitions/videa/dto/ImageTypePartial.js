module.exports = {
    "ImageTypePartial": {
        "properties": {
            "name": {
                "type": "string",
                "description": "Name that identifies the current image type. The name must be unique."
            },
            "width": {
                "type": "integer",
                "description": "Image width in pixels."
            },
            "height": {
                "type": "integer",
                "description": "Image height in pixels."
            }
        }
    }
};
