module.exports = {
    "DevicePartial": {
        "properties": {
            "name": {
                "type": "string"
            },
            "activeConfigUrl": {
                "type": "string"
            }
        }
    }
};