module.exports = {
	"ContentIngestSettings": {
		"properties": {
			"delimiter": {
				"type": "string",
			},
			"mappingId": {
				"type": "string"
			},
			"mapping": {
				"$ref": "#/definitions/MappingPartial"
			}
		},
		"required": ["delimiter"]
	}
};
