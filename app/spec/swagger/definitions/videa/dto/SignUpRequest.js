module.exports = {
    "SignUpRequest": {
        "properties": {
            "user": {
                "$ref": "#/definitions/User"
            },
            "account": {
                "$ref": "#/definitions/Account"
            },
            "workflowState" : {
                "type": "string"
            }
        }
    }
};
