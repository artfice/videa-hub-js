/**
 * Created by ryanwong on 2015-11-06.
 */
module.exports = {
    "ScreenType": {
        "allOf": [
            {
                "$ref": "#/definitions/BaseDTO"
            },
            {
                "properties": {
                    "description": {
                        "type": "string"
                    },
                    "image": {
                        "$ref": "#/definitions/Image"
                    }
                }
            }
        ]
    }
};