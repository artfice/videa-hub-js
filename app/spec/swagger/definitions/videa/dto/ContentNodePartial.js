module.exports = {
    "ContentNodePartial": {
        "properties": {
            "designation": {
                "type": "string"
            },
            "availabilities": {
                "type" : "array",
                "items": {
                    "$ref": "#/definitions/Availability"
                }
            },
            "values": {
                "$ref": "#/definitions/ContentNodeValues"
            }
        }
    }
};