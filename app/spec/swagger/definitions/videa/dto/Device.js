module.exports = {
    "Device": {
        "allOf": [
            {
                "$ref": "#/definitions/BaseDTO"
            },
            {
                "properties": {
                    "name": {
                        "type": "string"
                    },
                    "brandId": {
                        "type": "string"
                    },
                    "activeConfigUrl": {
                        "type": "string"
                    }
                },
                "required": ["name", "brandId"]
            }
        ]
    }
};