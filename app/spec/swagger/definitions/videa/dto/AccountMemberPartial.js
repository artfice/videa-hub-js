module.exports = {
    "AccountMemberPartial": {
        "properties": {
            "user": {
                "$ref": "#/definitions/UserPartial"
            },
            "roles": {
                "type": "array",
                "items": {
                    "type": "string"
                }
            }
        }
    }
};