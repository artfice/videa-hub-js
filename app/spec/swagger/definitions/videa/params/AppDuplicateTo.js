module.exports = {
    "AppDuplicateTo": {
        "properties": {
            "toAccountId": {
                "type": "string",
                "description": "Account Id to duplicate to"
            },
            "toBrandId": {
                "type": "string",
                "description": "Id of an existing brand to duplicate to"
            }
        }
    }
};
