module.exports = {
    "/accounts/{accountId}/catalogs": {
        "get": {
            "x-swagger-router-controller": "catalogs",
            "x-videa-resource" : "catalogs",
            "x-videa-permissions" : ["view"],
            "operationId": "getCatalogs",
            "tags": ["Catalogs"],
            "description": "Returns the list of catalogs.",
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "offset",
                    "in": "query",
                    "description": "Offset in the result set",
                    "type": "integer",
                    "default": 0
                },
                {
                    "name": "setSize",
                    "in": "query",
                    "description": "Limit number of results",
                    "type": "integer",
                    "default": 10
                },
                {
                    "name": "sort",
                    "in": "query",
                    "description": "User field name used to sort the results",
                    "type": "string"
                },
                {
                    "name": "ascending",
                    "in": "query",
                    "description": "Used to order by the results based on the given sort parameter",
                    "type": "boolean",
                    "default": true
                },
				{
					"name": "query",
					"in": "query",
					"description": "Search query to filter results.",
					"type": "string"
				}
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/Catalog"
                    }
                }
            }
        },
        "post": {
            "x-swagger-router-controller": "catalogs",
            "x-videa-resource" : "catalogs",
            "x-videa-permissions" : ["edit"],
            "operationId": "postCatalog",
            "tags": ["Catalogs"],
            "description": "Adds a new catalog.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "catalog",
                    "in": "body",
                    "description": "Catalog data to add. The Id and dates will be filled by the service and should not be sent.",
                    "required": true,
                    "schema": {
                        "$ref": "#/definitions/Catalog"
                    }
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/Catalog"
                    }
                },
                "400": {
                    "description": "Given catalog doesn't have an unique Id.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    },
    "/accounts/{accountId}/catalogs/{catalogId}": {
        "get": {
            "x-swagger-router-controller": "catalogs",
            "x-videa-resource" : "catalogs",
            "x-videa-permissions" : ["view"],
            "operationId": "getCatalog",
            "tags": ["Catalogs"],
            "description": "Returns an existing catalog.",
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "catalogId",
                    "in": "path",
                    "description": "Id of an existing catalog.",
                    "required": true,
                    "type": "string"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/Catalog"
                    }
                },
                "400": {
                    "description": "Catalog Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },
        "put": {
            "x-swagger-router-controller": "catalogs",
            "x-videa-resource" : "catalogs",
            "x-videa-permissions" : ["edit"],
            "operationId": "putCatalog",
            "tags": ["Catalogs"],
            "description": "Modifies an existing catalog.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "catalogId",
                    "in": "path",
                    "description": "Catalog Id to update.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "catalog",
                    "in": "body",
                    "description": "Catalog attributes to modify",
                    "required": true,
                    "schema": {
                        "$ref": "#/definitions/CatalogPartial"
                    }
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/Catalog"
                    }
                },
                "400": {
                    "description": "Catalog Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },
        "delete": {
            "x-swagger-router-controller": "catalogs",
            "x-videa-resource" : "catalogs",
            "x-videa-permissions" : ["delete"],
            "operationId": "removeCatalog",
            "tags": ["Catalogs"],
            "description": "Deletes an existing catalog.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "catalogId",
                    "in": "path",
                    "description": "Id of an existing catalog.",
                    "required": true,
                    "type": "string"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request."
                },
                "400": {
                    "description": "Catalog Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    }
};
