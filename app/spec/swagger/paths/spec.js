
module.exports = {
    "/spec": {
        "get": {
            "x-swagger-router-controller": "spec",
            "operationId": "getJson",
            "tags": ["API Spec"],
            "description": "Returns the API swagger specification in json format.",
			"security": [{
				"videa_auth": []
			}],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request."
                },
                "400": {
                    "description": "Error.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    }
};
