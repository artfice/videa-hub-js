
module.exports = {
    "/accounts": {
        "get": {
            "x-swagger-router-controller": "accounts",
            "x-videa-resource" : "accounts",
            "x-videa-permissions" : ["view"],
            "operationId": "getAccounts",
            "tags": ["Accounts"],
            "description": "Returns the list of accounts.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "offset",
                    "in": "query",
                    "description": "Offset in the result set",
                    "type": "integer",
                    "default": 0
                },
                {
                    "name": "setSize",
                    "in": "query",
                    "description": "Limit number of results",
                    "type": "integer",
                    "default": 10
                },
                {
                    "name": "sort",
                    "in": "query",
                    "description": "User field name used to sort the results",
                    "type": "string"
                },
                {
                    "name": "ascending",
                    "in": "query",
                    "description": "Used to order by the results based on the given sort parameter",
                    "type": "boolean",
                    "default": true
                },
				{
					"name": "query",
					"in": "query",
					"description": "Search query to filter results.",
					"type": "string"
				}
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/Account"
                    }
                }
            }
        },
        "post": {
            "x-swagger-router-controller": "accounts",
            "operationId": "postAccount",
            "tags": ["Accounts"],
            "description": "Adds a new account.",
            "parameters": [
                {
                    "name": "account",
                    "in": "body",
                    "description": "Account data to add. The id should be unique otherwise an error will be thrown.",
                    "required": true,
                    "schema": {
                        "$ref": "#/definitions/Account"
                    }
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/Account"
                    }
                },
                "400": {
                    "description": "Given account doesn't have an unique accountId.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    },
    "/accounts/{accountId}": {
        "get": {
            "x-swagger-router-controller": "accounts",
            "x-videa-resource" : "accounts",
            "x-videa-permissions" : ["view"],
            "operationId": "getAccount",
            "tags": ["Accounts"],
            "description": "Returns an existing account.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "account id of an existing account.",
                    "required": true,
                    "type": "string"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/Account"
                    }
                },
                "400": {
                    "description": "account with the given id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },
        "put": {
            "x-swagger-router-controller": "accounts",
            "x-videa-resource" : "accounts",
            "x-videa-permissions" : ["edit"],
            "operationId": "putAccount",
            "tags": ["Accounts"],
            "description": "Modifies an existing account.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "id of the account to update.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "account",
                    "in": "body",
                    "description": "Account attributes to modify",
                    "required": true,
                    "schema": {
                        "$ref": "#/definitions/Account"
                    }
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/Account"
                    }
                },
                "400": {
                    "description": "Account with given id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },
        "delete": {
            "x-swagger-router-controller": "accounts",
            "x-videa-resource" : "accounts",
            "x-videa-permissions" : ["delete"],
            "operationId": "removeAccount",
            "tags": ["Accounts"],
            "description": "Deletes an existing account.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "id of an existing account.",
                    "required": true,
                    "type": "string"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request."
                },
                "400": {
                    "description": "Account with given id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    }
};
