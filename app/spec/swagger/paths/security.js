module.exports = {
    "/security/permissions": {
        "get": {
            "x-swagger-router-controller": "security",
            "operationId": "getPermissions",
            "tags": ["Security"],
            "description": "Get user permissions for the given account",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "query",
                    "description": "Account Id.",
                    "type" : "string"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/Permissions"
                    }
                }
            }
        }
    }
};