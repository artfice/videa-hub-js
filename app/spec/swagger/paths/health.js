module.exports = {
    "/health/status": {
        "get": {
            "x-swagger-router-controller": "healthStatus",
            "operationId": "checkHealth",
            "tags": ["Health"],
            "description": "Check if the servers alive.",
            "parameters": [],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema" : {}
                }
            }
        }
    }
};