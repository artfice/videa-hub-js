module.exports = {
	"/accounts/mappings/convert" :{
		"post": {
			"x-swagger-router-controller": "mappings",
			"x-videa-resource" : "mappings",
			"x-videa-permissions" : ["edit"],
			"operationId": "convertObjectWithMapping",
			"tags": ["Mappings"],
			"description": "Convert object according to mapping.",
			"security": [{
				"videa_auth": []
			}],
			"parameters": [
				{
					"name": "mappingData",
					"in": "body",
					"description": "Mapping data containing the mapping and source to be converted.",
					"required": true,
					"schema": {
						"$ref": "#/definitions/MappingData"
					}
				}
			],
			"responses": {
				"default": {
					"description": "Invalid request.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				},
				"200": {
					"description": "Successful request."
				},
				"400": {
					"description": "Given mapping doesn't have an unique Id.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				}
			}
		}
	},
    "/accounts/{accountId}/mappings": {
        "get": {
            "x-swagger-router-controller": "mappings",
            "x-videa-resource" : "mappings",
            "x-videa-permissions" : ["view"],
            "operationId": "getMappings",
            "tags": ["Mappings"],
            "description": "Returns the list of mappings.",
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "offset",
                    "in": "query",
                    "description": "Offset in the result set",
                    "type": "integer",
                    "default": 0
                },
                {
                    "name": "setSize",
                    "in": "query",
                    "description": "Limit number of results",
                    "type": "integer",
                    "default": 10
                },
                {
                    "name": "sort",
                    "in": "query",
                    "description": "User field name used to sort the results",
                    "type": "string"
                },
                {
                    "name": "ascending",
                    "in": "query",
                    "description": "Used to order by the results based on the given sort parameter",
                    "type": "boolean",
                    "default": true
                },
				{
					"name": "query",
					"in": "query",
					"description": "Search query to filter results.",
					"type": "string"
				}
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/Mapping"
                    }
                }
            }
        },
        "post": {
            "x-swagger-router-controller": "mappings",
            "x-videa-resource" : "mappings",
            "x-videa-permissions" : ["edit"],
            "operationId": "postMapping",
            "tags": ["Mappings"],
            "description": "Adds a new mapping.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "mapping",
                    "in": "body",
                    "description": "Mapping data to add. The Id and dates will be filled by the service and should not be sent.",
                    "required": true,
                    "schema": {
                        "$ref": "#/definitions/Mapping"
                    }
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/Mapping"
                    }
                },
                "400": {
                    "description": "Given mapping doesn't have an unique Id.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    },
    "/accounts/{accountId}/mappings/{mappingId}": {
        "get": {
            "x-swagger-router-controller": "mappings",
            "x-videa-resource" : "mappings",
            "x-videa-permissions" : ["view"],
            "operationId": "getMapping",
            "tags": ["Mappings"],
            "description": "Returns an existing mapping.",
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "mappingId",
                    "in": "path",
                    "description": "Id of an existing mapping.",
                    "required": true,
                    "type": "string"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/Mapping"
                    }
                },
                "400": {
                    "description": "Mapping Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },
        "put": {
            "x-swagger-router-controller": "mappings",
            "x-videa-resource" : "mappings",
            "x-videa-permissions" : ["edit"],
            "operationId": "putMapping",
            "tags": ["Mappings"],
            "description": "Modifies an existing mapping.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "mappingId",
                    "in": "path",
                    "description": "Mapping Id to update.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "mapping",
                    "in": "body",
                    "description": "Mapping attributes to modify",
                    "required": true,
                    "schema": {
                        "$ref": "#/definitions/MappingPartial"
                    }
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/Mapping"
                    }
                },
                "400": {
                    "description": "Mapping Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },
        "delete": {
            "x-swagger-router-controller": "mappings",
            "x-videa-resource" : "mappings",
            "x-videa-permissions" : ["delete"],
            "operationId": "removeMapping",
            "tags": ["Mappings"],
            "description": "Deletes an existing mapping.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "mappingId",
                    "in": "path",
                    "description": "Id of an existing mapping.",
                    "required": true,
                    "type": "string"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request."
                },
                "400": {
                    "description": "Mapping Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    },
	"/accounts/{accountId}/mappings/{mappingId}/convert" :{
		"post": {
			"x-swagger-router-controller": "mappings",
			"x-videa-resource" : "mappings",
			"x-videa-permissions" : ["edit"],
			"operationId": "convertObject",
			"tags": ["Mappings"],
			"description": "Convert object according to mapping.",
			"security": [{
				"videa_auth": []
			}],
			"parameters": [
				{
					"name": "accountId",
					"in": "path",
					"description": "Account Id",
					"type": "string",
					"required": true
				},
				{
					"name": "mappingId",
					"in": "path",
					"description": "Mapping Id",
					"type": "string",
					"required": true
				},
				{
					"name": "source",
					"in": "body",
					"description": "Source object to convert according to an existing Mapping",
					"required": true,
					"schema": {
						"$ref": "#/definitions/MappingSource"
					}
				}
			],
			"responses": {
				"default": {
					"description": "Invalid request.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				},
				"200": {
					"description": "Successful request."
				},
				"400": {
					"description": "Given mapping doesn't have an unique Id.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				}
			}
		}
	}
};
