/**
 * Created by bardiakhosravi on 2015-11-30.
 */


module.exports = {
    "/clientapp/accounts/{accountId}/config/{configId}": {
        "get": {
            "x-swagger-router-controller": "clientapp",
            "operationId": "getConfig",
            "tags": ["Client App Config"],
            "description": "Returns a client app configuration",
            "parameters": [{
                "name": "accountId",
                "in": "path",
                "description": "Account Id",
                "type": "string",
                "required": true
            }, {
                "name": "configId",
                "in": "path",
                "description": "Config Id",
                "type": "string",
                "required": true
            }],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/ClientConfig"
                    }
                }
            }
        }
    },
    "/clientapp/accounts/{accountId}/app/{appId}/published": {
        "get": {
            "x-swagger-router-controller": "clientapp",
            "operationId": "getPublishedConfig",
            "tags": ["Client App Config"],
            "description": "Returns a published client app",
            "parameters": [{
                "name": "accountId",
                "in": "path",
                "description": "Account Id",
                "type": "string",
                "required": true
            }, {
                "name": "appId",
                "in": "path",
                "description": "app Id",
                "type": "string",
                "required": true
            }],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/ClientConfig"
                    }
                }
            }
        }
    },
    "/clientapp/accounts/{accountId}/editions": {
        "get": {
            "x-swagger-router-controller": "clientapp",
            "operationId": "getEditions",
            "tags": ["Client App Config"],
            "description": "Returns a Client Edition list",
            "parameters": [{
                "name": "accountId",
                "in": "path",
                "description": "Account Id",
                "type": "string",
                "required": true
            }, {
                "name": "brandId",
                "in": "query",
                "description": "Filter By brandId",
                "type": "string",
                "default": 0
            }, {
                "name": "status",
                "in": "query",
                "description": "Filter by edition status",
                "type": "string",
                "default": 0
            }, {
                "name": "editionId",
                "in": "query",
                "description": "Filter by editionId",
                "type": "string"
            }],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/ClientEdition"
                    }
                }
            }
        }
    }
};