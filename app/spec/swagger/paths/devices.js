module.exports = {
    "/accounts/{accountId}/brands/{brandId}/devices": {
        "get": {
            "x-swagger-router-controller": "devices",
            "operationId": "getDevices",
            "tags": ["Devices"],
            "description": "Returns the list of devices linked to a specific brand.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "brandId",
                    "in": "path",
                    "description": "Id of an existing brand.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "offset",
                    "in": "query",
                    "description": "Offset in the result set",
                    "type": "integer",
                    "default": 0
                },
                {
                    "name": "setSize",
                    "in": "query",
                    "description": "Limit number of results",
                    "type": "integer",
                    "default": 10
                },
                {
                    "name": "sort",
                    "in": "query",
                    "description": "User field name used to sort the results",
                    "type": "string"
                },
                {
                    "name": "ascending",
                    "in": "query",
                    "description": "Used to order by the results based on the given sort parameter",
                    "type": "boolean",
                    "default": true
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/ResultSet"
                    }
                }
            }
        },
        "post": {
            "x-swagger-router-controller": "devices",
            "operationId": "postDevice",
            "tags": ["Devices"],
            "description": "Adds a new device.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "brandId",
                    "in": "path",
                    "description": "Id of an existing brand.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "device",
                    "in": "body",
                    "description": "Device data to add. The Id and dates will be filled by the service and should not be sent.",
                    "required": true,
                    "schema": {
                        "$ref": "#/definitions/DevicePartial"
                    }
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/DevicePartial"
                    }
                },
                "400": {
                    "description": "Given device doesn't have an unique Id.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    },
    "/accounts/{accountId}/brands/{brandId}/devices/{deviceId}": {
        "get": {
            "x-swagger-router-controller": "devices",
            "operationId": "getDevice",
            "tags": ["Devices"],
            "description": "Returns an existing device.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "brandId",
                    "in": "path",
                    "description": "Id of an existing brand.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "deviceId",
                    "in": "path",
                    "description": "Id of an existing device.",
                    "required": true,
                    "type": "string"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/Device"
                    }
                },
                "400": {
                    "description": "Device Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },
        "put": {
            "x-swagger-router-controller": "devices",
            "operationId": "putDevice",
            "tags": ["Devices"],
            "description": "Modifies an existing device.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "brandId",
                    "in": "path",
                    "description": "Brand Id to update.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "deviceId",
                    "in": "path",
                    "description": "Id of an existing device.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "device",
                    "in": "body",
                    "description": "Brand attributes to modify",
                    "required": true,
                    "schema": {
                        "$ref": "#/definitions/DevicePartial"
                    }
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/Device"
                    }
                },
                "400": {
                    "description": "Device Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },
        "delete": {
            "x-swagger-router-controller": "devices",
            "operationId": "removeDevice",
            "tags": ["Devices"],
            "description": "Deletes an existing device.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "brandId",
                    "in": "path",
                    "description": "Id of an existing brand.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "deviceId",
                    "in": "path",
                    "description": "Id of an existing device.",
                    "required": true,
                    "type": "string"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request."
                },
                "400": {
                    "description": "Device Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    }
};