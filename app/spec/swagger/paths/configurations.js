module.exports = {
    "/accounts/{accountId}/brands/{brandId}/devices/{deviceId}/configurations": {
        "get": {
            "x-swagger-router-controller": "configurations",
            "operationId": "getConfigurations",
            "tags": ["Configuration"],
            "description": "Returns the list of configurations linked to a specific device.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "brandId",
                    "in": "path",
                    "description": "Id of an existing brand.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "deviceId",
                    "in": "path",
                    "description": "Id of an existing device.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "offset",
                    "in": "query",
                    "description": "Offset in the result set",
                    "type": "integer",
                    "default": 0
                },
                {
                    "name": "setSize",
                    "in": "query",
                    "description": "Limit number of results",
                    "type": "integer",
                    "default": 10
                },
                {
                    "name": "sort",
                    "in": "query",
                    "description": "User field name used to sort the results",
                    "type": "string"
                },
                {
                    "name": "ascending",
                    "in": "query",
                    "description": "Used to order by the results based on the given sort parameter",
                    "type": "boolean",
                    "default": true
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/ResultSet"
                    }
                }
            }
        },
        "post": {
            "x-swagger-router-controller": "configurations",
            "operationId": "uploadConfiguration",
            "tags": ["Configuration"],
            "description": "Upload a new json file associated to the given device.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "brandId",
                    "in": "path",
                    "description": "Id of an existing Brand.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "deviceId",
                    "in": "path",
                    "description": "Id of an existing Device.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "file",
                    "in": "formData",
                    "description": "Configuration in json format to upload.",
                    "required": true,
                    "type": "file"
                },
                {
                    "name": "filename",
                    "in": "formData",
                    "description": "Name of the configuration.",
                    "type": "string"
                }
            ],
            "consumes": ["multipart/form-data"],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request returns the new configuration details.",
                    "schema": {
                        "$ref": "#/definitions/Configuration"
                    }
                },
                "400": {
                    "description": "Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    },
    "/accounts/{accountId}/brands/{brandId}/devices/{deviceId}/configurations/{configId}/setdefault": {
        "post": {
            "x-swagger-router-controller": "configurations",
            "operationId": "setDefaultConfiguration",
            "tags": ["Configuration"],
            "description": "Sets the given configuration Id as the default configuration on the current device.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "brandId",
                    "in": "path",
                    "description": "Id of an existing Brand.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "deviceId",
                    "in": "path",
                    "description": "Id of an existing Device.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "configId",
                    "in": "path",
                    "description": "Id of an existing Configuration.",
                    "required": true,
                    "type": "string"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request."
                },
                "400": {
                    "description": "Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    }
};