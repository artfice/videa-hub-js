module.exports = {
    "/signups": {
        "get": {
            "x-swagger-router-controller": "signUps",
            "x-videa-resource" : "signUps",
            "x-videa-permissions" : ["view"],
            "operationId": "getSignUpRequests",
            "tags": ["Sign Ups"],
            "description": "Returns the list of sign up requests.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "offset",
                    "in": "query",
                    "description": "Offset in the result set",
                    "type": "integer",
                    "default": 0
                },
                {
                    "name": "setSize",
                    "in": "query",
                    "description": "Limit number of results",
                    "type": "integer",
                    "default": 10
                },
                {
                    "name": "sort",
                    "in": "query",
                    "description": "User field name used to sort the results",
                    "type": "string"
                },
                {
                    "name": "ascending",
                    "in": "query",
                    "description": "Used to order by the results based on the given sort parameter",
                    "type": "boolean",
                    "default": true
                },
				{
					"name": "query",
					"in": "query",
					"description": "Search query to filter results.",
					"type": "string"
				}
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/SignUpRequest"
                    }
                }
            }
        },
        "post": {
            "x-swagger-router-controller": "signUps",
            "operationId": "signUp",
            "tags": ["Sign Ups"],
            "description": "Signs up an account",
            "parameters": [
                {
                    "name": "signUp",
                    "in": "body",
                    "description": "SignUp with the account and user.",
                    "required": true,
                    "schema": {
                        "$ref": "#/definitions/SignUpRequest"
                    }
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/SignUpResponse"
                    }
                },
                "400": {
                    "description": "Given user doesn't have an unique Id.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "401": {
                    "description": "Username and password did not match.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }

            }
        }
    },
    "/signups/{signUpRequestId}":{
        "put": {
            "x-swagger-router-controller": "signUps",
            "x-videa-resource" : "signUps",
            "x-videa-permissions" : ["edit"],
            "operationId": "putSignUp",
            "tags": ["Sign Ups"],
            "description": "Modifies an existing signUpRequest.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "signUpRequestId",
                    "in": "path",
                    "description": "Id of the signUpRequest to update.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "signUp",
                    "in": "body",
                    "description": "SignUpRequest attributes to modify",
                    "required": true,
                    "schema": {
                        "$ref": "#/definitions/SignUpRequest"
                    }
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/SignUpResponse"
                    }
                },
                "400": {
                    "description": "SignUpRequest with given id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    }
};
