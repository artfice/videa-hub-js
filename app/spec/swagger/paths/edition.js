/**
 * Created by ryanwong on 2015-10-20.
 */
module.exports = {
    "/accounts/{accountId}/apps/{appId}/editions": {
        "post": {
            "x-swagger-router-controller": "editions",
            "operationId": "createEdition",
            "tags": ["Edition"],
            "description": "Adds a new Edition.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [{
                "name": "accountId",
                "in": "path",
                "description": "Account Id",
                "type": "string",
                "required": true
            }, {
                "name": "appId",
                "in": "path",
                "description": "App Id",
                "type": "string",
                "required": true
            }, {
                "name": "edition",
                "in": "body",
                "description": "Edition Schema",
                "required": true,
                "schema": {
                    "$ref": "#/definitions/Edition"
                }
            }],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/App"
                    }
                },
                "400": {
                    "description": "Given app doesn't have an unique Id.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },
        "get": {
            "x-swagger-router-controller": "editions",
            "operationId": "getAllEdition",
            "tags": ["Edition"],
            "description": "Returns all Edition in app.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [{
                "name": "accountId",
                "in": "path",
                "description": "Account Id",
                "type": "string",
                "required": true
            }, {
                "name": "appId",
                "in": "path",
                "description": "Id of an existing app.",
                "required": true,
                "type": "string"
            }],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/Edition"
                    }
                },
                "400": {
                    "description": "Edition Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    },
    "/accounts/{accountId}/apps/{appId}/editions/{editionId}": {
        "get": {
            "x-swagger-router-controller": "editions",
            "operationId": "getEdition",
            "tags": ["Edition"],
            "description": "Returns an Edition.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [{
                "name": "accountId",
                "in": "path",
                "description": "Account Id",
                "type": "string",
                "required": true
            }, {
                "name": "appId",
                "in": "path",
                "description": "Id of an existing app.",
                "required": true,
                "type": "string"
            }, {
                "name": "editionId",
                "in": "path",
                "description": "Id of an existing edition.",
                "required": true,
                "type": "string"
            }],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/Edition"
                    }
                },
                "400": {
                    "description": "Edition Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },
        "put": {
            "x-swagger-router-controller": "editions",
            "operationId": "updateEdition",
            "tags": ["Edition"],
            "description": "Modifies an existing Edition.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [{
                "name": "accountId",
                "in": "path",
                "description": "Account Id",
                "type": "string",
                "required": true
            }, {
                "name": "appId",
                "in": "path",
                "description": "App Id to update.",
                "required": true,
                "type": "string"
            }, {
                "name": "editionId",
                "in": "path",
                "description": "Edition Id to update.",
                "required": true,
                "type": "string"
            }, {
                "name": "edition",
                "in": "body",
                "description": "Edition object you want to modify",
                "required": true,
                "schema": {
                    "$ref": "#/definitions/Edition"
                }
            }],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/Edition"
                    }
                },
                "400": {
                    "description": "App Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },
        "delete": {
            "x-swagger-router-controller": "editions",
            "operationId": "deleteEdition",
            "tags": ["Edition"],
            "description": "Deletes an existing Edition and all configurations associated.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [{
                "name": "accountId",
                "in": "path",
                "description": "Account Id",
                "type": "string",
                "required": true
            }, {
                "name": "appId",
                "in": "path",
                "description": "Id of an existing app.",
                "required": true,
                "type": "string"
            }, {
                "name": "editionId",
                "in": "path",
                "description": "Edition Id to update.",
                "required": true,
                "type": "string"
            }],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request."
                },
                "400": {
                    "description": "App Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    },
    "/accounts/{accountId}/apps/{appId}/editions/{editionId}/duplicate": {
        "post": {
            "x-swagger-router-controller": "editions",
            "operationId": "duplicateEdition",
            "tags": ["Edition"],
            "description": "Duplicate an existing Edition.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [{
                "name": "accountId",
                "in": "path",
                "description": "Account Id",
                "type": "string",
                "required": true
            }, {
                "name": "appId",
                "in": "path",
                "description": "App Id to update.",
                "required": true,
                "type": "string"
            }, {
                "name": "editionId",
                "in": "path",
                "description": "Edition Id to update.",
                "required": true,
                "type": "string"
            }],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/Edition"
                    }
                },
                "400": {
                    "description": "App Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    },
    "/accounts/{accountId}/apps/{appId}/states": {
        "get": {
            "x-swagger-router-controller": "editions",
            "operationId": "getEditionStates",
            "tags": ["Edition"],
            "description": "Returns an Edition.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [{
                "name": "accountId",
                "in": "path",
                "description": "Account Id",
                "type": "string",
                "required": true
            }, {
                "name": "appId",
                "in": "path",
                "description": "Id of an existing app.",
                "required": true,
                "type": "string"
            }],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/BaseDTO"
                    }
                },
                "400": {
                    "description": "Edition Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    }
};