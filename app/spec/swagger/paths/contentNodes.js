module.exports = {
    "/accounts/{accountId}/content": {
        "get": {
            "x-swagger-router-controller": "contentNodes",
            "x-videa-resource": "contentNodes",
            "x-videa-permissions": ["view"],
            "operationId": "getContentNodes",
            "tags": ["Content Nodes"],
            "description": "Returns the list of ContentNode.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id.",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "offset",
                    "in": "query",
                    "description": "Offset in the result set.",
                    "type": "integer",
                    "default": 0
                },
                {
                    "name": "setSize",
                    "in": "query",
                    "description": "Limit number of results.",
                    "type": "integer",
                    "default": 10
                },
                {
                    "name": "sort",
                    "in": "query",
                    "description": "User field name used to sort the results.",
                    "type": "string"
                },
                {
                    "name": "ascending",
                    "in": "query",
                    "description": "Used to order by the results based on the given sort parameter.",
                    "type": "boolean",
                    "default": true
                },
                {
                    "name": "workflows",
                    "in": "query",
                    "description": "Parameter to choose the content with a one or more workflow",
                    "type": "string",
                    "default": '["Published"]'
                },
                {
                    "name": "q",
                    "in": "query",
                    "description": "DSL search query to filter results.",
                    "type": "string"
                },
                {
                    "name": "includeRefs",
                    "in": "query",
                    "description": "Activate to include reference content nodes.",
                    "type": "boolean",
                    "default": false
                },
                {
                    "name": "removeExpired",
                    "in": "query",
                    "description": "Activate to remove expired content nodes.",
                    "type": "boolean",
                    "default": false
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/ContentNode"
                    }
                }
            }
        },
        "post": {
            "x-swagger-router-controller": "contentNodes",
            "x-videa-resource": "contentNodes",
            "x-videa-permissions": ["edit"],
            "operationId": "postContentNode",
            "tags": ["Content Nodes"],
            "description": "Adds a new ContentNode.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "contentNode",
                    "in": "body",
                    "description": "ContentNode data to add. The Id and dates will be filled by the service and should not be sent.",
                    "required": true,
                    "schema": {
                        "$ref": "#/definitions/ContentNodePartial"
                    }
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/ContentNode"
                    }
                },
                "400": {
                    "description": "Given brand doesn't have an unique Id.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    },
    "/accounts/{accountId}/content/ids": {
        "get": {
            "x-swagger-router-controller": "contentNodes",
            "x-videa-resource": "contentNodes",
            "x-videa-permissions": ["view"],
            "operationId": "getContentNodesByIds",
            "tags": ["Content Nodes"],
            "description": "Returns the list of ContentNode, of a specific list of ids. The format of the list is [\"id1\",\"id2\",...].",
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id.",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "ids",
                    "in": "query",
                    "description": "Parameter to choose the contents with ids",
                    "type": "string"
                },
                {
                    "name": "includeRefs",
                    "in": "query",
                    "description": "Activate to include reference content nodes.",
                    "type": "boolean",
                    "default": false
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/ContentNode"
                    }
                }
            }
        },
        "post": {
            "x-swagger-router-controller": "contentNodes",
            "x-videa-resource": "contentNodes",
            "x-videa-permissions": ["view"],
            "operationId": "searchContentNodesByIds",
            "tags": ["Content Nodes"],
            "description": "Returns the list of ContentNode, of a specific list of ids. The format of the list is [\"id1\",\"id2\",...].",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id.",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "includeRefs",
                    "in": "query",
                    "description": "Activate to include reference content nodes.",
                    "type": "boolean",
                    "default": false
                },
                {
                    "name": "ids",
                    "in": "body",
                    "description": "Parameter to choose the contents with ids",
                    "schema": {
                        "type": "array",
                        "items": {
                            "type": "string"
                        }
                    }
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/ContentNode"
                    }
                }
            }
        }
    },
    "/accounts/{accountId}/content/search/basic/{templateId}": {
        "get": {
            "x-swagger-router-controller": "contentNodes",
            "x-videa-resource": "contentNodes",
            "x-videa-permissions": ["view"],
            "operationId": "basicSearchUsingQueryTemplate",
            "tags": ["Content Nodes"],
            "description": "Returns a list of ContentNodes using keywords passed to a ElasticSearch query template.",
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id.",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "templateId",
                    "in": "path",
                    "description": "Template Id.",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "offset",
                    "in": "query",
                    "description": "Offset in the result set.",
                    "type": "integer",
                    "default": 0
                },
                {
                    "name": "setSize",
                    "in": "query",
                    "description": "Limit number of results.",
                    "type": "integer",
                    "default": 10
                },
                {
                    "name": "sort",
                    "in": "query",
                    "description": "User field name used to sort the results.",
                    "type": "string"
                },
                {
                    "name": "ascending",
                    "in": "query",
                    "description": "Used to order by the results based on the given sort parameter.",
                    "type": "boolean",
                    "default": true
                },
                {
                    "name": "workflows",
                    "in": "query",
                    "description": "Parameter to choose the content with a one or more workflow",
                    "type": "string",
                    "default": '["Published"]'
                },
                {
                    "name": "includeRefs",
                    "in": "query",
                    "description": "Activate to include data of referenced content nodes.",
                    "type": "boolean",
                    "default": false
                },
                {
                    "name": "removeExpired",
                    "in": "query",
                    "description": "Activate to remove expired content nodes.",
                    "type": "boolean",
                    "default": false
                },                
                {
                    "name": "query",
                    "in": "query",
                    "description": "Content of the Query property in the template",
                    "type": "string",
                    "required": true
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/ContentNode"
                    }
                }
            }
        },
    },
    "/accounts/{accountId}/content/search": {
        "post": {
            "x-swagger-router-controller": "contentNodes",
            "x-videa-resource": "contentNodes",
            "x-videa-permissions": ["view"],
            "operationId": "searchContentNode",
            "tags": ["Content Nodes"],
            "description": "Returns the list of ContentNode.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id.",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "offset",
                    "in": "query",
                    "description": "Offset in the result set.",
                    "type": "integer",
                    "default": 0
                },
                {
                    "name": "setSize",
                    "in": "query",
                    "description": "Limit number of results.",
                    "type": "integer",
                    "default": 10
                },
                {
                    "name": "sort",
                    "in": "query",
                    "description": "User field name used to sort the results.",
                    "type": "string"
                },
                {
                    "name": "ascending",
                    "in": "query",
                    "description": "Used to order by the results based on the given sort parameter.",
                    "type": "boolean",
                    "default": true
                },
                {
                    "name": "workflows",
                    "in": "query",
                    "description": "Parameter to choose the content with a one or more workflow",
                    "type": "string",
                    "default": '["Published"]'
                },
                {
                    "name": "includeRefs",
                    "in": "query",
                    "description": "Activate to include reference content nodes.",
                    "type": "boolean",
                    "default": false
                },
                {
                    "name": "removeExpired",
                    "in": "query",
                    "description": "Activate to remove expired content nodes.",
                    "type": "boolean",
                    "default": false
                },
                {
                    "name": "query",
                    "in": "body",
                    "description": "Advanced search query to filter results.",
                    "schema": {
                        "$ref": "#/definitions/SearchQuery"
                    }
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/ContentNode"
                    }
                }
            }
        }
    },
    "/accounts/{accountId}/content/search/suggestions": {
        "get": {
            "x-swagger-router-controller": "contentNodes",
            "x-videa-resource": "contentNodes",
            "x-videa-permissions": ["view"],
            "operationId": "searchSuggestions",
            "tags": ["Content Nodes"],
            "description": "Returns a list of suggestion ContentNode designations.",
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id.",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "setSize",
                    "in": "query",
                    "description": "Limit number of results.",
                    "type": "integer",
                    "default": 10
                },
                {
                    "name": "query",
                    "in": "query",
                    "description": "Text query to get type ahead suggestions..",
                    "type": "string"
                },
                {
                    "name": "workflows",
                    "in": "query",
                    "description": "States to filter the suggestions.",
                    "type": "string",
                    "default": '["Published"]'
                },
                {
                    "name": "contentTypeId",
                    "in": "query",
                    "description": "ContentTypes to filter suggestions.",
                    "type": "string"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/ContentNode"
                    }
                }
            }
        }
    },
    "/accounts/{accountId}/content/search/type/{contentTypeId}": {
        "get": {
            "x-swagger-router-controller": "contentNodes",
            "x-videa-permissions": ["view"],
            "operationId": "getContentNodesFilterByContentType",
            "tags": ["Content Nodes"],
            "description": "Get content nodes filtered by content type.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id.",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "contentTypeId",
                    "in": "path",
                    "description": "Content type Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "offset",
                    "in": "query",
                    "description": "Offset in the result set.",
                    "type": "integer",
                    "default": 0
                },
                {
                    "name": "setSize",
                    "in": "query",
                    "description": "Limit number of results.",
                    "type": "integer",
                    "default": 10
                },
                {
                    "name": "sort",
                    "in": "query",
                    "description": "User field name used to sort the results.",
                    "type": "string"
                },
                {
                    "name": "ascending",
                    "in": "query",
                    "description": "Used to order by the results based on the given sort parameter.",
                    "type": "boolean",
                    "default": true
                },
                {
                    "name": "workflows",
                    "in": "query",
                    "description": "Parameter to choose the content with a one or more workflow",
                    "type": "string",
                    "default": '["Published"]'
                },
                {
                    "name": "includeRefs",
                    "in": "query",
                    "description": "Activate to include reference content nodes.",
                    "type": "boolean",
                    "default": false
                },
                {
                    "name": "removeExpired",
                    "in": "query",
                    "description": "Activate to remove expired content nodes.",
                    "type": "boolean",
                    "default": false
                },
                {
                    "name": "query",
                    "in": "query",
                    "description": "Advanced search query to filter results.",
                    "type": "string"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/Schema"
                    }
                },
                "400": {
                    "description": "Given app doesn't have an unique Id.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    },
    "/accounts/{accountId}/content/types/{contentTypeId}/schema": {
        "get": {
            "x-swagger-router-controller": "contentNodes",
            "x-videa-permissions": ["view"],
            "operationId": "getContentTypeServiceSchema",
            "tags": ["Content Nodes"],
            "description": "Get the schema of the content node.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "contentTypeId",
                    "in": "path",
                    "description": "Content type Id",
                    "type": "string",
                    "required": true
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/Schema"
                    }
                },
                "400": {
                    "description": "Given app doesn't have an unique Id.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    },
    "/accounts/{accountId}/content/{contentId}": {
        "get": {
            "x-swagger-router-controller": "contentNodes",
            "x-videa-resource": "contentNodes",
            "x-videa-permissions": ["view"],
            "operationId": "getContentNode",
            "tags": ["Content Nodes"],
            "description": "Returns an existing ContentNode.",
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "contentId",
                    "in": "path",
                    "description": "Id of an existing ContentNode.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "workflows",
                    "in": "query",
                    "description": "Parameter to choose the content by one or more workflow states",
                    "type": "string",
                    "default": '["Published"]'
                },
                {
                    "name": "includeRefs",
                    "in": "query",
                    "description": "Activate to include reference content nodes.",
                    "type": "boolean",
                    "default": false
                },
                {
                    "name": "removeExpired",
                    "in": "query",
                    "description": "Activate to remove expired content nodes.",
                    "type": "boolean",
                    "default": false
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/ContentNode"
                    }
                },
                "400": {
                    "description": "Brand Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },
        "put": {
            "x-swagger-router-controller": "contentNodes",
            "x-videa-resource": "contentNodes",
            "x-videa-permissions": ["edit"],
            "operationId": "putContentNode",
            "tags": ["Content Nodes"],
            "description": "Modifies an existing ContentNode.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "contentId",
                    "in": "path",
                    "description": "ContentNode Id to update.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "contentNode",
                    "in": "body",
                    "description": "ContentNode attributes to modify",
                    "required": true,
                    "schema": {
                        "$ref": "#/definitions/ContentNodePartial"
                    }
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/ContentNode"
                    }
                },
                "400": {
                    "description": "ContentNode Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },
        "delete": {
            "x-swagger-router-controller": "contentNodes",
            "x-videa-resource": "contentNodes",
            "x-videa-permissions": ["delete"],
            "operationId": "removeContentNode",
            "tags": ["Content Nodes"],
            "description": "Deletes an existing ContentNode.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "contentId",
                    "in": "path",
                    "description": "Id of an existing ContentNode.",
                    "required": true,
                    "type": "string"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request."
                },
                "400": {
                    "description": "ContentNode Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    },
    "/accounts/{accountId}/content/instantiate/{contentTypeId}": {
        "get": {
            "x-swagger-router-controller": "contentNodes",
            "x-videa-resource": "contentNodes",
            "x-videa-permissions": ["view"],
            "operationId": "instantiateContent",
            "tags": ["Content Nodes"],
            "description": "Returns an empty content node instantiate of the given content type.",
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "contentTypeId",
                    "in": "path",
                    "description": "Id of an existing ContentType.",
                    "required": true,
                    "type": "string"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request returns the field value.",
                    "schema": {
                        "$ref": "#/definitions/ContentNodePartial"
                    }
                },
                "400": {
                    "description": "Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    },
    "/accounts/{accountId}/content/{contentId}/workflow/fire/{trigger}": {
        "post": {
            "x-swagger-router-controller": "contentNodes",
            "x-videa-resource": "contentNodes",
            "x-videa-permissions": ["edit"],
            "operationId": "nextState",
            "tags": ["Content Nodes"],
            "description": "Updates the workflow based on the given trigger.",
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "contentId",
                    "in": "path",
                    "description": "Id of an existing ContentNode.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "trigger",
                    "in": "path",
                    "description": "Trigger identifier to apply at the current state.",
                    "required": true,
                    "type": "string"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful updates the content node and returns the next state.",
                    "schema": {
                        "type": "string"
                    }
                },
                "400": {
                    "description": "Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    },
    "/accounts/{accountId}/content/catalogs/{catalogId}": {
        "get": {
            "x-swagger-router-controller": "contentNodes",
            "x-videa-resource": "contentNodes",
            "x-videa-permissions": ["view"],
            "operationId": "getContentNodeWithCatalog",
            "tags": ["Content Nodes"],
            "description": "Returns the list of ContentNode with catalogId.",
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "catalogId",
                    "in": "path",
                    "description": "Id of an existing Catalog.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "offset",
                    "in": "query",
                    "description": "Offset in the result set.",
                    "type": "integer",
                    "default": 0
                },
                {
                    "name": "setSize",
                    "in": "query",
                    "description": "Limit number of results.",
                    "type": "integer",
                    "default": 10
                },
                {
                    "name": "sort",
                    "in": "query",
                    "description": "User field name used to sort the results.",
                    "type": "string"
                },
                {
                    "name": "ascending",
                    "in": "query",
                    "description": "Used to order by the results based on the given sort parameter.",
                    "type": "boolean",
                    "default": true
                },
                {
                    "name": "includeRefs",
                    "in": "query",
                    "description": "Activate to include reference content nodes.",
                    "type": "boolean",
                    "default": false
                },
                {
                    "name": "query",
                    "in": "query",
                    "description": "Advanced search query to filter results.",
                    "type": "string"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/ContentNode"
                    }
                },
                "400": {
                    "description": "Brand Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    },
    "/accounts/{accountId}/content/catalogs/{catalogId}/collections/{collectionId}": {
        "get": {
            "x-swagger-router-controller": "contentNodes",
            "x-videa-resource": "contentNodes",
            "x-videa-permissions": ["view"],
            "operationId": "getContentNodesWithCatalogsAndCollections",
            "tags": ["Content Nodes"],
            "description": "Returns the list of ContentNode with catalogId and inside of collection with id collectionId..",
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "catalogId",
                    "in": "path",
                    "description": "Id of an existing Catalog.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "collectionId",
                    "in": "path",
                    "description": "Id of an existing collection.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "offset",
                    "in": "query",
                    "description": "Offset in the result set.",
                    "type": "integer",
                    "default": 0
                },
                {
                    "name": "setSize",
                    "in": "query",
                    "description": "Limit number of results.",
                    "type": "integer",
                    "default": 10
                },
                {
                    "name": "sort",
                    "in": "query",
                    "description": "User field name used to sort the results.",
                    "type": "string"
                },
                {
                    "name": "ascending",
                    "in": "query",
                    "description": "Used to order by the results based on the given sort parameter.",
                    "type": "boolean",
                    "default": true
                },
                {
                    "name": "includeRefs",
                    "in": "query",
                    "description": "Activate to include reference content nodes.",
                    "type": "boolean",
                    "default": false
                },
                {
                    "name": "query",
                    "in": "query",
                    "description": "Advanced search query to filter results.",
                    "type": "string"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/ContentNode"
                    }
                },
                "400": {
                    "description": "Brand Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    },
    "/accounts/{accountId}/content/collections/{collectionId}": {
        "get": {
            "x-swagger-router-controller": "contentNodes",
            "x-videa-resource": "contentNodes",
            "x-videa-permissions": ["view"],
            "operationId": "getContentNodesWithCollection",
            "tags": ["Content Nodes"],
            "description": "Returns ContentNodes of a Collection.",
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "collectionId",
                    "in": "path",
                    "description": "Id of an existing collection.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "offset",
                    "in": "query",
                    "description": "Offset in the result set.",
                    "type": "integer",
                    "default": 0
                },
                {
                    "name": "setSize",
                    "in": "query",
                    "description": "Limit number of results.",
                    "type": "integer",
                    "default": 10
                },
                {
                    "name": "sort",
                    "in": "query",
                    "description": "User field name used to sort the results.",
                    "type": "string"
                },
                {
                    "name": "ascending",
                    "in": "query",
                    "description": "Used to order by the results based on the given sort parameter.",
                    "type": "boolean",
                    "default": true
                },
                {
                    "name": "workflows",
                    "in": "query",
                    "description": "Parameter to choose the content by one or more workflow states",
                    "type": "string",
                    "default": '["Published"]'
                },
                {
                    "name": "includeRefs",
                    "in": "query",
                    "description": "Activate to include reference content nodes.",
                    "type": "boolean",
                    "default": false
                },
                {
                    "name": "removeExpired",
                    "in": "query",
                    "description": "Activate to remove expired content nodes.",
                    "type": "boolean",
                    "default": false
                },
                {
                    "name": "query",
                    "in": "query",
                    "description": "Advanced search query to filter results.",
                    "type": "string"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/ContentNode"
                    }
                },
                "400": {
                    "description": "Brand Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    }
};
