module.exports = {
	"/schema/{schemaId}": {
		"get": {
			"x-swagger-router-controller": "schema",
			"operationId": "getSchema",
			"tags": ["Schema"],
			"description": "Get a schema.",
			"security": [{
				"videa_auth": []
			}],
			"parameters": [
				{
					"name": "schemaId",
					"in": "path",
					"description": "Schema Id",
					"type": "string",
					"required": true
				}
			],
			"responses": {
				"default": {
					"description": "Invalid request.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				},
				"200": {
					"description": "Successful request.",
					"schema": {
						"$ref": "#/definitions/Schema"
					}
				},
				"400": {
					"description": "Given app doesn't have an unique Id.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				}
			}
		}
	},
	"/schema/asset/{assetKey}/delimiter/{delimiter}": {
		"get": {
			"x-swagger-router-controller": "schema",
			"operationId": "getSchemaFile",
			"tags": ["Schema"],
			"description": "Get a schema of file.",
			"security": [{
				"videa_auth": []
			}],
			"parameters": [
				{
					"name": "assetKey",
					"in": "path",
					"description": "Asset key of file to get schema.",
					"required": true,
					"type": "string"
				},
				{
					"name": "delimiter",
					"in": "path",
					"description": "Delimiter for parsing file.",
					"required": true,
					"type": "string"
				}
			],
			"responses": {
				"default": {
					"description": "Invalid request.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				},
				"200": {
					"description": "Successful request.",
					"schema": {
						"$ref": "#/definitions/Schema"
					}
				},
				"400": {
					"description": "Given app doesn't have an unique Id.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				}
			}
		}
	},
	"/schema/" : {
		"post": {
			"x-swagger-router-controller": "schema",
			"operationId": "getSchemaFromJSON",
			"tags": ["Schema"],
			"description": "Get schema from JSONs",
			"security": [{
				"videa_auth": []
			}],
			"parameters": [
				{
					"name": "data",
					"in": "body",
					"description": "Data and settings",
					"required": true,
					"schema" : {
						"$ref": "#/definitions/ContentIngestDataWithSettings"
					}
				}
			],
			"responses": {
				"default": {
					"description": "Invalid request.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				},
				"200": {
					"description": "Successful request.",
					"schema": {
						"$ref": "#/definitions/Schema"
					}
				},
				"400": {
					"description": "Given app doesn't have an unique Id.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				}
			}
		}
	} 
};
