module.exports = {
	"/filemanager": {
		"post": {
			"x-swagger-router-controller": "fileManagers",
			"x-videa-resource" : "fileManager",
			"x-videa-permissions" : ["edit"],
			"operationId": "uploadFile",
			"tags": ["FileManager"],
			"description": "Upload a new file to amazon.",
			"security": [{
				"videa_auth": []
			}],
			"parameters": [
				{
					"name": "file",
					"in": "formData",
					"description": "File to upload.",
					"required": true,
					"type": "file"
				}
			],
			"consumes": ["multipart/form-data"],
			"responses": {
				"default": {
					"description": "Invalid request.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				},
				"200": {
					"description": "Successful.",
					"schema": {
						"type": "string"
					}
				},
				"400": {
					"description": "Id not found.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				}
			}
		}
	}
};
