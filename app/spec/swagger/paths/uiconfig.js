module.exports = {
    "/accounts/{accountId}/editions/{editionId}/configs": {
        "post": {
            "x-swagger-router-controller": "uiConfigs",
            "operationId": "createConfig",
            "tags": ["UiConfig"],
            "description": "Adds a new Config.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [{
                "name": "accountId",
                "in": "path",
                "description": "Account Id",
                "type": "string",
                "required": true
            }, {
                "name": "editionId",
                "in": "path",
                "description": "Edition Id",
                "type": "string",
                "required": true
            }, {
                "name": "config",
                "in": "body",
                "description": "Name of Config.",
                "required": true,
                "schema": {
                    "$ref": "#/definitions/Config"
                }
            }],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/Config"
                    }
                },
                "400": {
                    "description": "Given Edition Config doesn't have an unique Id.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    },
    "/accounts/{accountId}/editions/{editionId}/configs/{configId}": {
        "get": {
            "x-swagger-router-controller": "uiConfigs",
            "operationId": "getConfig",
            "tags": ["UiConfig"],
            "description": "Returns config Specified.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [{
                "name": "accountId",
                "in": "path",
                "description": "Account Id",
                "type": "string",
                "required": true
            }, {
                "name": "editionId",
                "in": "path",
                "description": "Edition Id",
                "type": "string",
                "required": true
            }, {
                "name": "configId",
                "in": "path",
                "description": "Config Id",
                "type": "string",
                "required": true
            }],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/Config"
                    }
                }
            }
        },
        "put": {
            "x-swagger-router-controller": "uiConfigs",
            "operationId": "editConfig",
            "tags": ["UiConfig"],
            "description": "Edit config name Specified.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [{
                "name": "accountId",
                "in": "path",
                "description": "Account Id",
                "type": "string",
                "required": true
            }, {
                "name": "editionId",
                "in": "path",
                "description": "Edition Id",
                "type": "string",
                "required": true
            }, {
                "name": "configId",
                "in": "path",
                "description": "Config Id",
                "type": "string",
                "required": true
            }, {
                "name": "config",
                "in": "body",
                "description": "Name of Config.",
                "required": true,
                "schema": {
                    "$ref": "#/definitions/Config"
                }
            }],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/UiConfig"
                    }
                }
            }
        },
        "delete": {
            "x-swagger-router-controller": "uiConfigs",
            "operationId": "deleteConfig",
            "tags": ["UiConfig"],
            "description": "Delete config Specified.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [{
                "name": "accountId",
                "in": "path",
                "description": "Account Id",
                "type": "string",
                "required": true
            }, {
                "name": "editionId",
                "in": "path",
                "description": "Edition Id",
                "type": "string",
                "required": true
            }, {
                "name": "configId",
                "in": "path",
                "description": "Config Id",
                "type": "string",
                "required": true
            }],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request."
                },
                "400": {
                    "description": "Edition Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    },
    "/accounts/{accountId}/editions/{editionId}/configs/{configId}/duplicate": {
        "post": {
            "x-swagger-router-controller": "uiConfigs",
            "operationId": "duplicateConfig",
            "tags": ["UiConfig"],
            "description": "Duplicate UIConfig.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [{
                "name": "accountId",
                "in": "path",
                "description": "Account Id",
                "type": "string",
                "required": true
            }, {
                "name": "editionId",
                "in": "path",
                "description": "Edition Id",
                "type": "string",
                "required": true
            }, {
                "name": "configId",
                "in": "path",
                "description": "Config Id",
                "type": "string",
                "required": true
            }],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/UiConfig"
                    }
                }
            }
        }
    }
};