module.exports = {
	"/accounts/{accountId}/ingest/asset/{assetKey}": {
		"post": {
			"x-swagger-router-controller": "contentIngest",
			"operationId": "batchIngest",
			"tags": ["Content Ingest"],
			"description": "Ingest content from file.",
			"security": [{
				"videa_auth": []
			}],
			"parameters": [
				{
					"name": "accountId",
					"in": "path",
					"description": "Account Id",
					"type": "string",
					"required": true
				},
				{
					"name": "assetKey",
					"in": "path",
					"description": "Asset key",
					"type": "string",
					"required": true
				},
                {
                    "name": "policy",
                    "in": "query",
                    "description": "Policy to Add, Update or Replace.",
                    "type": "string",
                    "default": "Add"
                },
				{
					"name": "settings",
					"in": "body",
					"description": "Settings",
					"required": true,
					"schema" : {
						"$ref": "#/definitions/ContentIngestSettings"
					}
				}
			],
			"responses": {
				"default": {
					"description": "Invalid request.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				},
				"200": {
					"description": "Successful request.",
					"schema": {
						"$ref": "#/definitions/Worker"
					}
				},
				"400": {
					"description": "Given app doesn't have an unique Id.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				}
			}
		}
	},
	"/accounts/{accountId}/ingest/mappings/{mappingId}": {
		"post": {
			"x-swagger-router-controller": "contentIngest",
			"operationId": "batchIngestJSON",
			"tags": ["Content Ingest"],
			"description": "Ingest content from file.",
			"security": [{
				"videa_auth": []
			}],
			"parameters": [
				{
					"name": "accountId",
					"in": "path",
					"description": "Account Id",
					"type": "string",
					"required": true
				},
				{
					"name": "mappingId",
					"in": "path",
					"description": "Mapping Id",
					"type": "string",
					"required": true
				},
                {
                    "name": "policy",
                    "in": "query",
                    "description": "Policy to Add, Update or Replace.",
                    "type": "string",
                    "default": "Add"
                },
				{
					"name": "content",
					"in": "body",
					"description": "Data and settings",
					"required": true,
					"schema" : {
						"$ref": "#/definitions/ContentIngestDataWithSettings"
					}
				}
			],
			"responses": {
				"default": {
					"description": "Invalid request.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				},
				"200": {
					"description": "Successful request.",
					"schema": {
						"$ref": "#/definitions/Worker"
					}
				},
				"400": {
					"description": "Given app doesn't have an unique Id.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				}
			}
		}
	}
};
