module.exports = {
    "/accounts/{accountId}/members": {
        "get": {
            "x-swagger-router-controller": "accountMembers",
            "x-videa-resource" : "members",
            "x-videa-permissions" : ["view"],
            "operationId": "getMembers",
            "tags": ["Account Members"],
            "description": "Returns the list of account members.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "offset",
                    "in": "query",
                    "description": "Offset in the result set",
                    "type": "integer",
                    "default": 0
                },
                {
                    "name": "setSize",
                    "in": "query",
                    "description": "Limit number of results",
                    "type": "integer",
                    "default": 10
                },
                {
                    "name": "sort",
                    "in": "query",
                    "description": "User field name used to sort the results",
                    "type": "string"
                },
                {
                    "name": "ascending",
                    "in": "query",
                    "description": "Used to order by the results based on the given sort parameter",
                    "type": "boolean",
                    "default": true
                },
				{
					"name": "query",
					"in": "query",
					"description": "Search query to filter results.",
					"type": "string"
				}
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/AccountMember"
                    }
                }
            }
        },
        "post": {
            "x-swagger-router-controller": "accountMembers",
            "x-videa-resource" : "members",
            "x-videa-permissions" : ["edit"],
            "operationId": "postMember",
            "tags": ["Account Members"],
            "description": "Creates a new user and make him a member of the selected account.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "member",
                    "in": "body",
                    "description": "Account member to add. The Id and dates will be filled by the service and should not be sent.",
                    "required": true,
                    "schema": {
                        "$ref": "#/definitions/AccountMemberPartial"
                    }
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/AccountMember"
                    }
                },
                "400": {
                    "description": "Given account member doesn't have an unique Id.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    },
    "/accounts/{accountId}/members/{userId}": {
        "get": {
            "x-swagger-router-controller": "accountMembers",
            "x-videa-resource" : "members",
            "x-videa-permissions" : ["view"],
            "operationId": "getMember",
            "tags": ["Account Members"],
            "description": "Returns an existing account member.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "userId",
                    "in": "path",
                    "description": "Id of an existing account member.",
                    "required": true,
                    "type": "string"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/AccountMember"
                    }
                },
                "400": {
                    "description": "Member Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },
        "post": {
            "x-swagger-router-controller": "accountMembers",
            "x-videa-resource" : "members",
            "x-videa-permissions" : ["edit"],
            "operationId": "postExistingMember",
            "tags": ["Account Members"],
            "description": "Adds an existing user and make him a member of the selected account.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "userId",
                    "in": "path",
                    "description": "User Id to add as a member.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "member",
                    "in": "body",
                    "description": "Member properties to set. The user property will be filled by the service",
                    "required": true,
                    "schema": {
                        "$ref": "#/definitions/AccountMemberPartial"
                    }
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/AccountMember"
                    }
                },
                "400": {
                    "description": "Given account member doesn't have an unique Id.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },
        "put": {
            "x-swagger-router-controller": "accountMembers",
            "x-videa-resource" : "members",
            "x-videa-permissions" : ["edit"],
            "operationId": "putMember",
            "tags": ["Account Members"],
            "description": "Modifies an existing account member.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "userId",
                    "in": "path",
                    "description": "User Id to update.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "member",
                    "in": "body",
                    "description": "AccountMember attributes to modify",
                    "required": true,
                    "schema": {
                        "$ref": "#/definitions/AccountMemberPartial"
                    }
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/AccountMember"
                    }
                },
                "400": {
                    "description": "Member Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },
        "delete": {
            "x-swagger-router-controller": "accountMembers",
            "x-videa-resource" : "members",
            "x-videa-permissions" : ["delete"],
            "operationId": "removeMember",
            "tags": ["Account Members"],
            "description": "Deletes an existing account member.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "userId",
                    "in": "path",
                    "description": "Id of an existing account member.",
                    "required": true,
                    "type": "string"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request."
                },
                "400": {
                    "description": "Member Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    }
};
