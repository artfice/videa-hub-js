module.exports = {
    "/accounts/{accountId}/defaultdata": {
        "post": {
            "x-swagger-router-controller": "defaultData",
            "operationId": "postDefaultData",
            "tags": ["Default Data"],
            "description": "Returns the list of ContentType.",
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request."
                }
            }
        }
    },
	"/accounts/{accountId}/sampledata": {
		"post": {
			"x-swagger-router-controller": "defaultData",
			"operationId": "postSampleData",
			"tags": ["Default Data"],
			"description": "Sets account with default sample data.",
			"parameters": [
				{
					"name": "accountId",
					"in": "path",
					"description": "Account Id",
					"type": "string",
					"required": true
				}
			],
			"responses": {
				"default": {
					"description": "Invalid request.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				},
				"200": {
					"description": "Successful request."
				}
			}
		}
	}
};
