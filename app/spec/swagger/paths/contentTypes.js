module.exports = {
	"/accounts/{accountId}/types": {
		"get": {
			"x-swagger-router-controller": "contentTypes",
			"x-videa-resource" : "contentTypes",
			"x-videa-permissions" : ["view"],
			"operationId": "getContentTypes",
			"tags": ["ContentTypes"],
			"description": "Returns the list of ContentType.",
            "security": [{
                "videa_auth": []
            }],
			"parameters": [
				{
					"name": "accountId",
					"in": "path",
					"description": "Account Id",
					"type": "string",
					"required": true
				},
				{
					"name": "offset",
					"in": "query",
					"description": "Offset in the result set",
					"type": "integer",
					"default": 0
				},
				{
					"name": "setSize",
					"in": "query",
					"description": "Limit number of results",
					"type": "integer",
					"default": 10
				},
				{
					"name": "sort",
					"in": "query",
					"description": "User field name used to sort the results",
					"type": "string"
				},
				{
					"name": "ascending",
					"in": "query",
					"description": "Used to order by the results based on the given sort parameter",
					"type": "boolean",
					"default": true
				},
				{
					"name": "query",
					"in": "query",
					"description": "Search query to filter results.",
					"type": "string"
				}
			],
			"responses": {
				"default": {
					"description": "Invalid request.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				},
				"200": {
					"description": "Successful request.",
					"schema": {
						"$ref": "#/definitions/ContentType"
					}
				}
			}
		},
		"post": {
			"x-swagger-router-controller": "contentTypes",
			"x-videa-resource" : "contentTypes",
			"x-videa-permissions" : ["edit"],
			"operationId": "postContentType",
			"tags": ["ContentTypes"],
			"description": "Adds a new ContentType.",
			"security": [{
				"videa_auth": []
			}],
			"parameters": [
				{
					"name": "accountId",
					"in": "path",
					"description": "Account Id",
					"type": "string",
					"required": true
				},
				{
					"name": "contentType",
					"in": "body",
					"description": "ContentType data to add. The Id and dates will be filled by the service and should not be sent.",
					"required": true,
					"schema": {
						"$ref": "#/definitions/ContentType"
					}
				}
			],
			"responses": {
				"default": {
					"description": "Invalid request.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				},
				"200": {
					"description": "Successful request.",
					"schema": {
						"$ref": "#/definitions/ContentType"
					}
				},
				"400": {
					"description": "Given brand doesn't have an unique Id.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				}
			}
		}
	},
	"/accounts/{accountId}/types/{contentTypeId}": {
		"get": {
			"x-swagger-router-controller": "contentTypes",
			"x-videa-resource" : "contentTypes",
			"x-videa-permissions" : ["view"],
			"operationId": "getContentType",
			"tags": ["ContentTypes"],
			"description": "Returns an existing ContentType.",
			"parameters": [
				{
					"name": "accountId",
					"in": "path",
					"description": "Account Id",
					"type": "string",
					"required": true
				},
				{
					"name": "contentTypeId",
					"in": "path",
					"description": "Id of an existing ContentType.",
					"required": true,
					"type": "string"
				}
			],
			"responses": {
				"default": {
					"description": "Invalid request.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				},
				"200": {
					"description": "Successful request.",
					"schema": {
						"$ref": "#/definitions/ContentType"
					}
				},
				"400": {
					"description": "Brand Id not found.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				}
			}
		},
		"put": {
			"x-swagger-router-controller": "contentTypes",
			"x-videa-resource" : "contentTypes",
			"x-videa-permissions" : ["edit"],
			"operationId": "putContentType",
			"tags": ["ContentTypes"],
			"description": "Modifies an existing ContentType.",
			"security": [{
				"videa_auth": []
			}],
			"parameters": [
				{
					"name": "accountId",
					"in": "path",
					"description": "Account Id",
					"type": "string",
					"required": true
				},
				{
					"name": "contentTypeId",
					"in": "path",
					"description": "ContentType Id to update.",
					"required": true,
					"type": "string"
				},
				{
					"name": "contentType",
					"in": "body",
					"description": "ContentType attributes to modify",
					"required": true,
					"schema": {
						"$ref": "#/definitions/ContentTypePartial"
					}
				}
			],
			"responses": {
				"default": {
					"description": "Invalid request.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				},
				"200": {
					"description": "Successful request.",
					"schema": {
						"$ref": "#/definitions/ContentType"
					}
				},
				"400": {
					"description": "ContentType Id not found.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				}
			}
		},
		"delete": {
			"x-swagger-router-controller": "contentTypes",
			"x-videa-resource" : "contentTypes",
			"x-videa-permissions" : ["delete"],
			"operationId": "removeContentType",
			"tags": ["ContentTypes"],
			"description": "Deletes an existing ContentType.",
			"security": [{
				"videa_auth": []
			}],
			"parameters": [
				{
					"name": "accountId",
					"in": "path",
					"description": "Account Id",
					"type": "string",
					"required": true
				},
				{
					"name": "contentTypeId",
					"in": "path",
					"description": "Id of an existing ContentType.",
					"required": true,
					"type": "string"
				}
			],
			"responses": {
				"default": {
					"description": "Invalid request.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				},
				"200": {
					"description": "Successful request."
				},
				"400": {
					"description": "ContentType Id not found.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				}
			}
		}
	},
	"/accounts/{accountId}/types/{contentTypeId}/fields": {
		"get": {
			"x-swagger-router-controller": "contentTypes",
			"x-videa-resource" : "contentTypes",
			"x-videa-permissions" : ["view"],
			"operationId": "getContentTypeFields",
			"tags": ["ContentTypes"],
			"description": "Returns a list of ContentFields that belong to the given ContentType.",
			"parameters": [
				{
					"name": "accountId",
					"in": "path",
					"description": "Account Id",
					"type": "string",
					"required": true
				},
				{
					"name": "contentTypeId",
					"in": "path",
					"description": "Id of an existing ContentType.",
					"required": true,
					"type": "string"
				}
			],
			"responses": {
				"default": {
					"description": "Invalid request.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				},
				"200": {
					"description": "Successful request.",
					"schema": {
						"type": "array",
						"items": {
							"$ref": "#/definitions/ContentField"
						}
					}
				},
				"400": {
					"description": "Id not found.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				}
			}
		},
		"put" : {
			"x-swagger-router-controller": "contentTypes",
			"x-videa-resource" : "contentTypes",
			"x-videa-permissions" : ["edit"],
			"operationId": "updateOrderContentTypeFields",
			"tags": ["ContentTypes"],
			"description": "Updates the order of the list of ContentFields  that belong to the given ContentType",
			"security": [{
				"videa_auth": []
			}],
			"parameters": [
				{
					"name": "accountId",
					"in": "path",
					"description": "Account Id",
					"type": "string",
					"required": true
				},
				{
					"name": "contentTypeId",
					"in": "path",
					"description": "Id of an existing ContentType.",
					"required": true,
					"type": "string"
				},
				{
					"name": "contentFieldGroup",
					"in": "query",
					"description": "Name of the contentField group.",
					"required": true,
					"type": "string"
				},
				{
					"name" : "contentFields",
					"in" : "body",
					"description" : "Array of contentFields to update",
					"required" : true,
					"schema": {
						"type": "array",
						"items": {
							"$ref": "#/definitions/ContentField"
						}
					}
				}
			],
			"responses": {
				"default": {
					"description": "Invalid request.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				},
				"200": {
					"description": "Successful request."
				},
				"400": {
					"description": "Id not found.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				}
			}
		}
	},
	"/accounts/{accountId}/types/{contentTypeId}/possibleNewFields": {
		"get": {
			"x-swagger-router-controller": "contentTypes",
			"x-videa-resource" : "contentTypes",
			"x-videa-permissions" : ["view"],
			"operationId": "getPossibleNewContentTypeFields",
			"tags": ["ContentTypes"],
			"description": "Returns a list of ContentFields that belongs to the account but are not in the contentType.",
			"parameters": [
				{
					"name": "accountId",
					"in": "path",
					"description": "Account Id",
					"type": "string",
					"required": true
				},
				{
					"name": "contentTypeId",
					"in": "path",
					"description": "Id of an existing ContentType.",
					"required": true,
					"type": "string"
				}
			],
			"responses": {
				"default": {
					"description": "Invalid request.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				},
				"200": {
					"description": "Successful request.",
					"schema": {
						"type": "array",
						"items": {
							"$ref": "#/definitions/ContentField"
						}
					}
				},
				"400": {
					"description": "Id not found.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				}
			}
		}
	},
	"/accounts/{accountId}/types/{contentTypeId}/fields/{contentFieldId}": {
		"post": {
			"x-swagger-router-controller": "contentTypes",
			"x-videa-resource" : "contentTypes",
			"x-videa-permissions" : ["edit"],
			"operationId": "addContentTypeField",
			"tags": ["ContentTypes"],
			"description": "Add the content field to the given content field group.",
			"security": [{
				"videa_auth": []
			}],
			"parameters": [
				{
					"name": "accountId",
					"in": "path",
					"description": "Account Id",
					"type": "string",
					"required": true
				},
				{
					"name": "contentTypeId",
					"in": "path",
					"description": "Id of an existing ContentType.",
					"required": true,
					"type": "string"
				},
				{
					"name": "contentFieldId",
					"in": "path",
					"description": "Id of an existing ContentField.",
					"required": true,
					"type": "string"
				},
				{
					"name": "contentFieldGroup",
					"in": "query",
					"description": "Name of the contentField group.",
					"required": true,
					"type": "string"
				}
			],
			"responses": {
				"default": {
					"description": "Invalid request.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				},
				"200": {
					"description": "Successful request."
				},
				"400": {
					"description": "Id not found.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				}
			}
		},
		"delete": {
			"x-swagger-router-controller": "contentTypes",
			"x-videa-resource" : "contentTypes",
			"x-videa-permissions" : ["delete"],
			"operationId": "removeContentTypeField",
			"tags": ["ContentTypes"],
			"description": "Remove the all references to the given contentFieldId on given content type.",
			"security": [{
				"videa_auth": []
			}],
			"parameters": [
				{
					"name": "accountId",
					"in": "path",
					"description": "Account Id",
					"type": "string",
					"required": true
				},
				{
					"name": "contentTypeId",
					"in": "path",
					"description": "Id of an existing ContentType.",
					"required": true,
					"type": "string"
				},
				{
					"name": "contentFieldId",
					"in": "path",
					"description": "Id of an existing ContentField.",
					"required": true,
					"type": "string"
				}
			],
			"responses": {
				"default": {
					"description": "Invalid request.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				},
				"200": {
					"description": "Successful request."
				},
				"400": {
					"description": "Id not found.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				}
			}
		}
	}
};
