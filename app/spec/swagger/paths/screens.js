/**
 * Created by bardiakhosravi on 2015-11-29.
 */

module.exports = {
    "/accounts/{accountId}/configs/{configId}/screens": {
        "get": {
            "x-swagger-router-controller": "screens",
            "operationId": "getScreens",
            "tags": ["Screens"],
            "description": "Get a list of screens for the provided config",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "configId",
                    "in": "path",
                    "description": "Config Id",
                    "type": "string",
                    "required": true
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "type": "array",
                        "items": {
                            "$ref": "#/definitions/Screen"
                        }
                    }
                }
            }
        },

        "post": {
            "x-swagger-router-controller": "screens",
            "operationId": "createScreen",
            "tags": ["Screens"],
            "description": "Create a new screen for an app config",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "configId",
                    "in": "path",
                    "description": "Config Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "screen",
                    "in": "body",
                    "description": "Screen object",
                    "required": true,
                    "schema": {
                        "$ref": "#/definitions/Screen"
                    }
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/Screen"
                    }
                },
                "400": {
                    "description": "Given App Config doesn't have an unique Id.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },

        "put": {
            "x-swagger-router-controller": "screens",
            "operationId": "updateScreen",
            "tags": ["Screens"],
            "description": "Create a new screen for an app config",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "configId",
                    "in": "path",
                    "description": "Config Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "screen",
                    "in": "body",
                    "description": "Screen object",
                    "required": true,
                    "schema": {
                        "$ref": "#/definitions/Screen"
                    }
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/Screen"
                    }
                },
                "400": {
                    "description": "Given App Config doesn't have an unique Id.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    },

    "/accounts/{accountId}/configs/{configId}/screens/{screenId}": {
        "delete": {
            "x-swagger-router-controller": "screens",
            "operationId": "deleteScreen",
            "tags": ["Screens"],
            "description": "Deletes a given screen",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "configId",
                    "in": "path",
                    "description": "Config Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "screenId",
                    "in": "path",
                    "type": "string",
                    "description": "Id of a screen",
                    "required": true
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request."
                },
                "400": {
                    "description": "Error deleting screen",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    }
};