module.exports = {
    "/system/version": {
        "get": {
            "x-swagger-router-controller": "system",
            "operationId": "getVersion",
            "tags": ["System"],
            "description": "Get the version.",
            "parameters": [],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/Version"
                    }
                }
            }
        }
    },
    "/version/current": {
        "get": {
            "x-swagger-router-controller": "system",
            "operationId": "getVersionString",
            "tags": ["System"],
            "description": "Get the version string.",
            "parameters": [],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/VersionStringResponse"
                    }
                }
            }
        }
    },
    "/licenses": {
        "get": {
            "x-swagger-router-controller": "system",
            "operationId": "getLicenses",
            "tags": ["System"],
            "description": "Get Open Source licenses.",
            "parameters": [],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/VersionStringResponse"
                    }
                }
            }
        }
    }
};
