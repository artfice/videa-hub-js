module.exports = {
    "/accounts/{accountId}/fields": {
        "get": {
            "x-swagger-router-controller": "contentFields",
			"x-videa-resource" : "contentFields",
			"x-videa-permissions" : ["view"],
            "operationId": "getContentFields",
            "tags": ["ContentFields"],
            "description": "Returns the list of ContentField.",
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "offset",
                    "in": "query",
                    "description": "Offset in the result set",
                    "type": "integer",
                    "default": 0
                },
                {
                    "name": "setSize",
                    "in": "query",
                    "description": "Limit number of results",
                    "type": "integer",
                    "default": 10
                },
                {
                    "name": "sort",
                    "in": "query",
                    "description": "User field name used to sort the results",
                    "type": "string"
                },
                {
                    "name": "ascending",
                    "in": "query",
                    "description": "Used to order by the results based on the given sort parameter",
                    "type": "boolean",
                    "default": true
                },
                {
                    "name": "query",
                    "in": "body",
                    "description": "Query object which will be used to search for results. If the query is not supplied the MatchAll criteria will be used.",
                    "schema": {
                        "$ref": "#/definitions/SearchQuery"
                    }
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/ContentField"
                    }
                }
            }
        },
        "post": {
            "x-swagger-router-controller": "contentFields",
			"x-videa-resource" : "contentFields",
			"x-videa-permissions" : ["edit"],
            "operationId": "postContentField",
            "tags": ["ContentFields"],
            "description": "Adds a new ContentField.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "contentField",
                    "in": "body",
                    "description": "ContentField data to add. The Id and dates will be filled by the service and should not be sent.",
                    "required": true,
                    "schema": {
                        "$ref": "#/definitions/ContentField"
                    }
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/ContentField"
                    }
                },
                "400": {
                    "description": "Given brand doesn't have an unique Id.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },
		"delete": {
			"x-swagger-router-controller": "contentFields",
			"x-videa-resource" : "contentFields",
			"x-videa-permissions" : ["delete"],
			"operationId": "removeContentFields",
			"tags": ["ContentFields"],
			"description": "Deletes existing ContentFields.",
			"security": [{
				"videa_auth": []
			}],
			"parameters": [
				{
					"name": "accountId",
					"in": "path",
					"description": "Account Id",
					"type": "string",
					"required": true
				},
				{
					"name": "contentFieldIds",
					"in": "body",
					"description": "Id of existing ContentFields.",
					"required": true,
					"schema": {
						"type": "array",
						"items": {
							"type": "string"
						}
					}
				}
			],
			"responses": {
				"default": {
					"description": "Invalid request.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				},
				"200": {
					"description": "Successful request."
				},
				"400": {
					"description": "ContentField Id not found.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				}
			}
		}
    },
    "/accounts/{accountId}/fields/types": {
        "get": {
            "x-swagger-router-controller": "contentFields",
			"x-videa-resource" : "contentFields",
			"x-videa-permissions" : ["view"],
            "operationId": "getContentFieldsTypes",
            "tags": ["ContentFields"],
            "description": "Returns the list of ContentField types.",
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "type": "array",
                        "items": {
                            "type": "string"
                        }
                    }
                }
            }
        }
    },
    "/accounts/{accountId}/fields/{contentFieldId}": {
        "get": {
            "x-swagger-router-controller": "contentFields",
			"x-videa-resource" : "contentFields",
			"x-videa-permissions" : ["view"],
            "operationId": "getContentField",
            "tags": ["ContentFields"],
            "description": "Returns an existing ContentField.",
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "contentFieldId",
                    "in": "path",
                    "description": "Id of an existing ContentField.",
                    "required": true,
                    "type": "string"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/ContentField"
                    }
                },
                "400": {
                    "description": "Brand Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },
        "put": {
            "x-swagger-router-controller": "contentFields",
			"x-videa-resource" : "contentFields",
			"x-videa-permissions" : ["edit"],
            "operationId": "putContentField",
            "tags": ["ContentFields"],
            "description": "Modifies an existing ContentField.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "contentFieldId",
                    "in": "path",
                    "description": "ContentField Id to update.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "contentField",
                    "in": "body",
                    "description": "ContentField attributes to modify",
                    "required": true,
                    "schema": {
                        "$ref": "#/definitions/ContentFieldPartial"
                    }
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/ContentField"
                    }
                },
                "400": {
                    "description": "ContentField Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },
        "delete": {
            "x-swagger-router-controller": "contentFields",
			"x-videa-resource" : "contentFields",
			"x-videa-permissions" : ["delete"],
            "operationId": "removeContentField",
            "tags": ["ContentFields"],
            "description": "Deletes an existing ContentField.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "contentFieldId",
                    "in": "path",
                    "description": "Id of an existing ContentField.",
                    "required": true,
                    "type": "string"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request."
                },
                "400": {
                    "description": "ContentField Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    }
};
