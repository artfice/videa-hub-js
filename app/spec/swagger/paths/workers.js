module.exports = {
	"/workers": {
		"get": {
			"x-swagger-router-controller": "workers",
			"operationId": "getWorkers",
			"tags": ["Workers"],
			"description": "Returns the list of workers.",
			"parameters": [
				{
					"name": "offset",
					"in": "query",
					"description": "Offset in the result set",
					"type": "integer",
					"default": 0
				},
				{
					"name": "setSize",
					"in": "query",
					"description": "Limit number of results",
					"type": "integer",
					"default": 10
				},
				{
					"name": "sort",
					"in": "query",
					"description": "User field name used to sort the results",
					"type": "string"
				},
				{
					"name": "ascending",
					"in": "query",
					"description": "Used to order by the results based on the given sort parameter",
					"type": "boolean",
					"default": true
				}
			],
			"responses": {
				"default": {
					"description": "Invalid request.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				},
				"200": {
					"description": "Successful request.",
					"schema": {
						"$ref": "#/definitions/Worker"
					}
				}
			}
		},
		"post": {
			"x-swagger-router-controller": "workers",
			"operationId": "postWorker",
			"tags": ["Workers"],
			"description": "Adds a new worker.",
			"parameters": [
				{
					"name": "worker",
					"in": "body",
					"description": "Worker data to add. The Id should be unique otherwise an error will be thrown.",
					"required": true,
					"schema": {
						"$ref": "#/definitions/Worker"
					}
				}
			],
			"responses": {
				"default": {
					"description": "Invalid request.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				},
				"200": {
					"description": "Successful request.",
					"schema": {
						"$ref": "#/definitions/Worker"
					}
				},
				"400": {
					"description": "Given worker doesn't have an unique Id.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				}
			}
		}
	},
	"/workers/{workerId}": {
		"get": {
			"x-swagger-router-controller": "workers",
			"operationId": "getWorker",
			"tags": ["Workers"],
			"description": "Returns an existing worker.",
			"parameters": [
				{
					"name": "workerId",
					"in": "path",
					"description": "Id of an existing worker.",
					"required": true,
					"type": "string"
				}
			],
			"responses": {
				"default": {
					"description": "Invalid request.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				},
				"200": {
					"description": "Successful request.",
					"schema": {
						"$ref": "#/definitions/Worker"
					}
				},
				"400": {
					"description": "Worker Id not found.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				}
			}
		}
	}
};
