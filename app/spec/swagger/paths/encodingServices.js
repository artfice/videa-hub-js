module.exports = {
	"/accounts/{accountId}/encodingservices": {
		"get": {
			"x-swagger-router-controller": "encodings",
			"operationId": "getEncoding",
			"tags": ["Encodings"],
			"description": "Returns the list of encodings.",
			"parameters": [
				{
					"name": "accountId",
					"in": "path",
					"description": "Account Id",
					"type": "string",
					"required": true
				}
			],
			"responses": {
				"default": {
					"description": "Invalid request.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				},
				"200": {
					"description": "Successful request.",
					"schema": {
						"$ref": "#/definitions/EncodingProvider"
					}
				}
			}
		},
		"post": {
			"x-swagger-router-controller": "encodings",
			"operationId": "postEncoding",
			"tags": ["Encodings"],
			"description": "Adds a new encoding.",
			"parameters": [
				{
					"name": "accountId",
					"in": "path",
					"description": "Account Id",
					"type": "string",
					"required": true
				},
				{
					"name": "encoding",
					"in": "body",
					"description": "Encoding data to add. The Id should be unique otherwise an error will be thrown.",
					"required": true,
					"schema": {
						"$ref": "#/definitions/EncodingProvider"
					}
				}
			],
			"responses": {
				"default": {
					"description": "Invalid request.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				},
				"200": {
					"description": "Successful request."
				},
				"400": {
					"description": "Given encoding doesn't have an unique Id.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				}
			}
		}
	},
	"/accounts/{accountId}/encodingservices/content/{contentId}/encoding" :{
		"post" :{
			"x-swagger-router-controller": "encodings",
			"operationId" : "startEncodingVideoJob",
			"tags": ["Encodings"],
			"description": "Starts a new encoding job for the video passed.",
			"parameters": [
				{
					"name": "accountId",
					"in": "path",
					"description": "Account Id",
					"type": "string",
					"required": true
				},
				{
					"name": "contentId",
					"in": "path",
					"description": "Content Id",
					"type": "string",
					"required": true
				},
				{
					"name": "encodingData",
					"in": "body",
					"description": "Data with the video url and content id.",
					"required": true,
					"schema": {
						"type": "array",
						"items": {
							"$ref"  : "#/definitions/EncodingData"
						}
					}
				}
			],
			"responses": {
				"default": {
					"description": "Invalid request.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				},
				"200": {
					"description": "Successful request."
				},
				"400": {
					"description": "Could not encode video.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				}
			}
		}
	}
};
