/**
 * Created by ryanwong on 2015-10-20.
 */
module.exports = {
    "/accounts/{accountId}/brands/{brandId}/apps": {
        "post": {
            "x-swagger-router-controller": "apps",
            "operationId": "createApp",
            "tags": ["App"],
            "description": "Adds a new App.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [{
                "name": "accountId",
                "in": "path",
                "description": "Account Id",
                "type": "string",
                "required": true
            }, {
                "name": "brandId",
                "in": "path",
                "description": "Brand Id",
                "type": "string",
                "required": true
            }, {
                "name": "app",
                "in": "body",
                "description": "Name of new App and App type.",
                "required": true,
                "schema": {
                    "$ref": "#/definitions/App"
                }
            }],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/App"
                    }
                },
                "400": {
                    "description": "Given app doesn't have an unique Id.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },
        "get": {
            "x-swagger-router-controller": "apps",
            "operationId": "getAppByBrand",
            "tags": ["App"],
            "description": "Get all Apps by Brand.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [{
                "name": "accountId",
                "in": "path",
                "description": "Account Id",
                "type": "string",
                "required": true
            }, {
                "name": "brandId",
                "in": "path",
                "description": "Brand Id",
                "type": "string",
                "required": true
            }],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "type": "array",
                        "$ref": "#/definitions/App"
                    }
                },
                "400": {
                    "description": "Given app doesn't have an unique Id.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    },
    "/accounts/{accountId}/brands/{brandId}/apps/{appId}": {
        "get": {
            "x-swagger-router-controller": "apps",
            "operationId": "getApp",
            "tags": ["App"],
            "description": "Returns an App.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [{
                "name": "accountId",
                "in": "path",
                "description": "Account Id",
                "type": "string",
                "required": true
            }, {
                "name": "brandId",
                "in": "path",
                "description": "Id of an existing brand.",
                "required": true,
                "type": "string"
            }, {
                "name": "appId",
                "in": "path",
                "description": "Id of an existing app.",
                "required": true,
                "type": "string"
            }],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/App"
                    }
                },
                "400": {
                    "description": "App Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },
        "put": {
            "x-swagger-router-controller": "apps",
            "operationId": "updateApp",
            "tags": ["App"],
            "description": "Modifies an existing App.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [{
                "name": "accountId",
                "in": "path",
                "description": "Account Id",
                "type": "string",
                "required": true
            }, {
                "name": "brandId",
                "in": "path",
                "description": "Brand Id to update.",
                "required": true,
                "type": "string"
            }, {
                "name": "appId",
                "in": "path",
                "description": "App Id to update.",
                "required": true,
                "type": "string"
            }, {
                "name": "app",
                "in": "body",
                "description": "App name attributes to modify",
                "required": true,
                "schema": {
                    "$ref": "#/definitions/App"
                }
            }],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/App"
                    }
                },
                "400": {
                    "description": "App Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },
        "delete": {
            "x-swagger-router-controller": "apps",
            "operationId": "deleteApp",
            "tags": ["App"],
            "description": "Deletes an existing App and all configurations associated.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [{
                "name": "accountId",
                "in": "path",
                "description": "Account Id",
                "type": "string",
                "required": true
            }, {
                "name": "brandId",
                "in": "path",
                "description": "Id of an existing brand.",
                "required": true,
                "type": "string"
            }, {
                "name": "appId",
                "in": "path",
                "description": "App Id to update.",
                "required": true,
                "type": "string"
            }],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request."
                },
                "400": {
                    "description": "App Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    },
    "/accounts/{fromAccountId}/brands/{fromBrandId}/apps/{appId}/duplicate": {
        "post": {
            "x-swagger-router-controller": "apps",
            "operationId": "duplicateApp",
            "tags": ["App"],
            "description": "Duplicate an existing App under specified account and brand.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [{
                "name": "fromAccountId",
                "in": "path",
                "description": "Account Id to duplicate from",
                "type": "string",
                "required": true
            }, {
                "name": "fromBrandId",
                "in": "path",
                "description": "Id of an existing brand to duplicate to",
                "required": true,
                "type": "string"
            }, {
                "name": "appId",
                "in": "path",
                "description": "App Id to duplicate.",
                "required": true,
                "type": "string"
            }, {
                "name": "appDuplicateTo",
                "in": "body",
                "description": "Where the new app will be duplicated to, it needs an account id and a brand id. If not set, default to fromAccountId and fromBrandId",
                "required": false,
                "schema": {
                    "$ref": "#/definitions/AppDuplicateTo"
                }
            }],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request."
                },
                "400": {
                    "description": "App can not be duplicated.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    }
};
