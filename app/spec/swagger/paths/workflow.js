/**
 * Created by bardiakhosravi on 15-05-13.
 */
module.exports = {
    "/workflow/states": {
        "get": {
            "x-swagger-router-controller": "workflow",
			"x-videa-resource" : "workflow",
			"x-videa-permissions" : ["view"],
            "operationId": "getAllWorkflowStates",
            "tags": ["Workflow"],
            "description": "Returns all the available states in the workflow.",
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "type": "array",
                        "items": {
                            "type": "string"
                        }
                    }
                },
                "400": {
                    "description": "account with the given id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    },
    "/workflow/states/{state}/triggers": {
        "get": {
            "x-swagger-router-controller": "workflow",
			"x-videa-resource" : "workflow",
			"x-videa-permissions" : ["view"],
            "operationId": "getAllowedTriggers",
            "tags": ["Workflow"],
            "description": "Returns the available triggers for the given state.",
            "parameters": [
                {
                    "name": "state",
                    "in": "path",
                    "description": "Identifier of the current state.",
                    "required": true,
                    "type": "string"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "type": "array",
                        "items": {
                            "type": "string"
                        }
                    }
                },
                "400": {
                    "description": "account with the given id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    }
};
