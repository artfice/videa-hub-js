module.exports = {
    "/accounts/{accountId}/collections": {
        "get": {
            "x-swagger-router-controller": "collections",
			"x-videa-resource" : "collections",
			"x-videa-permissions" : ["view"],
            "operationId": "getCollections",
            "tags": ["Collections"],
            "description": "Returns the list of collections.",
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "offset",
                    "in": "query",
                    "description": "Offset in the result set",
                    "type": "integer",
                    "default": 0
                },
                {
                    "name": "setSize",
                    "in": "query",
                    "description": "Limit number of results",
                    "type": "integer",
                    "default": 10
                },
                {
                    "name": "sort",
                    "in": "query",
                    "description": "User field name used to sort the results",
                    "type": "string"
                },
                {
                    "name": "ascending",
                    "in": "query",
                    "description": "Used to order by the results based on the given sort parameter",
                    "type": "boolean",
                    "default": true
                },
				{
					"name": "query",
					"in": "query",
					"description": "Search query to filter results.",
					"type": "string"
				}
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/Collection"
                    }
                }
            }
        },
        "post": {
            "x-swagger-router-controller": "collections",
			"x-videa-resource" : "collections",
			"x-videa-permissions" : ["edit"],
            "operationId": "postCollection",
            "tags": ["Collections"],
            "description": "Adds a new collection.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "collection",
                    "in": "body",
                    "description": "collection data to add. The Id and dates will be filled by the service and should not be sent.",
                    "required": true,
                    "schema": {
                        "$ref": "#/definitions/Collection"
                    }
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/Collection"
                    }
                },
                "400": {
                    "description": "Given collection doesn't have an unique Id.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    },
    "/accounts/{accountId}/collections/{collectionId}": {
        "get": {
            "x-swagger-router-controller": "collections",
			"x-videa-resource" : "collections",
			"x-videa-permissions" : ["view"],
            "operationId": "getCollection",
            "tags": ["Collections"],
            "description": "Returns an existing collection.",
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "collectionId",
                    "in": "path",
                    "description": "Id of an existing collection.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "includeRefs",
                    "in" : "query",
                    "description" : "Activate to include reference content nodes.",
                    "type" : "boolean",
                    "default" : false
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/Collection"
                    }
                },
                "400": {
                    "description": "collection Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },
        "put": {
            "x-swagger-router-controller": "collections",
			"x-videa-resource" : "collections",
			"x-videa-permissions" : ["edit"],
            "operationId": "putCollection",
            "tags": ["Collections"],
            "description": "Modifies an existing collection.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "collectionId",
                    "in": "path",
                    "description": "collection Id to update.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "collection",
                    "in": "body",
                    "description": "collection attributes to modify",
                    "required": true,
                    "schema": {
                        "$ref": "#/definitions/CollectionPartial"
                    }
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/Collection"
                    }
                },
                "400": {
                    "description": "Collection Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },
        "delete": {
            "x-swagger-router-controller": "collections",
			"x-videa-resource" : "collections",
			"x-videa-permissions" : ["delete"],
            "operationId": "removeCollection",
            "tags": ["Collections"],
            "description": "Deletes an existing collection.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "collectionId",
                    "in": "path",
                    "description": "Id of an existing collection.",
                    "required": true,
                    "type": "string"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request."
                },
                "400": {
                    "description": "Collection Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    },
	"/accounts/{accountId}/collections/{collectionId}/content/{contentNodeId}": {
		"delete": {
			"x-swagger-router-controller": "collections",
			"x-videa-resource" : "collections",
			"x-videa-permissions" : ["delete"],
			"operationId": "removeContentNode",
			"tags": ["Collections"],
			"description": "Deletes a content node from collection references.",
			"security": [{
				"videa_auth": []
			}],
			"parameters": [
				{
					"name": "accountId",
					"in": "path",
					"description": "Account Id",
					"type": "string",
					"required": true
				},
				{
					"name": "collectionId",
					"in": "path",
					"description": "Id of an existing collection.",
					"required": true,
					"type": "string"
				},
				{
					"name": "contentNodeId",
					"in": "path",
					"description": "Id of content node.",
					"required": true,
					"type": "string"
				}
			],
			"responses": {
				"default": {
					"description": "Invalid request.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				},
				"200": {
					"description": "Successful request.",
					"schema": {
						"$ref": "#/definitions/Collection"
					}
				},
				"400": {
					"description": "Collection Id not found.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				}
			}
		}
	}
};
