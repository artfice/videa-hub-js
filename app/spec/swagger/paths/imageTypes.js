module.exports = {
    "/accounts/{accountId}/imagetypes": {
        "get": {
            "x-swagger-router-controller": "imageTypes",
			"x-videa-resource" : "imageTypes",
			"x-videa-permissions" : ["view"],
            "operationId": "getImageTypes",
            "tags": ["ImageTypes"],
            "description": "Returns the list of ImageType.",
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "offset",
                    "in": "query",
                    "description": "Offset in the result set",
                    "type": "integer",
                    "default": 0
                },
                {
                    "name": "setSize",
                    "in": "query",
                    "description": "Limit number of results",
                    "type": "integer",
                    "default": 10
                },
                {
                    "name": "sort",
                    "in": "query",
                    "description": "User field name used to sort the results",
                    "type": "string"
                },
                {
                    "name": "ascending",
                    "in": "query",
                    "description": "Used to order by the results based on the given sort parameter",
                    "type": "boolean",
                    "default": true
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/ImageType"
                    }
                }
            }
        },
        "post": {
            "x-swagger-router-controller": "imageTypes",
			"x-videa-resource" : "imageTypes",
			"x-videa-permissions" : ["edit"],
            "operationId": "postImageType",
            "tags": ["ImageTypes"],
            "description": "Adds a new ImageType.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "imageType",
                    "in": "body",
                    "description": "ImageType data to add. The Id and dates will be filled by the service and should not be sent.",
                    "required": true,
                    "schema": {
                        "$ref": "#/definitions/ImageType"
                    }
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/ImageType"
                    }
                },
                "400": {
                    "description": "Given brand doesn't have an unique Id.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    },
    "/accounts/{accountId}/imagetypes/{imageTypeId}": {
        "get": {
            "x-swagger-router-controller": "imageTypes",
			"x-videa-resource" : "imageTypes",
			"x-videa-permissions" : ["view"],
            "operationId": "getImageType",
            "tags": ["ImageTypes"],
            "description": "Returns an existing ImageType.",
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "imageTypeId",
                    "in": "path",
                    "description": "Id of an existing ImageType.",
                    "required": true,
                    "type": "string"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/ImageType"
                    }
                },
                "400": {
                    "description": "Brand Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },
        "put": {
            "x-swagger-router-controller": "imageTypes",
			"x-videa-resource" : "imageTypes",
			"x-videa-permissions" : ["edit"],
            "operationId": "putImageType",
            "tags": ["ImageTypes"],
            "description": "Modifies an existing ImageType.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "imageTypeId",
                    "in": "path",
                    "description": "ImageType Id to update.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "imageType",
                    "in": "body",
                    "description": "ImageType attributes to modify",
                    "required": true,
                    "schema": {
                        "$ref": "#/definitions/ImageTypePartial"
                    }
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/ImageType"
                    }
                },
                "400": {
                    "description": "ImageType Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },
        "delete": {
            "x-swagger-router-controller": "imageTypes",
			"x-videa-resource" : "imageTypes",
			"x-videa-permissions" : ["delete"],
            "operationId": "removeImageType",
            "tags": ["ImageTypes"],
            "description": "Deletes an existing ImageType.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "imageTypeId",
                    "in": "path",
                    "description": "Id of an existing ImageType.",
                    "required": true,
                    "type": "string"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request."
                },
                "400": {
                    "description": "ImageType Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    }
};
