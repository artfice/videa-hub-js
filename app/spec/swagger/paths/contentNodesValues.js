module.exports = {
    "/accounts/{accountId}/content/{contentId}/values/{fieldId}": {
        "get": {
            "x-swagger-router-controller": "contentNodesValues",
            "x-videa-resource": "contentNodes",
            "x-videa-permissions": ["view"],
            "operationId": "getContentNodeFieldValue",
            "tags": ["Content Nodes Values"],
            "description": "Returns the value of a given field.",
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "contentId",
                    "in": "path",
                    "description": "Id of an existing ContentNode.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "fieldId",
                    "in": "path",
                    "description": "Id of an existing Field stored on the ContentNode.",
                    "required": true,
                    "type": "string"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request returns the field value.",
                    "schema": {
                        "type": "string"
                    }
                },
                "400": {
                    "description": "Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },
        "post": {
            "x-swagger-router-controller": "contentNodesValues",
            "x-videa-resource": "contentNodes",
            "x-videa-permissions": ["edit"],
            "operationId": "postContentNodeValue",
            "tags": ["Content Nodes Values"],
            "description": "Modifies an existing field of a ContentNode.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "contentId",
                    "in": "path",
                    "description": "ContentNode Id to update.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "fieldId",
                    "in": "path",
                    "description": "Id of an existing Field stored on the ContentNode.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "fieldValue",
                    "in": "body",
                    "description": "Field value to modify",
                    "required": true,
                    "schema": {
                        "$ref": "#/definitions/ContentNodePartial"
                        //"type": "string"
                    }
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request."
                },
                "400": {
                    "description": "Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    },
    "/accounts/{accountId}/content/{contentId}/values/{fieldId}/images": {
        "post": {
            "x-swagger-router-controller": "contentNodesValues",
            "x-videa-resource": "contentNodes",
            "x-videa-permissions": ["edit"],
            "operationId": "uploadContentNodeFieldImage",
            "tags": ["Content Nodes Values"],
            "description": "Upload a new image and associated to the given content field.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "contentId",
                    "in": "path",
                    "description": "Id of an existing ContentNode.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "fieldId",
                    "in": "path",
                    "description": "Id of an existing Field stored on the ContentNode.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "imageType",
                    "in": "query",
                    "description": "Name of the image type associated with this image.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "image",
                    "in": "formData",
                    "description": "Image to upload.",
                    "required": true,
                    "type": "file"
                }
            ],
            "consumes": ["multipart/form-data"],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request returns the field value.",
                    "schema": {
                        "type": "string"
                    }
                },
                "400": {
                    "description": "Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    },
    "/accounts/{accountId}/content/{contentId}/values/{fieldId}/images/{imageId}": {
        "delete": {
            "x-swagger-router-controller": "contentNodesValues",
            "x-videa-resource": "contentNodes",
            "x-videa-permissions": ["delete"],
            "operationId": "removeContentNodeFieldImage",
            "tags": ["Content Nodes Values"],
            "description": "Remove the selected image from the content field.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "contentId",
                    "in": "path",
                    "description": "Id of an existing ContentNode.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "fieldId",
                    "in": "path",
                    "description": "Id of an existing Field stored on the ContentNode.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "imageId",
                    "in": "path",
                    "description": "Id of the image to remove.",
                    "required": true,
                    "type": "string"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request deletes the image.",
                    "schema": {
                        "type": "string"
                    }
                },
                "400": {
                    "description": "Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    },
    "/accounts/{accountId}/content/{contentId}/values/{fieldId}/refs": {
        "get": {
            "x-swagger-router-controller": "contentNodesValues",
            "x-videa-resource": "contentNodes",
            "x-videa-permissions": ["view"],
            "operationId": "getContentNodeFieldRefs",
            "tags": ["Content Nodes Values"],
            "description": "Get content nodes references.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id.",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "contentId",
                    "in": "path",
                    "description": "Content node Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "fieldId",
                    "in": "path",
                    "description": "Content field Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "offset",
                    "in": "query",
                    "description": "Offset in the result set.",
                    "type": "integer",
                    "default": 0
                },
                {
                    "name": "setSize",
                    "in": "query",
                    "description": "Limit number of results.",
                    "type": "integer",
                    "default": 10
                },
                {
                    "name": "sort",
                    "in": "query",
                    "description": "User field name used to sort the results.",
                    "type": "string"
                },
                {
                    "name": "ascending",
                    "in": "query",
                    "description": "Used to order by the results based on the given sort parameter.",
                    "type": "boolean",
                    "default": true
                },
                {
                    "name": "workflows",
                    "in": "query",
                    "description": "Parameter to choose the content with a one or more workflow",
                    "type": "string",
                    "default": '["Published"]'
                },
                {
                    "name": "includeRefs",
                    "in": "query",
                    "description": "Activate to include reference content nodes.",
                    "type": "boolean",
                    "default": false
                },
                {
                    "name": "removeExpired",
                    "in": "query",
                    "description": "Activate to remove expired content nodes.",
                    "type": "boolean",
                    "default": false
                },
                {
                    "name": "query",
                    "in": "query",
                    "description": "Advanced search query to filter results.",
                    "type": "string"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/Schema"
                    }
                },
                "400": {
                    "description": "Given app doesn't have an unique Id.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    },
    "/accounts/{accountId}/content/{contentId}/values/{fieldId}/refs/{referenceId}": {
        "post": {
            "x-swagger-router-controller": "contentNodesValues",
            "x-videa-resource": "contentNodes",
            "x-videa-permissions": ["edit"],
            "operationId": "postContentNodeFieldRef",
            "tags": ["Content Nodes Values"],
            "description": "Adds a new reference to the given content field.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "contentId",
                    "in": "path",
                    "description": "Id of an existing ContentNode.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "fieldId",
                    "in": "path",
                    "description": "Id of an existing Field stored on the ContentNode.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "referenceId",
                    "in": "path",
                    "description": "Id of an existing ContentNode.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "index",
                    "in": "query",
                    "description": "Optional param to insert the new reference into the given index.",
                    "required": false,
                    "type": "integer"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request returns the field value.",
                    "schema": {
                        "type": "string"
                    }
                },
                "400": {
                    "description": "Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },
        "delete": {
            "x-swagger-router-controller": "contentNodesValues",
            "x-videa-resource": "contentNodes",
            "x-videa-permissions": ["edit"],
            "operationId": "deleteContentNodeFieldRef",
            "tags": ["Content Nodes Values"],
            "description": "Removes a reference from the given content field.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "contentId",
                    "in": "path",
                    "description": "Id of an existing ContentNode.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "fieldId",
                    "in": "path",
                    "description": "Id of an existing Field stored on the ContentNode.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "referenceId",
                    "in": "path",
                    "description": "Id of an existing ContentNode.",
                    "required": true,
                    "type": "string"
                },
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request returns the field value.",
                    "schema": {
                        "type": "string"
                    }
                },
                "400": {
                    "description": "Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },
    }
};
