module.exports = {
    "/auth": {
        "post": {
            "x-swagger-router-controller": "auth",
            "x-videa-resource" : "auth",
            "x-videa-permissions" : ["edit"],
            "operationId": "signIn",
            "tags": ["Identity Management"],
            "description": "Signs into the videa hub and receives a token",
            "parameters": [
                {
                    "name": "signIn",
                    "in": "body",
                    "description": "SignIn structure with username and password.",
                    "required": true,
                    "schema": {
                        "$ref": "#/definitions/SignIn"
                    }
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/SignInResponse"
                    }
                },
                "400": {
                    "description": "Given user doesn't have an unique Id.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "401": {
                    "description": "Username and password did not match.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }

            }
        }
    },
    "/auth/identity": {
        "get": {
            "x-swagger-router-controller": "auth",
            "x-videa-resource" : "auth",
            "x-videa-permissions" : ["view"],
            "operationId": "getIdentity",
            "tags": ["Identity Management"],
            "description": "Retrieves the user identity based on the cookie or token.",
            "parameters": [ ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/SignInResponse"
                    }
                },
                "400": {
                    "description": "Token not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "401": {
                    "description": "Token is not valid.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }

            }
        }
    },

    "/signout" : {
        "post": {
            "x-swagger-router-controller": "auth",
            "x-videa-resource" : "auth",
            "x-videa-permissions" : ["edit"],
            "operationId": "signOut",
            "tags": ["Identity Management"],
            "description": "Signs out from  videa hub",
            "parameters": [ ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema" : {}
                }
            }
        }
    }
};
