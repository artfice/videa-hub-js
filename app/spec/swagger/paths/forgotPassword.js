module.exports = {
    "/forgotpassword/{email}": {
        "post": {
            "x-swagger-router-controller": "forgotPasswords",
            "operationId": "requestChange",
            "tags": ["Forgot Password"],
            "description": "Requests a change of password",
            "parameters": [
                {
                    "name": "email",
                    "in": "path",
                    "description": "User email",
                    "type": "string",
                    "required": true
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/ForgotPasswordResponse"
                    }
                },
                "400": {
                    "description": "Given user doesn't have an unique Id.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "401": {
                    "description": "Username and password did not match.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }

            }
        }
    },
    "/change-password/{token}": {
        "get": {
            "x-swagger-router-controller": "forgotPasswords",
            "operationId": "verifyToken",
            "tags": ["Forgot Password"],
            "description": "Modifies the user password.",
            "parameters": [
                {
                    "name": "token",
                    "in": "path",
                    "description": "Token",
                    "type": "string",
                    "required": true
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request."
                },
                "400": {
                    "description": "SignUpRequest with given id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },
        "put": {
            "x-swagger-router-controller": "forgotPasswords",
            "operationId": "changePassword",
            "tags": ["Forgot Password"],
            "description": "Modifies the user password.",
            "parameters": [
                {
                    "name": "token",
                    "in": "path",
                    "description": "Token",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "newPassword",
                    "in": "body",
                    "description": "New password.",
                    "required": true,
                    "schema" : {
                        "$ref" : "#/definitions/ChangePasswordRequest"
                    }
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/ForgotPasswordResponse"
                    }
                },
                "400": {
                    "description": "SignUpRequest with given id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    }
};