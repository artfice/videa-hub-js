/**
 * Created by bardiakhosravi on 2015-11-30.
 */


module.exports = {
    "/dynamicEnum/accounts/{accountId}/uiConfigs/{configId}/analytics": {
        "get": {
            "x-swagger-router-controller": "dynamicEnum",
            "operationId": "getAnalytics",
            "tags": ["Dynamic Enumeration"],
            "description": "Returns a List of dynamicEnum Analytics",
            "parameters": [{
                "name": "accountId",
                "in": "path",
                "description": "Account Id",
                "type": "string",
                "required": true
            }, {
                "name": "configId",
                "in": "path",
                "description": "Config Id",
                "type": "string",
                "required": true
            }],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/BaseReference"
                    }
                }
            }
        }
    },
    "/dynamicEnum/accounts/{accountId}/uiConfigs/{configId}/screens": {
        "get": {
            "x-swagger-router-controller": "dynamicEnum",
            "operationId": "getScreens",
            "tags": ["Dynamic Enumeration"],
            "description": "Returns a List of dynamicEnum Screens",
            "parameters": [{
                "name": "accountId",
                "in": "path",
                "description": "Account Id",
                "type": "string",
                "required": true
            }, {
                "name": "configId",
                "in": "path",
                "description": "Config Id",
                "type": "string",
                "required": true
            }, {
                "name": "type",
                "in": "query",
                "description": "Type of Screen to Return. Either all, regular, search",
                "type": "string",
                "default": "all"
            }],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/BaseReference"
                    }
                }
            }
        }
    }
};
