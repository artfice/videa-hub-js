
module.exports = {
    "/role/account": {
        "get": {
            "x-swagger-router-controller": "role",
			"x-videa-resource" : "rolesAccount",
			"x-videa-permissions" : ["view"],
            "operationId": "getAccountRoles",
            "tags": ["Roles"],
            "description": "Returns the available user roles.",
			"security": [{
				"videa_auth": []
			}],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request."
                },
                "400": {
                    "description": "Error.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    },
	"/role/user": {
		"get": {
			"x-swagger-router-controller": "role",
			"x-videa-resource" : "rolesUser",
			"x-videa-permissions" : ["view"],
			"operationId": "getUserRoles",
			"tags": ["Roles"],
			"description": "Returns the available member roles.",
			"security": [{
				"videa_auth": []
			}],
			"responses": {
				"default": {
					"description": "Invalid request.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				},
				"200": {
					"description": "Successful request."
				},
				"400": {
					"description": "Error.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				}
			}
		}
	}
};
