module.exports = {
    "/accounts/{accountId}/brands": {
        "get": {
            "x-swagger-router-controller": "brands",
            "operationId": "getBrands",
            "tags": ["Brands"],
            "description": "Returns the list of brands.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "offset",
                    "in": "query",
                    "description": "Offset in the result set",
                    "type": "integer",
                    "default": 0
                },
                {
                    "name": "setSize",
                    "in": "query",
                    "description": "Limit number of results",
                    "type": "integer",
                    "default": 0
                },
                {
                    "name": "sort",
                    "in": "query",
                    "description": "User field name used to sort the results",
                    "type": "string"
                },
                {
                    "name": "ascending",
                    "in": "query",
                    "description": "Used to order by the results based on the given sort parameter",
                    "type": "boolean",
                    "default": true
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/Brand"
                    }
                }
            }
        },
        "post": {
            "x-swagger-router-controller": "brands",
            "operationId": "postBrand",
            "tags": ["Brands"],
            "description": "Adds a new brand.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "brand",
                    "type": "string",
                    "in": "formData",
                    "description": "Brand data to add. The Id and dates will be filled by the service and should not be sent.",
                    "required": true
                },
                {
                    "name": "image",
                    "in": "formData",
                    "description": "Brand Logo to upload.",
                    "required": false,
                    "type": "file"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/Brand"
                    }
                },
                "400": {
                    "description": "Given brand doesn't have an unique Id.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    },
    "/accounts/{accountId}/brands/{brandId}": {
        "get": {
            "x-swagger-router-controller": "brands",
            "operationId": "getBrand",
            "tags": ["Brands"],
            "description": "Returns an existing brand.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "brandId",
                    "in": "path",
                    "description": "Id of an existing brand.",
                    "required": true,
                    "type": "string"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/Brand"
                    }
                },
                "400": {
                    "description": "Brand Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },
        "put": {
            "x-swagger-router-controller": "brands",
            "operationId": "putBrand",
            "tags": ["Brands"],
            "description": "Modifies an existing brand.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "brandId",
                    "in": "path",
                    "description": "Brand Id to update.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "brand",
                    "type": "string",
                    "in": "formData",
                    "description": "Brand data to add. The Id and dates will be filled by the service and should not be sent.",
                    "required": true
                },
                {
                    "name": "image",
                    "in": "formData",
                    "description": "Brand Logo to upload.",
                    "required": false,
                    "type": "file"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/Brand"
                    }
                },
                "400": {
                    "description": "Brand Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },
        "delete": {
            "x-swagger-router-controller": "brands",
            "operationId": "removeBrand",
            "tags": ["Brands"],
            "description": "Deletes an existing brand.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "brandId",
                    "in": "path",
                    "description": "Id of an existing brand.",
                    "required": true,
                    "type": "string"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request."
                },
                "400": {
                    "description": "Brand Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    },
    "/accounts/{accountId}/brands/{brandId}/gallery": {
        "get": {
            "x-swagger-router-controller": "brands",
            "operationId": "getGallery",
            "tags": ["Brand Gallery"],
            "description": "Returns a list of gallery image.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "brandId",
                    "in": "path",
                    "description": "Id of an existing brand.",
                    "required": true,
                    "type": "string"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/GalleryImage"
                    }
                },
                "400": {
                    "description": "Brand Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },
        "post": {
            "x-swagger-router-controller": "brands",
            "operationId": "addGalleryImage",
            "tags": ["Brand Gallery"],
            "description": "Adds a new gallery image to brand.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },{
                    "name": "brandId",
                    "in": "path",
                    "description": "Brand Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "name",
                    "type": "string",
                    "in": "formData",
                    "description": "Image name",
                    "required": true
                },
                {
                    "name": "image",
                    "in": "formData",
                    "description": "Gallery Image to upload.",
                    "required": true,
                    "type": "file"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/GalleryImage"
                    }
                },
                "400": {
                    "description": "Given brand doesn't have an unique Id.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    },
    "/accounts/{accountId}/brands/{brandId}/gallery/{galleryImageId}": {
        "put": {
            "x-swagger-router-controller": "brands",
            "operationId": "replaceGalleryImage",
            "tags": ["Brand Gallery"],
            "description": "Replace existing gallery image.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "brandId",
                    "in": "path",
                    "description": "Brand Id to update.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "galleryImageId",
                    "type": "string",
                    "in": "path",
                    "description": "Gallery Id for gallery image you want to update.",
                    "required": true
                },
                {
                    "name": "name",
                    "type": "string",
                    "in": "formData",
                    "description": "Image name",
                    "required": true
                },
                {
                    "name": "image",
                    "in": "formData",
                    "description": "Gallery image to upload.",
                    "required": false,
                    "type": "file"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/GalleryImage"
                    }
                },
                "400": {
                    "description": "Brand Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },
        "delete": {
            "x-swagger-router-controller": "brands",
            "operationId": "removeGalleryImage",
            "tags": ["Brand Gallery"],
            "description": "Deletes an existing Brand Gallery.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "brandId",
                    "in": "path",
                    "description": "Id of an existing brand.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "galleryImageId",
                    "in": "path",
                    "description": "Id of an existing gallery image.",
                    "required": true,
                    "type": "string"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request."
                },
                "400": {
                    "description": "Brand Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    }
};