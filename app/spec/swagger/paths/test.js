module.exports = {
	"/test/resetimagetransforms": {
		"post": {
			"x-swagger-router-controller": "test",
			"operationId": "resetImageTypesTransforms",
			"tags": ["Test"],
			"description": "Resets all images transforms in cloudinary in all accounts.",
			"parameters": [	],
			"responses": {
				"default": {
					"description": "Invalid request.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				},
				"200": {
					"description": "Successful request.",
					"schema": {
						"$ref": "#/definitions/Worker"
					}
				},
				"400": {
					"description": "Given user doesn't have an unique Id.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				}
			}
		}
	},
    "/test/account/{accountId}/randomData": {
		"get": {
			"x-swagger-router-controller": "test",
			"operationId": "postRandomDataTestGet",
			"tags": ["Test"],
			"description": "Adds default data to an account.",
			"parameters": [
				{
					"name": "accountId",
					"in": "path",
					"description": "Id of an existing user.",
					"required": true,
					"type": "string"
				}
			],
			"responses": {
				"default": {
					"description": "Invalid request.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				},
				"200": {
					"description": "Successful request.",
					"schema": {
						"$ref": "#/definitions/Worker"
					}
				},
				"400": {
					"description": "Given user doesn't have an unique Id.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				}
			}
		},
		"post": {
			"x-swagger-router-controller": "test",
			"operationId": "postRandomData",
			"tags": ["Test"],
			"description": "Adds default data to an account.",
			"parameters": [
				{
					"name": "accountId",
					"in": "path",
					"description": "Id of an existing user.",
					"required": true,
					"type": "string"
				},
				{
					"name": "dataSettings",
					"in": "body",
					"description": "Settings that specifies what data and which amounts.",
					"required": true,
					"schema": {
						"$ref": "#/definitions/DataSettings"
					}
				}
			],
			"responses": {
				"default": {
					"description": "Invalid request.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				},
				"200": {
					"description": "Successful request.",
					"schema": {
						"$ref": "#/definitions/Worker"
					}
				},
				"400": {
					"description": "Given user doesn't have an unique Id.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				}
			}
		}
	},
	"/test/account/{accountId}/resetNodes" : {
		"post" :{
			"x-swagger-router-controller": "test",
			"operationId": "resetNodesFromBase",
			"tags": ["Test"],
			"description": "Resets elasticsearch index from the nodes in mongodb.",
			"parameters": [
				{
					"name": "accountId",
					"in": "path",
					"description": "Id of an existing user.",
					"required": true,
					"type": "string"
				}
			],
			"responses": {
				"default": {
					"description": "Invalid request.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				},
				"200": {
					"description": "Successful request."
				},
				"400": {
					"description": "Given user doesn't have an unique Id.",
					"schema": {
						"$ref": "#/definitions/VideaError"
					}
				}
			}
		}
	},
    "/test/accounts/{accountId}/content/basicSearch": {
        "post": {
            "x-swagger-router-controller": "test",
            "x-videa-resource": "contentNodes",
            "x-videa-permissions": ["view"],
            "operationId": "basicSearch",
            "tags": ["Test"],
            "description": "Returns a list of ContentNodes using an Elastic Search query.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id.",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "query",
                    "in": "body",
                    "description": "Elastic Search query string (JSON).",
                    "required": true,
                    "schema": {
                        "$ref": "#/definitions/ElasticSearchQuery"
                    }
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/ContentNode"
                    }
                }
            }
        }
    },
    "/test/accounts/{accountId}/content/basicSearchUsingMultipleFields": {
        "post": {
            "x-swagger-router-controller": "test",
            "x-videa-resource": "contentNodes",
            "x-videa-permissions": ["view"],
            "operationId": "basicSearchUsingMultipleFields",
            "tags": ["Test"],
            "description": "Returns a list of ContentNodes using Elastic Search query on multiple fields.",
            "security": [{
                "videa_auth": []
            }],
            "parameters": [
                {
                    "name": "accountId",
                    "in": "path",
                    "description": "Account Id.",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "query",
                    "in": "query",
                    "description": "Query string to filter results.",
                    "type": "string",
                    "required": true
                },
                {
                    "name": "fields",
                    "in": "query",
                    "description": "Fields to search: [\"f1\", \"f2\", ...]",
                    "type": "string",
                    "required": true
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/ContentNode"
                    }
                }
            }
        }
    }
};

