module.exports = {
    "/users": {
        "get": {
            "x-swagger-router-controller": "users",
            "operationId": "getUsers",
            "tags": ["Users"],
            "description": "Returns the list of users.",
            "parameters": [
                {
                    "name": "offset",
                    "in": "query",
                    "description": "Offset in the result set",
                    "type": "integer",
                    "default": 0
                },
                {
                    "name": "setSize",
                    "in": "query",
                    "description": "Limit number of results",
                    "type": "integer",
                    "default": 10
                },
                {
                    "name": "sort",
                    "in": "query",
                    "description": "User field name used to sort the results",
                    "type": "string"
                },
                {
                    "name": "ascending",
                    "in": "query",
                    "description": "Used to order by the results based on the given sort parameter",
                    "type": "boolean",
                    "default": true
                },
				{
					"name": "query",
					"in": "query",
					"description": "Search query to filter results.",
					"type": "string"
				}
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/User"
                    }
                }
            }
        },
        "post": {
            "x-swagger-router-controller": "users",
            "operationId": "postUser",
            "tags": ["Users"],
            "description": "Adds a new user.",
            "parameters": [
                {
                    "name": "user",
                    "in": "body",
                    "description": "User data to add. The Id should be unique otherwise an error will be thrown.",
                    "required": true,
                    "schema": {
                        "$ref": "#/definitions/User"
                    }
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/User"
                    }
                },
                "400": {
                    "description": "Given user doesn't have an unique Id.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    },
    "/users/{userId}": {
        "get": {
            "x-swagger-router-controller": "users",
            "operationId": "getUser",
            "tags": ["Users"],
            "description": "Returns an existing user.",
            "parameters": [
                {
                    "name": "userId",
                    "in": "path",
                    "description": "Id of an existing user.",
                    "required": true,
                    "type": "string"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/User"
                    }
                },
                "400": {
                    "description": "User Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },
        "put": {
            "x-swagger-router-controller": "users",
            "operationId": "putUser",
            "tags": ["Users"],
            "description": "Modifies an existing user.",
            "parameters": [
                {
                    "name": "userId",
                    "in": "path",
                    "description": "User Id to update.",
                    "required": true,
                    "type": "string"
                },
                {
                    "name": "user",
                    "in": "body",
                    "description": "User attributes to modify",
                    "required": true,
                    "schema": {
                        "$ref": "#/definitions/UserPartial"
                    }
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request.",
                    "schema": {
                        "$ref": "#/definitions/User"
                    }
                },
                "400": {
                    "description": "User Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        },
        "delete": {
            "x-swagger-router-controller": "users",
            "operationId": "removeUser",
            "tags": ["Users"],
            "description": "Deletes an existing user.",
            "parameters": [
                {
                    "name": "userId",
                    "in": "path",
                    "description": "Id of an existing user.",
                    "required": true,
                    "type": "string"
                }
            ],
            "responses": {
                "default": {
                    "description": "Invalid request.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                },
                "200": {
                    "description": "Successful request."
                },
                "400": {
                    "description": "User Id not found.",
                    "schema": {
                        "$ref": "#/definitions/VideaError"
                    }
                }
            }
        }
    }
};
