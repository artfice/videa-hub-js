"use strict";
var nconf = require('nconf');

// Import all files in current folder and sub folders.
var requireDirectory = require('require-directory');

var definitions = {};
requireDirectory(module, './definitions', { visit: function (obj) {
    var property;
    for (property in obj) {
        if (obj.hasOwnProperty(property)) {
            definitions[property] = obj[property];
        }
    }
}});

var securityDefinitions = {};
requireDirectory(module, './securityDefinitions', { visit: function (obj) {
    var property;
    for (property in obj) {
        if (obj.hasOwnProperty(property)) {
            securityDefinitions[property] = obj[property];
        }
    }
}});

var paths = {};
requireDirectory(module, './paths', { visit: function (obj) {

    var property;
    for (property in obj) {
        if (obj.hasOwnProperty(property)) {
            paths[property] = obj[property];
        }
    }
}});

module.exports = {
    "swagger": "2.0",
    "info": {
        "title": "Videa Hub API",
        "description": "API for getting and manage videa content.",
        "version": "1.0"
    },
    "produces": ["application/json"],
    "host": nconf.get("swagger:videa:url"),
    "basePath": "/api/v1",
    "securityDefinitions": securityDefinitions,
    "paths": paths,
    "definitions": definitions
};

