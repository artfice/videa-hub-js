var Promise = require('bluebird');
var nconf = require('nconf');

var serviceConfig = nconf.get('mongodb');
var VersionService = require('videa/services/mongodb/VersionService.js');
module.exports = new VersionService(serviceConfig);
