var nconf = require('nconf');

var UserService = require('../node_modules/videa/services/mongodb/UserService.js');
var AccountService = require('../node_modules/videa/services/mongodb/AccountService.js');
var AccountMemberService = require('../node_modules/videa/services/mongodb/AccountMemberService.js');
var BrandServiceMongoDB = require('videa/services/mongodb/BrandServiceMongoDB.js');
var AppServiceMongoDB = require('videa/services/mongodb/AppServiceMongoDB.js');
var EditionServiceMongoDB = require('videa/services/mongodb/EditionServiceMongoDB.js');
var ConfigServiceMongoDB = require('videa/services/mongodb/ConfigServiceMongoDB.js');
var ScreenServiceMongoDB = require('videa/services/mongodb/ScreenServiceMongoDB.js');
var GalleryImageMongoDB = require('videa/services/mongodb/GalleryImageServiceMongoDB.js');
var DeviceService = require('videa/services/mongodb/DeviceService.js');
var ConfigurationService = require('videa/services/mongodb/ConfigurationService');
var CatalogService = require('../node_modules/videa/cms/services/mongodb/CatalogService.js');
var CollectionService = require('../node_modules/videa/cms/services/mongodb/CollectionService.js');
var ContentTypeService = require('../node_modules/videa/cms/services/mongodb/ContentTypeService.js');
var ContentFieldService = require('../node_modules/videa/cms/services/mongodb/ContentFieldService.js');
var MongoDBContentNodeService = require('../node_modules/videa/cms/services/mongodb/ContentNodeService.js');
var ElasticSearchContentNodeService = require('../node_modules/videa/cms/services/elasticsearch/ContentNodeService.js');
var IntegrationContentNodeService = require('../node_modules/videa/cms/services/integration/ContentNodeService.js');
var BrandServiceIntegration = require('videa/services/integration/BrandServiceIntegration.js');
var EditionServiceIntegration = require('videa/appstudio/services/integration/EditionServiceIntegration.js');
var AppServiceIntegration = require('videa/services/integration/AppServiceIntegration.js');
var ScreenServiceIntegration = require('videa/services/integration/ScreenServiceIntegration.js');
var UIConfigServiceIntegration = require('videa/services/integration/UIConfigServiceIntegration.js');
var WorkflowService = require('videa/services/integration/WorkflowService.js');
var RemoteAssetService = require('videa/services/aws/s3/RemoteAssetService.js');
var EmailService = require('videa/services/sendgrid/EmailService.js');
var ForgotPasswordService = require('videa/services/common/ForgotPasswordService.js');

var IntegrationACLService = require('videa/services/integration/ACLService.js');
var SchemaService = require('videa/core/services/SchemaService.js');
var VersionService = require('videa/services/mongodb/VersionService.js');
var WorkerService = require('videa/services/mongodb/WorkerService.js');
var LockServices = require('videa/services/mongodb/LockService.js');
var UpLynkVideoEncodingService = require('../node_modules/videa/cms/services/uplynk/VideoEncodingService');
var VideoEncodingService = require('../node_modules/videa/cms/services/integration/VideoEncodingService');
var MappingService = require('videa/services/mongodb/MappingService');

var AuthenticationServiceConfig = require('../node_modules/videa/services/common/AuthenticationServiceConfig.js');
var AuthenticationService = require('../node_modules/videa/services/common/AuthenticationService.js');

var SignUpService = require('../node_modules/videa/services/mongodb/SignUpService.js');

var ImageTypeService = require('../node_modules/videa/services/mongodb/ImageTypeService.js');
var ImageService = require('videa/services/cloudinary/ImageService.js');
var SecurityService = require('videa/services/common/SecurityService.js');
var SpecService = require('videa/services/common/SpecService.js');
var EncodingService = require('../node_modules/videa/cms/services/mongodb/EncodingService.js');
var FileManagerService = require('videa/services/common/FileManagerService');

var ContentIngestionService = require('../node_modules/videa/cms/services/integration/ContentIngestionService');
var LambdaService = require('videa/core/services/LambdaService');
var AppstudioAccountCreation = require('videa/services/integration/AppstudioAccountCreation.js');
var AppPublishingService = require('videa/appstudio/services/integration/AppPublishingService.js');
var PublishedClientConfigApplicationService = require('videa/appstudio/services/integration/PublishedClientConfigApplicationService.js');
var EditionStateService = require('videa/appstudio/services/integration/EditionStateService.js');
var AppstudioDomainService = require('videa/appstudio/services/AppstudioDomainService.js');
var ClientEditionService = require('videa/appstudio/services/integration/ClientEditionService.js');

var ContentNodeRepository = require('../node_modules/videa/cms/repository/ContentNodeRepository');
var QueryTemplateRepository = require('../node_modules/videa/cms/repository/QueryTemplateRepository');
var AccountConfigurationRepository = require('../node_modules/videa/cms/repository/AccountConfigurationRepository');

var serviceConfig = nconf.get('mongodb');
var awsConfig = nconf.get('aws');
var serviceContainer = {};
var lambdaService = new LambdaService();

var emailConfig = nconf.get('sendgrid');
var emailService = new EmailService(emailConfig);
serviceConfig.emailService = emailService;

var aclService = new IntegrationACLService();
serviceConfig.acl = aclService;

var versionService = new VersionService(serviceConfig);

var remoteAssetService = new RemoteAssetService(awsConfig);
serviceConfig.remoteAssetService = remoteAssetService;

var workerServiceConfig = nconf.get('mongodb');
workerServiceConfig.services = serviceConfig;
var workerService = new WorkerService(workerServiceConfig);
serviceConfig.workerService = workerService;

var workflowService = new WorkflowService();
serviceConfig.workflowService = workflowService;
serviceContainer.workflowService = workflowService;

var userServiceConfig = nconf.get('mongodb');
userServiceConfig.passwordHashSecret = nconf.get('auth:hashSecret');
userServiceConfig.hashAlgorithm = nconf.get('auth:hashAlgorithm');
userServiceConfig.defaultUsersRole = nconf.get('users:defaultRole');

var userService = new UserService(userServiceConfig);
serviceConfig.userService = userService;
serviceContainer.userService = userService;

var accountService = new AccountService(serviceConfig);
serviceConfig.accountService = accountService;
serviceContainer.accountService = accountService;

var membersConfig = {}; //TODO clone object
membersConfig.defaultMemberRole = nconf.get('accounts:defaultMemberRole');
membersConfig.emailService = emailService;
membersConfig.userService = userService;
membersConfig.accountService = accountService;
var accountMemberService = new AccountMemberService(membersConfig);
serviceConfig.accountMemberService = accountMemberService;

var brandServiceMongoDB = new BrandServiceMongoDB(serviceConfig);
serviceConfig.brandServiceMongoDB = brandServiceMongoDB;

var appServiceMongoDB = new AppServiceMongoDB(serviceConfig);
serviceConfig.appServiceMongoDB = appServiceMongoDB;

var editionServiceMongoDB = new EditionServiceMongoDB(serviceConfig);
serviceConfig.editionServiceMongoDB = editionServiceMongoDB;

var galleryImageMongoDB = new GalleryImageMongoDB(serviceConfig);
serviceConfig.galleryImageMongoDB = galleryImageMongoDB;

var configServiceMongoDB = new ConfigServiceMongoDB(serviceConfig);
serviceConfig.configServiceMongoDB = configServiceMongoDB;

var screenServiceMongoDB = new ScreenServiceMongoDB(serviceConfig);
serviceConfig.screenServiceMongoDB = screenServiceMongoDB;

var deviceService = new DeviceService(serviceConfig);
serviceConfig.deviceService = deviceService;

var configurationService = new ConfigurationService(serviceConfig);
serviceConfig.configurationService = configurationService;

var catalogService = new CatalogService(serviceConfig);
serviceConfig.catalogService = catalogService;
serviceContainer.catalogService = catalogService;

var collectionService = new CollectionService(serviceConfig);
serviceConfig.collectionService = collectionService;


var imageTypeService = new ImageTypeService(serviceConfig);
serviceConfig.imageTypeService = imageTypeService;
serviceContainer.imageTypeService = imageTypeService;

var imageServiceConfig = nconf.get('cloudinary');
imageServiceConfig.imageTypeService = imageTypeService;
var imageService = new ImageService(imageServiceConfig);
serviceConfig.imageService = imageService;
serviceContainer.imageService = imageService;

var authenticationServiceConfig = {
    userService: userService,
    hashAlgorithm: nconf.get('auth:hashAlgorithm'),
    hashSecret: nconf.get('auth:hashSecret'),
    tokenSecret: nconf.get('auth:tokenSecret'),
    tokenDuration: nconf.get('auth:tokenDuration')
};

var forgotPasswordConfig = nconf.get('forgotPassword');
forgotPasswordConfig.userService = userService;
forgotPasswordConfig.emailService = emailService;
forgotPasswordConfig.hashAlgorithm = nconf.get('auth:hashAlgorithm');
forgotPasswordConfig.hashSecret = nconf.get('auth:hashSecret');
forgotPasswordConfig.tokenSecret = nconf.get('auth:tokenSecret');
var forgotPasswordService = new ForgotPasswordService(forgotPasswordConfig);
serviceConfig.forgotPasswordService = forgotPasswordService;

var schemaService = new SchemaService(serviceConfig);
serviceConfig.schemaService = schemaService;

var contentFieldService = new ContentFieldService(serviceConfig);
serviceConfig.contentFieldService = contentFieldService;
serviceContainer.contentFieldService = contentFieldService;

var contentTypeService = new ContentTypeService(serviceConfig);
serviceConfig.contentTypeService = contentTypeService;
serviceContainer.contentTypeService = contentTypeService;

var elascticSearchConfig = nconf.get('elasticsearch');
elascticSearchConfig.contentFieldService = contentFieldService;

var baseContentNodeService = new MongoDBContentNodeService(serviceConfig);
var searchContentNodeService = new ElasticSearchContentNodeService(elascticSearchConfig);

var contentNodeRepository = new ContentNodeRepository(elascticSearchConfig);
var queryTemplateRepository = new QueryTemplateRepository(serviceConfig);
var accountConfigurationRepository = new AccountConfigurationRepository(serviceConfig);

var contentNodeService = new IntegrationContentNodeService({
    baseService: baseContentNodeService,
    searchService: searchContentNodeService,
    imageService: imageService,
    workflowService: workflowService,
    collectionService: collectionService,
    contentTypeService: contentTypeService,
    contentFieldService: contentFieldService,
    workerService: workerService,
    contentNodeRepository: contentNodeRepository,
    queryTemplateRepository: queryTemplateRepository,
    accountConfigurationRepository: accountConfigurationRepository
});

serviceConfig.contentNodeService = contentNodeService;
serviceContainer.contentNodeService = contentNodeService;

var TranslationService = require('videa/services/translation/TranslationService.js');

var ClientConfigServiceMongoDB = require('videa/services/mongodb/ClientConfigServiceMongoDB.js');
var clientConfigService = new ClientConfigServiceMongoDB(serviceConfig);

var translationService = new TranslationService();

var editionStateService = new EditionStateService();

var appPublishingService = new AppPublishingService({
    appService: appServiceMongoDB,
    configService: configServiceMongoDB,
    clientConfigService: clientConfigService,
    translationService: translationService,
    editionService: editionServiceMongoDB
});

var appstudioDomainService = new AppstudioDomainService({
    appService: appServiceMongoDB,
    configService: configServiceMongoDB,
    appPublishingService: appPublishingService,
    editionService: editionServiceMongoDB,
    screenService: screenServiceMongoDB,
});

var publishedClientConfigApplicationService = new PublishedClientConfigApplicationService({
    appService: appServiceMongoDB,
    clientConfigService: clientConfigService
});

var clientEditionService = new ClientEditionService({
    appService: appServiceMongoDB,
    configService: configServiceMongoDB,
    editionService: editionServiceMongoDB,
    brandService: brandServiceMongoDB,
    editionStateService: editionStateService
});

var editionServiceIntegration = new EditionServiceIntegration({
    editionService: editionServiceMongoDB,
    configService: configServiceMongoDB,
    appService: appServiceMongoDB,
    brandService: brandServiceMongoDB,
    clientConfigService: clientConfigService,
    translationService: translationService,
    screenServiceMongoDB: screenServiceMongoDB,
    screenServiceRepository: screenServiceMongoDB,
    editionStateService: editionStateService,
    appPublishingService: appPublishingService
});

var appServiceIntegration = new AppServiceIntegration({
    appService: appServiceMongoDB,
    configService: configServiceMongoDB,
    brandService: brandServiceMongoDB,
    clientConfigService: clientConfigService,
    translationService: translationService,
    screenServiceMongoDB: screenServiceMongoDB,
    screenServiceRepository: screenServiceMongoDB,
    editionService: editionServiceMongoDB,
    editionServiceIntegration: editionServiceIntegration
});

var brandService = new BrandServiceIntegration({
    brandService: brandServiceMongoDB,
    appService: appServiceIntegration,
    appRepository: appServiceMongoDB,
    imageService: imageService,
    galleryImageService: galleryImageMongoDB
});

serviceContainer.brandService = brandService;

var configServiceIntegration = new UIConfigServiceIntegration({
    configService: configServiceMongoDB,
    editionService: editionServiceMongoDB,
    appService: appServiceMongoDB,
    screenRepository: screenServiceMongoDB
});

var screenServiceIntegration = new ScreenServiceIntegration({
    configService: configServiceMongoDB,
    screenService: screenServiceMongoDB
});

var UIConfigTranslator = require('videa/services/translation/UIConfigTranslator.js');
var GalleryImageTranslator = require('videa/services/translation/uiConfig/theme/GalleryImageTranslator.js');
var SearchSettingsTranslator = require('videa/services/translation/uiConfig/SearchSettingsTranslator.js');

var TabletUiConfigTranslator = require('videa/services/translation/uiConfig/TabletUiConfigTranslator.js');
var MobileUiConfigTranslator = require('videa/services/translation/uiConfig/MobileUiConfigTranslator.js');
var TvUiConfigTranslator = require('videa/services/translation/uiConfig/TvUiConfigTranslator.js');
var ScreenTranslator = require('videa/services/translation/ScreenTranslator.js');
var AdvanceScreenTranslator = require('videa/services/translation/screen/AdvanceScreenTranslator.js');
var SearchResultScreenTranslator = require('videa/services/translation/SearchResultScreenTranslator.js');
var CarouselTranslator = require('videa/services/translation/dataview/CarouselTranslator.js');
var ListViewTranslator = require('videa/services/translation/dataview/tileview/ListViewTranslator.js');
var SwimlaneTranslator = require('videa/services/translation/dataview/tileview/SwimlaneTranslator.js');
var GridViewTranslator = require('videa/services/translation/dataview/tileview/GridViewTranslator.js');
var ImageTranslator = require('videa/services/translation/field/ImageTranslator.js');
var LabelTranslator = require('videa/services/translation/field/LabelTranslator.js');
var CollectionsViewTranslator = require('videa/services/translation/dataview/CollectionsViewTranslator.js');

var NavigationTranslator = require('videa/services/translation/navigation/NavigationTranslator.js');
var SectionTranslator = require('videa/services/translation/navigation/SectionTranslator.js');
var SecondaryMenuTranslator = require('videa/services/translation/navigation/SecondaryMenuTranslator.js');
var SecondaryMenuSectionTranslator = require('videa/services/translation/navigation/section/SecondaryMenuSectionTranslator');
var LabelSectionTranslator = require('videa/services/translation/navigation/section/LabelSectionTranslator.js');
var ImageSectionTranslator = require('videa/services/translation/navigation/section/ImageSectionTranslator.js');

var TVNavigationTranslator = require('videa/services/translation/navigation/TVNavigationTranslator.js');
var TabletNavigationTranslator = require('videa/services/translation/navigation/TabletNavigationTranslator.js');
var MobileNavigationTranslator = require('videa/services/translation/navigation/MobileNavigationTranslator.js');

var MobileScreenTranslator = require('videa/services/translation/screen/MobileScreenTranslator.js');
var TabletScreenTranslator = require('videa/services/translation/screen/TabletScreenTranslator.js');
var TvScreenTranslator = require('videa/services/translation/screen/TvScreenTranslator.js');

var MobileSearchResultScreenTranslator = require('videa/services/translation/screen/MobileSearchResultScreenTranslator.js');
var TabletSearchResultScreenTranslator = require('videa/services/translation/screen/TabletSearchResultScreenTranslator.js');
var TVSearchResultScreenTranslator = require('videa/services/translation/screen/TVSearchResultScreenTranslator.js');

var ListenerTranslator = require('videa/services/translation/ListenerTranslator.js');
var AuthenticationTranslator = require('videa/services/translation/AuthenticationTranslator.js');
var ComponentStyleTranslator = require('videa/services/translation/component/ComponentStyleTranslator.js');
var LabelStyleTranslator = require('videa/services/translation/field/style/LabelStyleTranslator.js');
var LabelContentTranslator = require('videa/services/translation/field/content/LabelContentTranslator.js');
var FieldContentTranslator = require('videa/services/translation/field/content/FieldContentTranslator.js');
var ImageStyleTranslator = require('videa/services/translation/field/style/ImageStyleTranslator.js');
var TVHackNavigationTranslator = require('videa/services/translation/navigation/TVHackNavigationTranslator.js');
var CarouselStyleTranslator = require('videa/services/translation/dataview/style/CarouselStyleTranslator.js');
var GridViewStyleTranslator = require('videa/services/translation/dataview/style/GridViewStyleTranslator.js');
var CollectionPickerStyleTranslator = require('videa/services/translation/dataview/style/CollectionPickerStyleTranslator.js');
var TileViewStyleTranslator = require('videa/services/translation/dataview/style/TileViewStyleTranslator.js');
var DataViewHeadingStyleTranslator = require('videa/services/translation/dataview/style/DataViewHeadingStyleTranslator.js');
var ComponentControlsTranslator = require('videa/services/translation/component/ComponentControlsTranslator.js');
var ComponentTranslator = require('videa/services/translation/component/ComponentTranslator.js');
var NamedComponentTranslator = require('videa/services/translation/component/NamedComponentTranslator.js');
var SizedComponentStyleTranslator = require('videa/services/translation/component/SizedComponentStyleTranslator.js');
var LinkCollectionTranslator = require('videa/services/translation/dataview/collection/LinkCollectionTranslator.js');
var OfflineTranslator = require('videa/services/translation/dataview/collection/OfflineTranslator.js');
var RemoteCollectionTranslator = require('videa/services/translation/dataview/collection/RemoteCollectionTranslator.js');
var UIDCollectionTranslator = require('videa/services/translation/dataview/collection/UIDCollectionTranslator.js');
var DataViewHeadingTranslator = require('videa/services/translation/dataview/heading/DataViewHeadingTranslator.js');
var CollectionPickerContentTranslator = require('videa/services/translation/dataview/picker/CollectionPickerContentTranslator.js');
var CarouselTileTranslator = require('videa/services/translation/dataview/tile/CarouselTileTranslator.js');
var DataViewTileTranslator = require('videa/services/translation/dataview/tile/DataViewTileTranslator.js');
var BaseCollectionTranslator = require('videa/services/translation/dataview/BaseCollectionTranslator.js');
var BaseFieldStyleTranslator = require('videa/services/translation/field/style/BaseFieldStyleTranslator.js');
var MobileAuthenticationTranslator = require('videa/services/translation/authentication/MobileAuthenticationTranslator.js');
var TabletAuthenticationTranslator = require('videa/services/translation/authentication/TabletAuthenticationTranslator.js');
var TVAuthenticationTranslator = require('videa/services/translation/authentication/TVAuthenticationTranslator.js');
var ScreenStyleTranslator = require('videa/services/translation/screen/ScreenStyleTranslator.js');
var ImageContentTranslator = require('videa/services/translation/field/content/ImageContentTranslator.js');
var CarouselControlsTranslator = require('videa/services/translation/dataview/CarouselControlsTranslator.js');
var ChromecastThemeTranslator = require('videa/services/translation/chromecast/ThemeTranslator.js');
var ChromecastTitleTranslator = require('videa/services/translation/chromecast/TitleTranslator.js');
var ChromecastReceiverTranslator = require('videa/services/translation/chromecast/ReceiverTranslator.js');
var ChromecastTranslator = require('videa/services/translation/ChromecastTranslator.js');
var ToggleStyleTranslator = require('videa/services/translation/field/style/ToggleStyleTranslator.js');
var ToggleTranslator = require('videa/services/translation/field/ToggleTranslator.js');
var TextFieldStyleTranslator = require('videa/services/translation/field/style/TextFieldStyleTranslator.js');
var TextFieldContentTranslator = require('videa/services/translation/field/content/TextFieldContentTranslator.js');
var TextFieldTranslator = require('videa/services/translation/field/TextFieldTranslator.js');
var ColoredComponentStyleTranslator = require('videa/services/translation/component/ColoredComponentStyleTranslator.js');
var SectionStyleTranslator = require('videa/services/translation/navigation/style/SectionStyleTranslator.js');
var SecondaryMenuStyleTranslator = require('videa/services/translation/navigation/style/SecondaryMenuStyleTranslator.js');
var DrawerMenuTranslator = require('videa/services/translation/navigation/menu/DrawerMenuTranslator.js');
var BottomMenuTranslator = require('videa/services/translation/navigation/menu/BottomMenuTranslator.js');
var LocalProviderTranslator = require('videa/services/translation/watchhistory/LocalProviderTranslator.js');
var MpxProviderTranslator = require('videa/services/translation/watchhistory/MpxProviderTranslator.js');
var WatchHistoryTranslator = require('videa/services/translation/WatchHistoryTranslator.js');
var UserMpxProviderTranslator = require('videa/services/translation/user/MpxProviderTranslator.js');
var UserVideaProviderTranslator = require('videa/services/translation/user/VideaProviderTranslator.js');
var UserTranslator = require('videa/services/translation/UserTranslator.js');
var AdvanceComponentTranslator = require('videa/services/translation/component/AdvanceComponentTranslator.js');
var GoogleAnalyticTranslator = require('videa/services/translation/uiConfig/analytic/GoogleAnalyticTranslator.js');
var CustomDataTranslator = require('videa/services/translation/CustomDataTranslator.js');
var CustomAnalyticTranslator = require('videa/services/translation/uiConfig/analytic/CustomAnalyticTranslator.js');
var MobileGoogleAnalyticTranslator = require('videa/services/translation/uiConfig/analytic/MobileGoogleAnalyticTranslator.js');
var TabletGoogleAnalyticTranslator = require('videa/services/translation/uiConfig/analytic/TabletGoogleAnalyticTranslator.js');
var TvGoogleAnalyticTranslator = require('videa/services/translation/uiConfig/analytic/TvGoogleAnalyticTranslator.js');
var ExternalConfigTranslator = require('videa/services/translation/ExternalConfigTranslator.js');
var GemiusAnalyticsTranslator = require('videa/services/translation/uiConfig/analytic/GemiusAnalyticsTranslator.js');
var MobileGemiusAnalyticsTranslator = require('videa/services/translation/uiConfig/analytic/MobileGemiusAnalyticsTranslator.js');
var TabletGemiusAnalyticsTranslator = require('videa/services/translation/uiConfig/analytic/TabletGemiusAnalyticsTranslator.js');
var TVGemiusAnalyticsTranslator = require('videa/services/translation/uiConfig/analytic/TvGemiusAnalyticsTranslator.js');
var ExternalEditionTranslator = require('videa/services/translation/ExternalEditionTranslator.js');
var BaseEditionTranslator = require('videa/services/translation/BaseEditionTranslator.js');
var MobileAppEditionTranslator = require('videa/services/translation/MobileAppEditionTranslator.js');
var TabletAppEditionTranslator = require('videa/services/translation/TabletAppEditionTranslator.js');
var TVAppEditionTranslator = require('videa/services/translation/TVAppEditionTranslator.js');
var MobileBundleEditionTranslator = require('videa/services/translation/bundle/MobileBundleEditionTranslator.js');
var TabletBundleEditionTranslator = require('videa/services/translation/bundle/TabletBundleEditionTranslator.js');
var TVBundleEditionTranslator = require('videa/services/translation/bundle/TVBundleEditionTranslator.js');
var MobileAkamaiVocCmsTranslator = require('videa/services/translation/cms/MobileAkamaiVocCmsTranslator');
var MobileCustomCmsTranslator = require('videa/services/translation/cms/MobileCustomCmsTranslator');
var MobileGenericCmsTranslator = require('videa/services/translation/cms/MobileGenericCmsTranslator');
var MobileMpxCmsTranslator = require('videa/services/translation/cms/MobileMpxCmsTranslator');
var MobileTrueLogicCmsTranslator = require('videa/services/translation/cms/MobileTrueLogicCmsTranslator');
var MobileVideaCmsTranslator = require('videa/services/translation/cms/MobileVideaCmsTranslator');
var MobileVimondCmsTranslator = require('videa/services/translation/cms/MobileVimondCmsTranslator');

var TabletAkamaiVocCmsTranslator = require('videa/services/translation/cms/TabletAkamaiVocCmsTranslator');
var TabletCustomCmsTranslator = require('videa/services/translation/cms/TabletCustomCmsTranslator');
var TabletGenericCmsTranslator = require('videa/services/translation/cms/TabletGenericCmsTranslator');
var TabletMpxCmsTranslator = require('videa/services/translation/cms/TabletMpxCmsTranslator');
var TabletTrueLogicCmsTranslator = require('videa/services/translation/cms/TabletTrueLogicCmsTranslator');
var TabletVideaCmsTranslator = require('videa/services/translation/cms/TabletVideaCmsTranslator');
var TabletVimondCmsTranslator = require('videa/services/translation/cms/TabletVimondCmsTranslator');

var TVAkamaiVocCmsTranslator = require('videa/services/translation/cms/TVAkamaiVocCmsTranslator');
var TVCustomCmsTranslator = require('videa/services/translation/cms/TVCustomCmsTranslator');
var TVGenericCmsTranslator = require('videa/services/translation/cms/TVGenericCmsTranslator');
var TVMpxCmsTranslator = require('videa/services/translation/cms/TVMpxCmsTranslator');
var TVTrueLogicCmsTranslator = require('videa/services/translation/cms/TVTrueLogicCmsTranslator');
var TVVideaCmsTranslator = require('videa/services/translation/cms/TVVideaCmsTranslator');
var TVVimondCmsTranslator = require('videa/services/translation/cms/TVVimondCmsTranslator');

var StandardAspectRatioTranslator = require('videa/services/translation/aspectRatio/StandardAspectRatioTranslator');
var CustomAspectRatioTranslator = require('videa/services/translation/aspectRatio/CustomAspectRatioTranslator');
var BaseFontTranslator = require('videa/services/translation/font/BaseFontTranslator');
var OpenSansTranslator = require('videa/services/translation/font/OpenSansTranslator');
var RobotoTranslator = require('videa/services/translation/font/RobotoTranslator');
var SanFranciscoTranslator = require('videa/services/translation/font/SanFranciscoTranslator');
var CustomFontTranslator = require('videa/services/translation/font/CustomFontTranslator');
var ProgressBarTranslator = require('videa/services/translation/field/ProgressBarTranslator');
var ProgressBarStyleTranslator = require('videa/services/translation/field/style/ProgressBarStyleTranslator');

var FavoritesTranslator = require('videa/services/translation/FavoritesTranslator.js');
var FavoritesLocalProviderTranslator = require('videa/services/translation/favorites/LocalProviderTranslator.js');

var ColorTranslator = require('videa/services/translation/color/ColorTranslator.js');
var OffsetColorTranslator = require('videa/services/translation/color/OffsetColorTranslator.js');
var GradientColorTranslator = require('videa/services/translation/color/GradientColorTranslator.js');
var ComplexColorTranslator = require('videa/services/translation/color/ComplexColorTranslator.js');
var NavigationStyleTranslator = require('videa/services/translation/screen/NavigationStyleTranslator.js');
var TopBarStyleTranslator = require('videa/services/translation/screen/TopBarStyleTranslator.js');
var TopBarTitleStyleTranslator = require('videa/services/translation/screen/TitleStyleTranslator.js');
var BottomMenuStyleTranslator = require('videa/services/translation/navigation/style/BottomMenuStyleTranslator.js');
var MenuTabTranslator = require('videa/services/translation/navigation/menu/tab/MenuTabTranslator.js');
var FavoriteCollectionTranslator = require('videa/services/translation/dataview/collection/FavoriteCollectionTranslator.js');
var WatchHistoryCollectionTranslator = require('videa/services/translation/dataview/collection/WatchHistoryCollectionTranslator.js');
var WebViewTranslator = require('videa/services/translation/webview/WebViewTranslator.js');
var WebViewStyleTranslator = require('videa/services/translation/webview/WebViewStyleTranslator.js');
var WebViewContentTranslator = require('videa/services/translation/webview/WebViewContentTranslator.js');
var WelcomeScreenTranslator = require('videa/services/translation/WelcomeScreenTranslator.js');
var SystemEventTranslator = require('videa/services/translation/uiConfig/analytic/systemEvents/SystemEventTranslator.js');
var LayerTranslator = require('videa/services/translation/component/LayerTranslator.js');
var TileLayerTranslator = require('videa/services/translation/component/TileLayerTranslator.js');
var LayerControlsTranslator = require('videa/services/translation/component/LayerControlsTranslator.js');

translationService.registerTranslators(
    [{
            translatorClass: TvGoogleAnalyticTranslator
        }, {
            translatorClass: TabletGoogleAnalyticTranslator
        }, {
            translatorClass: MobileGoogleAnalyticTranslator
        }, {
            translatorClass: CustomAnalyticTranslator
        }, {
            translatorClass: CustomDataTranslator
        }, {
            translatorClass: GoogleAnalyticTranslator
        }, {
            translatorClass: AdvanceComponentTranslator
        }, {
            translatorClass: LocalProviderTranslator
        }, {
            translatorClass: MpxProviderTranslator
        }, {
            translatorClass: WatchHistoryTranslator
        }, {
            translatorClass: UserMpxProviderTranslator
        }, {
            translatorClass: UserVideaProviderTranslator
        }, {
            translatorClass: UserTranslator
        }, {
            translatorClass: BottomMenuTranslator
        }, {
            translatorClass: DrawerMenuTranslator
        }, {
            translatorClass: SecondaryMenuStyleTranslator
        }, {
            translatorClass: SectionStyleTranslator
        }, {
            translatorClass: ImageSectionTranslator
        }, {
            translatorClass: LabelSectionTranslator
        }, {
            translatorClass: ColoredComponentStyleTranslator
        }, {
            translatorClass: TextFieldTranslator
        }, {
            translatorClass: TextFieldContentTranslator
        }, {
            translatorClass: TextFieldStyleTranslator
        }, {
            translatorClass: ToggleTranslator
        }, {
            translatorClass: ToggleStyleTranslator
        }, {
            translatorClass: ChromecastTranslator
        }, {
            translatorClass: ChromecastThemeTranslator
        }, {
            translatorClass: ChromecastTitleTranslator
        }, {
            translatorClass: ChromecastReceiverTranslator
        }, {
            translatorClass: CarouselControlsTranslator
        }, {
            translatorClass: ImageContentTranslator
        }, {
            translatorClass: ScreenStyleTranslator
        }, {
            translatorClass: MobileAuthenticationTranslator
        }, {
            translatorClass: TabletAuthenticationTranslator
        }, {
            translatorClass: TVAuthenticationTranslator
        }, {
            translatorClass: BaseFieldStyleTranslator
        }, {
            translatorClass: DataViewTileTranslator
        }, {
            translatorClass: CarouselTileTranslator
        }, {
            translatorClass: CollectionPickerContentTranslator
        }, {
            translatorClass: DataViewHeadingTranslator
        }, {
            translatorClass: UIDCollectionTranslator
        }, {
            translatorClass: RemoteCollectionTranslator
        }, {
            translatorClass: OfflineTranslator
        }, {
            translatorClass: LinkCollectionTranslator
        }, {
            translatorClass: SizedComponentStyleTranslator
        }, {
            translatorClass: NamedComponentTranslator
        }, {
            translatorClass: ComponentTranslator
        }, {
            translatorClass: CollectionPickerStyleTranslator
        }, {
            translatorClass: TileViewStyleTranslator
        }, {
            translatorClass: DataViewHeadingStyleTranslator
        }, {
            translatorClass: SectionTranslator
        }, {
            translatorClass: SecondaryMenuSectionTranslator
        }, {
            translatorClass: SecondaryMenuTranslator
        }, {
            translatorClass: NavigationTranslator
        }, {
            translatorClass: TVNavigationTranslator
        }, {
            translatorClass: TabletNavigationTranslator
        }, {
            translatorClass: MobileNavigationTranslator
        }, {
            translatorClass: ListenerTranslator
        }, {
            translatorClass: AuthenticationTranslator
        }, {
            translatorClass: ComponentStyleTranslator
        }, {
            translatorClass: LabelStyleTranslator
        }, {
            translatorClass: ImageStyleTranslator
        }, {
            translatorClass: LabelContentTranslator
        }, {
            translatorClass: FieldContentTranslator
        }, {
            translatorClass: GalleryImageTranslator,
            config: {
                galleryService: galleryImageMongoDB
            }
        }, {
            translatorClass: SearchSettingsTranslator
        }, {
            translatorClass: UIConfigTranslator,
            config: {
                galleryService: galleryImageMongoDB,
                screenService: screenServiceIntegration
            }
        }, {
            translatorClass: TabletUiConfigTranslator,
            config: {
                galleryService: galleryImageMongoDB,
                screenService: screenServiceIntegration
            }
        }, {
            translatorClass: MobileUiConfigTranslator,
            config: {
                galleryService: galleryImageMongoDB,
                screenService: screenServiceIntegration
            }
        }, {
            translatorClass: TvUiConfigTranslator,
            config: {
                galleryService: galleryImageMongoDB,
                screenService: screenServiceIntegration
            }
        }, {
            translatorClass: ScreenTranslator
        }, {
            translatorClass: AdvanceScreenTranslator
        }, {
            translatorClass: SearchResultScreenTranslator
        }, {
            translatorClass: CarouselTranslator
        }, {
            translatorClass: SwimlaneTranslator
        }, {
            translatorClass: ListViewTranslator
        }, {
            translatorClass: GridViewTranslator
        }, {
            translatorClass: ImageTranslator
        }, {
            translatorClass: LabelTranslator
        }, {
            translatorClass: CollectionsViewTranslator
        },

        {
            translatorClass: MobileScreenTranslator
        }, {
            translatorClass: TabletScreenTranslator
        }, {
            translatorClass: TvScreenTranslator
        },

        {
            translatorClass: MobileSearchResultScreenTranslator
        }, {
            translatorClass: TabletSearchResultScreenTranslator
        }, {
            translatorClass: TVSearchResultScreenTranslator
        },

        {
            translatorClass: TVHackNavigationTranslator
        }, {
            translatorClass: CarouselStyleTranslator
        }, {
            translatorClass: GridViewStyleTranslator
        }, {
            translatorClass: ComponentControlsTranslator
        }, {
            translatorClass: ExternalConfigTranslator
        }, {
            translatorClass: GemiusAnalyticsTranslator
        }, {
            translatorClass: MobileGemiusAnalyticsTranslator
        }, {
            translatorClass: TabletGemiusAnalyticsTranslator
        }, {
            translatorClass: TVGemiusAnalyticsTranslator
        }, {
            translatorClass: ExternalEditionTranslator,
            config: {
                configService: configServiceMongoDB
            }
        }, {
            translatorClass: BaseEditionTranslator,
            config: {
                configService: configServiceMongoDB
            }
        }, {
            translatorClass: MobileAppEditionTranslator,
            config: {
                configService: configServiceMongoDB
            }
        }, {
            translatorClass: TabletAppEditionTranslator,
            config: {
                configService: configServiceMongoDB
            }
        }, {
            translatorClass: TVAppEditionTranslator,
            config: {
                configService: configServiceMongoDB
            }
        }, {
            translatorClass: MobileBundleEditionTranslator,
            config: {
                configService: configServiceMongoDB
            }
        }, {
            translatorClass: TabletBundleEditionTranslator,
            config: {
                configService: configServiceMongoDB
            }
        }, {
            translatorClass: TVBundleEditionTranslator,
            config: {
                configService: configServiceMongoDB
            }
        }, {
            translatorClass: MobileAkamaiVocCmsTranslator
        }, {
            translatorClass: MobileCustomCmsTranslator
        }, {
            translatorClass: MobileGenericCmsTranslator
        }, {
            translatorClass: MobileMpxCmsTranslator
        }, {
            translatorClass: MobileTrueLogicCmsTranslator
        }, {
            translatorClass: MobileVideaCmsTranslator
        }, {
            translatorClass: MobileVimondCmsTranslator
        },

        {
            translatorClass: TabletAkamaiVocCmsTranslator
        }, {
            translatorClass: TabletCustomCmsTranslator
        }, {
            translatorClass: TabletGenericCmsTranslator
        }, {
            translatorClass: TabletMpxCmsTranslator
        }, {
            translatorClass: TabletTrueLogicCmsTranslator
        }, {
            translatorClass: TabletVideaCmsTranslator
        }, {
            translatorClass: TabletVimondCmsTranslator
        },

        {
            translatorClass: TVAkamaiVocCmsTranslator
        }, {
            translatorClass: TVCustomCmsTranslator
        }, {
            translatorClass: TVGenericCmsTranslator
        }, {
            translatorClass: TVMpxCmsTranslator
        }, {
            translatorClass: TVTrueLogicCmsTranslator
        }, {
            translatorClass: TVVideaCmsTranslator
        }, {
            translatorClass: TVVimondCmsTranslator
        }, {
            translatorClass: BaseFontTranslator
        }, {
            translatorClass: OpenSansTranslator
        }, {
            translatorClass: RobotoTranslator
        }, {
            translatorClass: SanFranciscoTranslator
        }, {
            translatorClass: CustomFontTranslator
        }, {
            translatorClass: StandardAspectRatioTranslator
        }, {
            translatorClass: CustomAspectRatioTranslator
        }, {
            translatorClass: ProgressBarTranslator
        }, {
            translatorClass: ProgressBarStyleTranslator
        }, {
            translatorClass: FavoritesTranslator
        }, {
            translatorClass: FavoritesLocalProviderTranslator
        }, {
            translatorClass: ColorTranslator
        }, {
            translatorClass: OffsetColorTranslator
        }, {
            translatorClass: GradientColorTranslator
        }, {
            translatorClass: ComplexColorTranslator
        }, {
            translatorClass: NavigationStyleTranslator
        }, {
            translatorClass: TopBarStyleTranslator
        }, {
            translatorClass: TopBarTitleStyleTranslator
        }, {
            translatorClass: BottomMenuStyleTranslator
        }, {
            translatorClass: MenuTabTranslator
        }, {
            translatorClass: FavoriteCollectionTranslator
        }, {
            translatorClass: WatchHistoryCollectionTranslator
        }, {
            translatorClass: WebViewTranslator
        }, {
            translatorClass: WebViewStyleTranslator
        }, {
            translatorClass: WebViewContentTranslator
        }, {
            translatorClass: WelcomeScreenTranslator
        }, {
            translatorClass: SystemEventTranslator
        }, {
            translatorClass: LayerTranslator
        }, {
            translatorClass: TileLayerTranslator
        }, {
            translatorClass: LayerControlsTranslator
        }
    ]);

serviceConfig.brandService = brandService;
serviceConfig.appService = appServiceIntegration;
serviceConfig.editionService = editionServiceIntegration;
serviceConfig.configService = configServiceIntegration;
serviceConfig.screenService = screenServiceIntegration;

var authenticationService = new AuthenticationService(authenticationServiceConfig);
serviceContainer.authenticationService = authenticationService;

var signUpService = new SignUpService(serviceConfig);

var securityConfig = {
    aclService: aclService,
    authenticationService: authenticationService,
    userService: userService
};
var securityService = new SecurityService(securityConfig);

var swaggerDoc = require('../spec/swagger/index.js');
var specConfig = {
    securityService: securityService,
    swaggerDoc: swaggerDoc
};
var specService = new SpecService(specConfig);

var lockServiceConfig = nconf.get('mongodb');
lockServiceConfig.services = serviceConfig;
var lockService = new LockServices(lockServiceConfig);
serviceConfig.lockService = lockService;

var encodingService = new EncodingService(serviceConfig);
serviceConfig.encodingService = encodingService;

var videoEncodingConfig = {
    encodingService: encodingService
};
var videoEncodingServices = {};

var UpLinkSchema = require('videa/cms/schema/encoding/provider/UpLynkProvider');
videoEncodingServices[UpLinkSchema.ID] = new UpLynkVideoEncodingService();

videoEncodingConfig.videoEncodingServices = videoEncodingServices;
var videoEncodingService = new VideoEncodingService(videoEncodingConfig);
serviceConfig.videoEncodingService = videoEncodingService;

serviceConfig.lambdaService = lambdaService;
var mappingService = new MappingService(serviceConfig);
serviceConfig.mappingService = mappingService;

var DefaultDataService = require('videa/services/common-default-data/DefaultDataService.js');
var defaultDataService = new DefaultDataService({
    services: serviceContainer
});
var SampleDataService = require('videa/services/common-default-data/SampleDataService.js');
var sampleDataService = new SampleDataService({
    services: serviceContainer
});

var fileManagerConfig = {};
fileManagerConfig.remoteAssetService = remoteAssetService;
fileManagerConfig.config = nconf.get('file');
var fileManagerService = new FileManagerService(fileManagerConfig);
serviceConfig.fileManagerService = fileManagerService;

var contentIngestionConfig = {};
contentIngestionConfig.contentNodeService = contentNodeService;
contentIngestionConfig.mappingService = mappingService;
contentIngestionConfig.workerService = workerService;

var contentIngestionService = new ContentIngestionService(contentIngestionConfig);

serviceConfig.sampleDataService = sampleDataService;
serviceConfig.contentIngestionService = contentIngestionService;

var appstudioAccountCreation = new AppstudioAccountCreation({
    brandService: brandServiceMongoDB,
    appService: appServiceMongoDB,
    editionService: editionServiceMongoDB,
    configService: configServiceMongoDB,
    screenService: screenServiceMongoDB,
    accountService: accountService,
    galleryImageService: galleryImageMongoDB,
    imageService: imageService,
    clientConfigService: clientConfigService
});

module.exports = {
    workflowService: workflowService,
    userService: userService,
    accountService: accountService,
    accountMemberService: accountMemberService,
    brandServiceMongoDB: brandServiceMongoDB,
    appServiceMongoDB: appServiceMongoDB,
    editionServiceMongoDB: editionServiceMongoDB,
    galleryImageMongoDB: galleryImageMongoDB,
    deviceService: deviceService,
    configurationService: configurationService,
    catalogService: catalogService,
    collectionService: collectionService,
    contentTypeService: contentTypeService,
    contentFieldService: contentFieldService,
    contentNodeService: contentNodeService,
    authenticationService: authenticationService,
    signUpService: signUpService,
    imageTypeService: imageTypeService,
    imageService: imageService,
    defaultDataService: defaultDataService,
    sampleDataService: sampleDataService,
    brandService: brandService,
    emailService: emailService,
    forgotPasswordService: forgotPasswordService,
    appService: appServiceIntegration,
    configService: configServiceIntegration,
    screenServiceIntegration: screenServiceIntegration,
    aclService: aclService,
    securityService: securityService,
    versionService: versionService,
    schemaService: schemaService,
    clientConfigService: clientConfigService,
    specService: specService,
    workerService: workerService,
    lockService: lockService,
    encodingService: encodingService,
    videoEncodingService: videoEncodingService,
    mappingService: mappingService,
    fileManagerService: fileManagerService,
    contentIngestionService: contentIngestionService,
    configServiceMongoDB: configServiceMongoDB,
    screenServiceMongoDB: screenServiceMongoDB,
    lambdaService: lambdaService,
    appstudioAccountCreation: appstudioAccountCreation,
    appPublishingService: appPublishingService,
    editionService: editionServiceIntegration,
    editionStateService: editionStateService,
    appstudioDomainService: appstudioDomainService,
    clientEditionService: clientEditionService,
    publishedClientConfigApplicationService: publishedClientConfigApplicationService
};
