var nconf = require('nconf');
nconf.env().argv();

var env = nconf.get('NODE_ENV');
console.log("ENV: " + env);

if (env !== undefined) {
    // load config override for the given environment
    console.log("Loading environment conf: " + __dirname + '/config.' + env + '.json');
    nconf.file(env, __dirname + '/config.' + env + '.json');
}

console.log("Loading default conf: " + __dirname + '/config.json');
nconf.file(__dirname + '/config.json');

module.exports = nconf;

