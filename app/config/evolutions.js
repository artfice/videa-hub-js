var Promise = require('bluebird');
var nconf = require('nconf');
var services = require('./services.js');

var EvolutionService = require('videa/services/evolutions/EvolutionService.js');
var CommonEvolutionService = require('videa/services/common/EvolutionService.js');
var MongoDBEvolutionService = require('videa/services/mongodb/EvolutionService.js');
var IntegrationEvolutionService = require('videa/services/integration/EvolutionService.js');
var ElasticSearchEvolutionService = require('videa/services/elasticsearch/EvolutionService.js');

var mongoConfig = nconf.get('mongodb');
var elasticSearchConfig = nconf.get('elasticsearch');

// order matters when multiple modules need to apply an evolution for the same build number
var modules = [
    {
        name: "mongoDB",
        service: new MongoDBEvolutionService(mongoConfig, services),
        evolutionsFolder: "mongodb"
    },
    {
        name: "elasticsearch",
        service: new ElasticSearchEvolutionService(elasticSearchConfig, services),
        evolutionsFolder: "elasticsearch"
    },
    {
        name: "integration",
        service: new IntegrationEvolutionService(services),
        evolutionsFolder: "integration"
    },
    {
        name: "common",
        service: new CommonEvolutionService(services),
        evolutionsFolder: "common"
    }
];

var evolutionService = new EvolutionService(services, modules);

module.exports = {
    applyEvolutions: function (previousVersion, currentVersion) {
        return evolutionService.apply(previousVersion, currentVersion);
    }
}
