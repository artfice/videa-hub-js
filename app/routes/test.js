"use strict";

var services = require('../config/services.js');
var workerService = services.workerService;
var contentNodeService = services.contentNodeService;

var postRandomData = function (req, res) {
	var settings = req.swagger.params.dataSettings.value;
	settings.accountId = req.swagger.params.accountId.value;


	workerService.add({
			parameters: {settings: settings},
			taskName: "AddRandomData"
		}).then(function(worker){
		res.status(200).json(worker);
	}).catch(function (e) {
		res.status(400).json(e);
	});
};

var postRandomDataTestGet = function(req, res){
	var settings = {
		totalNodesToAdd: 20000,
		nodesToAddEachTime: 50
	};
	settings.accountId = req.swagger.params.accountId.value;


	workerService.add({
		parameters: {
			settings: settings
		},
		taskName: "AddRandomData"
	}).then(function(worker){
		res.status(200).json(worker);
	}).catch(function (e) {
		res.status(400).json(e);
	});
};

var resetNodesFromBase = function(req, res){
	var accountId = req.swagger.params.accountId.value;

    contentNodeService.startReIndex(accountId).then(function(worker){
		res.status(202).json(worker);
	}).catch(function (e) {
		res.status(400).json(e);
	});
}

var resetImageTypesTransforms = function(req, res){
    workerService.add({
        parameters: {},
        taskName: "ResetCloudinaryTransforms"
    }).then(function(worker){
        res.status(200).json(worker);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};

var basicSearch = function(req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value);

    var query = req.swagger.params.query.value;
    if (query) {
        try {
            query = JSON.parse(query);
        } catch(error) {
            //Cannot parse query. Proceeding anyway.
        }
    }

    contentNodeService.basicSearch(accountId, query).then(
        function(result) {
            res.status(200).json(result);
        }
    ).catch(
        function(error) {
            res.status(400).json(error);
        }
    );
};

var basicSearchUsingMultipleFields = function(req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value);
    var query = req.swagger.params.query.value;

    var fields = undefined;
    try {
        fields = JSON.parse(decodeURIComponent(req.swagger.params.fields.value));
    } catch (e) {
        //Cannot parse fields. Proceeding anyway.
    }

    contentNodeService.basicSearchUsingMultipleFields(accountId, query, fields).then(
        function(result) {
            res.status(200).json(result);
        }
    ).catch(
        function(error) {
            res.status(400).json(error);
        }
    );
};

module.exports = {
	postRandomData : postRandomData,
	postRandomDataTestGet : postRandomDataTestGet,
	resetNodesFromBase : resetNodesFromBase,
    resetImageTypesTransforms : resetImageTypesTransforms,
    basicSearch: basicSearch,
    basicSearchUsingMultipleFields: basicSearchUsingMultipleFields
};
