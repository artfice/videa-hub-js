"use strict";

var ImageType = require('videa/dto/ImageType');
var imageTypeService = require('../config/services.js').imageTypeService;

var getImageTypes = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        searchOptions = {};
    searchOptions.offset = req.swagger.params.offset.value;
    searchOptions.setSize = req.swagger.params.setSize.value;
    searchOptions.sort = decodeURIComponent(req.swagger.params.sort.value);
    searchOptions.ascending = req.swagger.params.ascending.value;

    imageTypeService.search(accountId, searchOptions).then(function (result) {
        res.status(200).json(result);
        return null;
    }).catch(function (e) {
        res.status(400).json(e);
        return null;
    });
};

var postImageType = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value);

    imageTypeService.add(accountId, req.swagger.params.imageType.value).then(function (result) {
        res.status(200).json(result);
        return null;
    }).catch(function (e) {
        res.status(400).json(e);
        return null;
    });
};

var getImageType = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        imageTypeId = decodeURIComponent(req.swagger.params.imageTypeId.value);

    imageTypeService.getById(accountId, imageTypeId).then(function (result) {
        res.status(200).json(result);
        return null;
    }).catch(function (e) {
        res.status(400).json(e);
        return null;
    });
};

var putImageType = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        imageTypeId = decodeURIComponent(req.swagger.params.imageTypeId.value);

    imageTypeService.update(accountId, imageTypeId, req.swagger.params.imageType.value).then(function (result) {
        res.status(200).json(result);
        return null;
    }).catch(function (e) {
        res.status(400).json(e);
        return null;
    });
};

var removeImageType = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        imageTypeId = decodeURIComponent(req.swagger.params.imageTypeId.value);

    imageTypeService.remove(accountId, imageTypeId).then(function (result) {
        res.status(200).json(result);
        return null;
    }).catch(function (e) {
        res.status(400).json(e);
        return null;
    });
};

module.exports = {
    getImageTypes: getImageTypes,
    postImageType: postImageType,
    getImageType: getImageType,
    putImageType: putImageType,
    removeImageType: removeImageType
};
