"use strict";

var accountMemberService = require('../config/services.js').accountMemberService;
var SearchOperator = require('videa/dto/SearchOperator.js');

var getMember = function(req, res){

    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        userId = decodeURIComponent(req.swagger.params.userId.value);

    accountMemberService.get(accountId, userId).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};


var getMembers = function (req, res) {

    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        searchOptions = {};
    searchOptions.offset = req.swagger.params.offset.value;
    searchOptions.setSize = req.swagger.params.setSize.value;
    searchOptions.sort = decodeURIComponent(req.swagger.params.sort.value);
    searchOptions.ascending = req.swagger.params.ascending.value;

	var qValue =  req.swagger.params.query.value;

	if(qValue) {
		searchOptions.query = {operator: SearchOperator.Or , value: [
			{operator: SearchOperator.Equal, value: qValue, field : 'username'},
			{operator: SearchOperator.Equal, value: qValue, field : 'firstName'},
			{operator: SearchOperator.Equal, value: qValue, field : 'lastName'},
			{operator: SearchOperator.Equal, value: qValue, field : 'email'}
		]
		};
	}

    accountMemberService.search(accountId, searchOptions).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};

var postMember = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        member = req.swagger.params.member.value;

    accountMemberService.addNewUser(accountId, member).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};

var putMember = function(req, res){

    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        userId = decodeURIComponent(req.swagger.params.userId.value),
        member = req.swagger.params.member.value;

    accountMemberService.updateExitingUser(accountId, userId, member).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};


var postExistingMember = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        userId = decodeURIComponent(req.swagger.params.userId.value),
        member = req.swagger.params.member.value;

    accountMemberService.addExistingUser(accountId, userId, member).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};

var removeMember = function(req, res){
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        userId = decodeURIComponent(req.swagger.params.userId.value);

    accountMemberService.removeMember(accountId, userId).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};

module.exports = {
    getMember : getMember,
    getMembers: getMembers,
    postMember: postMember,
    postExistingMember: postExistingMember,
    putMember : putMember,
    removeMember : removeMember
};
