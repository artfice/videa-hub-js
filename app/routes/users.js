"use strict";

var userService = require('../config/services.js').userService;
var SearchOperator = require('videa/dto/SearchOperator.js');

var getUsers = function (req, res) {

	var searchOptions = {};
	searchOptions.offset = req.swagger.params.offset.value;
	searchOptions.setSize = req.swagger.params.setSize.value;
	searchOptions.sort = req.swagger.params.sort.value;
	searchOptions.ascending = req.swagger.params.ascending.value;
	var qValue =  req.swagger.params.query.value;

	if(qValue) {
		searchOptions.query = {operator: SearchOperator.Or , value: [
			{operator: SearchOperator.Equal, value: qValue, field : 'username'},
			{operator: SearchOperator.Equal, value: qValue, field : 'firstName'},
			{operator: SearchOperator.Equal, value: qValue, field : 'lastName'},
			{operator: SearchOperator.Equal, value: qValue, field : 'email'}
		]
		};
	}

	userService.search(searchOptions).then(function (result) {
		res.status(200).json(result);
	}).catch(function (e) {
		res.status(400).json(e);
	});
};

var postUser = function (req, res) {

	userService.add(req.swagger.params.user.value).then(function (result) {
		res.status(200).json(result);
	}).catch(function (e) {
		res.status(400).json(e);
	});
};

var getUser = function (req, res) {
	var userId = req.swagger.params.userId.value;

	userService.getById(userId).then(function (result) {
		console.log(result);
		res.status(200).json(result);
	}).catch(function (e) {
		res.status(400).json(e);
	});
};

var putUser = function (req, res) {
	userService.update(req.swagger.params.userId.value, req.swagger.params.user.value).then(function (result) {
		res.status(200).json(result);
	}).catch(function (e) {
		res.status(400).json(e);
	});
};

var removeUser = function (req, res) {

	var userId = req.swagger.params.userId.value;
	userService.remove(userId).then(function (result) {
		res.status(200).json(result);
	}).catch(function (e) {
		res.status(400).json(e);
	});
};

module.exports = {
	getUsers: getUsers,
	postUser: postUser,
	getUser: getUser,
	putUser: putUser,
	removeUser: removeUser
};
