"use strict";

var services = require('../config/services.js');
var versionService = services.versionService;

var getVersion = function (req, res) {

    versionService.get().then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};

var getVersionString = function (req, res) {
    res.status(200).json(versionService.getPackageVersion());
};

var getLicenses = function (req, res) {
    res.status(200).json(versionService.getLicense());
};

module.exports = {
    getVersion: getVersion,
    getVersionString: getVersionString,
    getLicenses: getLicenses
};
