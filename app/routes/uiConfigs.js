"use strict";
/**
 * Created by ryanwong on 2015-10-21.
 */

var appService = require('../config/services.js').appService;
var appPublishingService = require('../config/services.js').appPublishingService;
var configService = require('../config/services.js').configService;

// TODO: there should be no screen related services needed here
var screenService = require('../config/services.js').screenServiceIntegration;
var videaErrors = require('../node_modules/videa/dto/VideaError.js');
var ExternalResourceError = videaErrors.ExternalResourceError;
var ItemNotFoundError = videaErrors.ItemNotFoundError;
var JSONParseError = videaErrors.JSONParseError;

var getConfig = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        configId = decodeURIComponent(req.swagger.params.configId.value),
        editionId = decodeURIComponent(req.swagger.params.editionId.value);

    configService.getConfigById(accountId, configId).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        console.log(e);
        res.status(400).json(e);
    });

};

var createConfig = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        editionId = decodeURIComponent(req.swagger.params.editionId.value),
        config = req.swagger.params.config.value;

    configService.addConfig(accountId, editionId, config).then(function (config) {
        res.status(200).json(config);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};

var editConfig = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        configId = decodeURIComponent(req.swagger.params.configId.value),
        editionId = decodeURIComponent(req.swagger.params.editionId.value),
        config = req.swagger.params.config.value;
    configService.updateConfig(accountId, editionId, configId, config).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        console.log(e);
        res.status(400).json(e);
    });

};

var duplicateConfig = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        configId = decodeURIComponent(req.swagger.params.configId.value),
        editionId = decodeURIComponent(req.swagger.params.editionId.value);
    configService.duplicateConfig(accountId, editionId, configId).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        console.log(e);
        res.status(400).json(e);
    });

};

var deleteConfig = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        configId = decodeURIComponent(req.swagger.params.configId.value),
        editionId = decodeURIComponent(req.swagger.params.editionId.value);

    configService.removeConfig(accountId, editionId, configId).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};

module.exports = {
    getConfig: getConfig,
    deleteConfig: deleteConfig,
    editConfig: editConfig,
    createConfig: createConfig,
    duplicateConfig: duplicateConfig
};