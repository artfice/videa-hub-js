"use strict";

var mappingService = require('../config/services.js').mappingService;
var SearchOperator = require('videa/dto/SearchOperator.js');

var getMappings = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        searchOptions = {};
    searchOptions.offset = req.swagger.params.offset.value;
    searchOptions.setSize = req.swagger.params.setSize.value;
    searchOptions.sort = decodeURIComponent(req.swagger.params.sort.value);
    searchOptions.ascending = req.swagger.params.ascending.value;

	var qValue =  req.swagger.params.query.value;

	if(qValue) {
		searchOptions.query = {operator: SearchOperator.Equal, value: qValue, field : 'name'};
	}

	mappingService.search(accountId, searchOptions).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};

var postMapping = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value);

	mappingService.add(accountId, req.swagger.params.mapping.value).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};

var getMapping = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
		mappingId = decodeURIComponent(req.swagger.params.mappingId.value);

	mappingService.getById(accountId, mappingId).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};

var putMapping = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        mappingId = decodeURIComponent(req.swagger.params.mappingId.value);

	mappingService.update(accountId, mappingId, req.swagger.params.mapping.value).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};

var removeMapping = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
		mappingId = decodeURIComponent(req.swagger.params.mappingId.value);

	mappingService.remove(accountId, mappingId).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};

var convertObject = function(req, res){
	var accountId = decodeURIComponent(req.swagger.params.accountId.value),
		mappingId = decodeURIComponent(req.swagger.params.mappingId.value);

	mappingService.convertObject(accountId, req.swagger.params.source.value, mappingId).then(function (result) {
		res.status(200).json(result);
	}).catch(function (e) {
		res.status(400).json(e);
	});
};

var convertObjectWithMapping = function(req, res){
	var mappingData = req.swagger.params.mappingData.value;
	res.status(200).json(mappingService.convertObjectWithMapping(mappingData.source, mappingData.mapping));
};


module.exports = {
	getMappings: getMappings,
	postMapping: postMapping,
    getMapping: getMapping,
	putMapping: putMapping,
	removeMapping: removeMapping,
	convertObject : convertObject,
	convertObjectWithMapping : convertObjectWithMapping
};
