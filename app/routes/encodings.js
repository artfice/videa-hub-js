"use strict";

var services = require('../config/services.js');
var encodingService = services.encodingService;
//var videoEncodingService = services.videoEncodingService;
var workerService = services.workerService;

var getEncoding = function (req, res) {

	var accountId = req.swagger.params.accountId.value;

	encodingService.get(accountId).then(function (result) {
		res.status(200).json(result);
	}).catch(function (e) {
		res.status(400).json(e);
	});
};

var postEncoding = function (req, res) {

	var accountId = req.swagger.params.accountId.value;
	
	encodingService.add(accountId, req.swagger.params.encoding.value).then(function (result) {
		res.status(200).json(result);
	}).catch(function (e) {
		res.status(400).json(e);
	});
};

var startEncodingVideoJob = function(req, res){
	
	var settings = {videoEncodingData : req.swagger.params.encodingData.value};
	settings.accountId = req.swagger.params.accountId.value;
	settings.contentId = req.swagger.params.contentId.value;

	workerService.add({
		parameters: {
			settings: settings
		},
		taskName: "VideoEncoding"
	}).then(function(worker){
		res.status(200).json(worker);
	}).catch(function (e) {
		res.status(400).json(e);
	});
}

module.exports = {
	getEncoding: getEncoding,
	postEncoding: postEncoding,
	startEncodingVideoJob : startEncodingVideoJob
};
