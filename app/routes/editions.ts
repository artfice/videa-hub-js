"use strict";
import services = require('../config/services.js');
const editionService = services.editionService;
const editionStateService = services.editionStateService;

const getEdition = (req, res) => {
    const accountId = decodeURIComponent(req.swagger.params.accountId.value);
    const appId = decodeURIComponent(req.swagger.params.appId.value);
    const editionId = decodeURIComponent(req.swagger.params.editionId.value);

    editionService.getById(accountId, editionId).then((result) => {
        res.status(200).json(result);
    }).catch((e) => {
        res.status(400).json(e.message);
    });

};

const duplicateEdition = (req, res) => {
    const accountId = decodeURIComponent(req.swagger.params.accountId.value);
    const appId = decodeURIComponent(req.swagger.params.appId.value);
    const editionId = decodeURIComponent(req.swagger.params.editionId.value);

    editionService.duplicateEdition(accountId, appId, editionId).then((result) => {
        res.status(200).json(result);
    }).catch((e) => {
        res.status(400).json(e.message);
    });

};

const getAllEdition = (req, res) => {
    const accountId = decodeURIComponent(req.swagger.params.accountId.value);
    const appId = decodeURIComponent(req.swagger.params.appId.value);

    editionService.getByApp(accountId, appId).then((result) => {
        res.status(200).json(result);
    }).catch((e) => {
        res.status(400).json(e.message);
    });

};

const createEdition = (req, res) => {
    const accountId = decodeURIComponent(req.swagger.params.accountId.value);
    const appId = decodeURIComponent(req.swagger.params.appId.value);
    const edition = req.swagger.params.edition.value;

    editionService.create(accountId, appId, edition).then((result) => {
        res.status(200).json(result);
    }).catch((e) => {
        console.log(e);
        res.status(400).json(e);
    });
};

const updateEdition = (req, res) => {
    const accountId = decodeURIComponent(req.swagger.params.accountId.value);
    const appId = decodeURIComponent(req.swagger.params.appId.value);
    const editionId = decodeURIComponent(req.swagger.params.editionId.value);
    const edition = req.swagger.params.edition.value;

    editionService.update(accountId, appId, edition).then((result) => {
        res.status(200).json(result);
    }).catch((e) => {
        console.log(e);
        res.status(400).json(e);
    });
};

const deleteEdition = (req, res) => {
    const accountId = decodeURIComponent(req.swagger.params.accountId.value);
    const appId = decodeURIComponent(req.swagger.params.appId.value);
    const editionId = decodeURIComponent(req.swagger.params.editionId.value);

    editionService.remove(accountId, appId, editionId).then((result) => {
        res.status(200).json(result);
    }).catch((e) => {
        console.log(e);
        res.status(400).json(e);
    });
};

const getEditionStates = (req, res) => {
    const accountId = decodeURIComponent(req.swagger.params.accountId.value);
    const appId = decodeURIComponent(req.swagger.params.appId.value);
    res.status(200).json({
        states: editionStateService.getStates(accountId, appId)
    });
};

export = {
    createEdition: createEdition,
    updateEdition: updateEdition,
    deleteEdition: deleteEdition,
    getEdition: getEdition,
    getAllEdition: getAllEdition,
    getEditionStates: getEditionStates,
    duplicateEdition: duplicateEdition
};





