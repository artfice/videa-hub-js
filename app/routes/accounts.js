"use strict";

var accountService = require('../config/services.js').accountService;
var SearchOperator = require('videa/dto/SearchOperator.js');

var getAccounts = function (req, res) {

    var userId = req.securityResponse.isAllowed ? undefined : req.securityResponse.identity.id;
    var searchOptions = {};
    searchOptions.offset = req.swagger.params.offset.value;
    searchOptions.setSize = req.swagger.params.setSize.value;
    searchOptions.sort = decodeURIComponent(req.swagger.params.sort.value);
    searchOptions.ascending = req.swagger.params.ascending.value;

	var qValue =  req.swagger.params.query.value;

	if(qValue) {
		searchOptions.query = {operator: SearchOperator.Equal, value: qValue, field : 'name'};
	}

    accountService.search(userId, searchOptions).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};

var postAccount = function (req, res) {
    var account = req.swagger.params.account.value;

    accountService.add(account).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};

var getAccount = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value);

    accountService.getById(accountId).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};

var putAccount = function (req, res) {

    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        account = req.swagger.params.account.value;

    accountService.update(accountId, account).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};

var removeAccount = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value);

    accountService.remove(accountId).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};

module.exports = {
    getAccounts: getAccounts,
    postAccount: postAccount,
    getAccount: getAccount,
    putAccount: putAccount,
    removeAccount: removeAccount
};
