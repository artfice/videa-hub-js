var clientConfigService = require('../config/services.js').clientConfigService;
var clientEditionService = require('../config/services.js').clientEditionService;
var publishedClientConfigApplicationService = require('../config/services.js').publishedClientConfigApplicationService;

var getConfig = function (req, res) {

    var configId = decodeURIComponent(req.swagger.params.configId.value),
        accountId = decodeURIComponent(req.swagger.params.accountId.value);

    clientConfigService.getById(accountId, configId).then(
        function (clientConfig) {
            res.status(200).json(clientConfig);
        }
    ).catch(
        function (err) {
            res.status(400).json(err);
        }
    );
};

var getPublishedConfig = function (req, res) {

    var appId = decodeURIComponent(req.swagger.params.appId.value),
        accountId = decodeURIComponent(req.swagger.params.accountId.value);

    publishedClientConfigApplicationService.getPublishedClientConfig(accountId, appId).then(
        function (clientConfig) {
            res.status(200).json(clientConfig);
        }
    ).catch(
        function (err) {
            res.status(400).json(err);
        }
    );
};

var getEditions = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value);
    var brandId = req.swagger.params.brandId.value;
    var status = req.swagger.params.status.value;
    var editionId = req.swagger.params.editionId.value;
    clientEditionService.getEditions(accountId, brandId, editionId, status).then(
        function (clientEditions) {
            res.status(200).json(clientEditions);
        }
    ).catch(
        function (err) {
            res.status(400).json(err);
        }
    );
};

module.exports = {
    getConfig: getConfig,
    getEditions: getEditions,
    getPublishedConfig: getPublishedConfig
};