"use strict";

var streamifier = require('streamifier');
var configurationService = require('../config/services.js').configurationService;

var getConfigurations = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        brandId = decodeURIComponent(req.swagger.params.brandId.value),
        deviceId = decodeURIComponent(req.swagger.params.deviceId.value),
        searchOptions = {};
    searchOptions.offset = req.swagger.params.offset.value;
    searchOptions.setSize = req.swagger.params.setSize.value;
    searchOptions.sort = decodeURIComponent(req.swagger.params.sort.value);
    searchOptions.ascending = req.swagger.params.ascending.value;

    configurationService.list(accountId, brandId, deviceId, searchOptions).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};

var uploadConfiguration = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        brandId = decodeURIComponent(req.swagger.params.brandId.value),
        deviceId = decodeURIComponent(req.swagger.params.deviceId.value),
        filename = req.swagger.params.filename.value,
        config = req.swagger.params.file.value;

    console.log("CONFIG NAME: ", filename);
    if (!filename)
        filename = config.originalname;

    configurationService.upload(accountId, { name: filename, brandId: brandId, deviceId: deviceId },
        streamifier.createReadStream(config.buffer)).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};

var setDefaultConfiguration = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        brandId = decodeURIComponent(req.swagger.params.brandId.value),
        deviceId = decodeURIComponent(req.swagger.params.deviceId.value),
        configId = decodeURIComponent(req.swagger.params.configId.value);

    configurationService.setDefault(accountId, { brandId: brandId, deviceId: deviceId, id: configId})
        .then(function (result) {
            res.status(200).json(result);
        }).catch(function (e) {
            res.status(400).json(e);
        });
};

module.exports = {
    getConfigurations: getConfigurations,
    uploadConfiguration: uploadConfiguration,
    setDefaultConfiguration: setDefaultConfiguration
};
