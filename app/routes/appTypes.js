/**
 * Created by bardiakhosravi on 15-10-19.
 */
"use strict";

var appTypeService = require('../config/services.js').appTypeService;

var getAppTypes = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        searchOptions = {};
    searchOptions.offset = req.swagger.params.offset.value;
    searchOptions.setSize = req.swagger.params.setSize.value;
    searchOptions.sort = decodeURIComponent(req.swagger.params.sort.value);
    searchOptions.ascending = req.swagger.params.ascending.value;

    appTypeService.search(accountId, searchOptions).then(
        function (result) {
            res.status(200).json(result);
        }
    ).catch(
        function (e) {
            res.status(400).json(e);
        }
    );
};

module.exports = {
    getAppTypes: getAppTypes
};



