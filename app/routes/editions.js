"use strict";
var services = require('../config/services.js');
var editionService = services.editionService;
var editionStateService = services.editionStateService;
var getEdition = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value);
    var appId = decodeURIComponent(req.swagger.params.appId.value);
    var editionId = decodeURIComponent(req.swagger.params.editionId.value);
    editionService.getById(accountId, editionId).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e.message);
    });
};
var duplicateEdition = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value);
    var appId = decodeURIComponent(req.swagger.params.appId.value);
    var editionId = decodeURIComponent(req.swagger.params.editionId.value);
    editionService.duplicateEdition(accountId, appId, editionId).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e.message);
    });
};
var getAllEdition = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value);
    var appId = decodeURIComponent(req.swagger.params.appId.value);
    editionService.getByApp(accountId, appId).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e.message);
    });
};
var createEdition = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value);
    var appId = decodeURIComponent(req.swagger.params.appId.value);
    var edition = req.swagger.params.edition.value;
    editionService.create(accountId, appId, edition).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        console.log(e);
        res.status(400).json(e);
    });
};
var updateEdition = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value);
    var appId = decodeURIComponent(req.swagger.params.appId.value);
    var editionId = decodeURIComponent(req.swagger.params.editionId.value);
    var edition = req.swagger.params.edition.value;
    editionService.update(accountId, appId, edition).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        console.log(e);
        res.status(400).json(e);
    });
};
var deleteEdition = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value);
    var appId = decodeURIComponent(req.swagger.params.appId.value);
    var editionId = decodeURIComponent(req.swagger.params.editionId.value);
    editionService.remove(accountId, appId, editionId).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        console.log(e);
        res.status(400).json(e);
    });
};
var getEditionStates = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value);
    var appId = decodeURIComponent(req.swagger.params.appId.value);
    res.status(200).json({
        states: editionStateService.getStates(accountId, appId)
    });
};
module.exports = {
    createEdition: createEdition,
    updateEdition: updateEdition,
    deleteEdition: deleteEdition,
    getEdition: getEdition,
    getAllEdition: getAllEdition,
    getEditionStates: getEditionStates,
    duplicateEdition: duplicateEdition
};
