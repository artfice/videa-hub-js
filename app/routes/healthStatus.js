"use strict";

var checkHealth = function (req, res) {
    res.status(200).json({});
};

module.exports = {
    checkHealth: checkHealth
};
