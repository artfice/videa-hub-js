"use strict";

var services = require('../config/services.js');
var defaultDataService = services.defaultDataService;
var sampleDataService = services.sampleDataService;

var postDefaultData = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value);

    defaultDataService.loadDefaultData(accountId).then(function () {
        res.status(200).json({});
    }).catch(function (e) {
        res.status(400).json(e);
    });
};


var postSampleData = function (req, res) {
	var accountId = decodeURIComponent(req.swagger.params.accountId.value);

	sampleDataService.loadSampleData(accountId).then(function () {
		res.status(200).json({});
	}).catch(function (e) {
		res.status(400).json(e);
	});
};

module.exports = {
    postDefaultData: postDefaultData,
	postSampleData : postSampleData
};
