"use strict";

var services = require('../config/services.js');
var signUpService = services.signUpService;
var SearchOperator = require('videa/dto/SearchOperator.js');
//var jwt = require('jwt-simple');

var getSignUpRequests = function (req, res) {
	var searchOptions = {};
	searchOptions.offset = req.swagger.params.offset.value;
	searchOptions.setSize = req.swagger.params.setSize.value;
	searchOptions.sort = req.swagger.params.sort.value;
	searchOptions.ascending = req.swagger.params.ascending.value;

	var qValue =  req.swagger.params.query.value;

	if(qValue) {
		searchOptions.query = {operator: SearchOperator.Or , value: [
			{operator: SearchOperator.Equal, value: qValue, field : 'user.username'},
			{operator: SearchOperator.Equal, value: qValue, field : 'user.firstName'},
			{operator: SearchOperator.Equal, value: qValue, field : 'user.lastName'},
			{operator: SearchOperator.Equal, value: qValue, field : 'user.email'},
			{operator: SearchOperator.Equal, value: qValue, field : 'account.name'}
		]
		};
	}

	signUpService.search(searchOptions).then(function (result) {
		res.status(200).json(result);
	}).catch(function (e) {
		res.status(400).json(e);
	});
};

var signUp = function(req, res) {

	var signUp = req.swagger.params.signUp.value;

	signUpService.add(signUp).then(
		function (result) {
			res.status(200).json(result);
		},
		function (err) {
			res.status(400).json(err);
		}
	);
};

var putSignUp = function(req, res) {

	var signUpRequestId = decodeURIComponent(req.swagger.params.signUpRequestId.value),
		userId = req.securityResponse.identity.id,
		signUp = req.swagger.params.signUp.value;

	signUpService.update(signUpRequestId, signUp, userId).then(
		function (result) {
			res.status(200).json(result);
		},
		function (err) {
			res.status(400).json(err);
		}
	);
};

module.exports = {
	getSignUpRequests : getSignUpRequests,
	signUp: signUp,
	putSignUp : putSignUp
};
