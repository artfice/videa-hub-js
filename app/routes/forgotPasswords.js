"use strict";

var services = require('../config/services.js');
var forgotPasswordService = services.forgotPasswordService;

var requestChange = function (req, res) {

    var email = req.swagger.params.email.value;

    forgotPasswordService.requestChangePassword(email).then(function(result){
        res.status(200).json(result);
    }).catch(function(err) {
        res.status(400).json(err);
    });
};

var changePassword = function(req, res) {

    var token = req.swagger.params.token.value;
    var request = req.swagger.params.newPassword.value;

    forgotPasswordService.changeUserPassword(token, request.newPassword).then(function(result){
        res.status(200).json(result);
    }).catch(function(err) {
        res.status(400).json(err);
    });
};


var verifyToken = function(req, res) {

    var token = req.swagger.params.token.value;

    forgotPasswordService.verifyToken(token).then(function(result){
        res.status(200).json(result);
    }).catch(function(err) {
        res.status(400).json(err);
    });
};

module.exports = {
    requestChange : requestChange,
    changePassword: changePassword,
    verifyToken : verifyToken
};
