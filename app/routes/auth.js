"use strict";

var services = require('../config/services.js');
var authenticationService = services.authenticationService;
var userService = services.userService;
var nconf = require('nconf');
var jwt = require('jwt-simple');


var signIn = function(req, res) {
    var signIn = req.swagger.params.signIn.value;
    authenticationService.authenticate(signIn).then(
        function (result) {
            res.cookie(nconf.get('webapi:cookieName'), result.token, { maxAge: nconf.get('auth:tokenDuration') });
            res.status(200).json(result);
        },
        function (err) {
            res.clearCookie(nconf.get('webapi:cookieName'));
            res.status(400).json(err);
        }
    );

};

var signOut = function(req, res){
    res.clearCookie(nconf.get('webapi:cookieName'));
    res.status(200).json({});
}

var getIdentity = function(req, res){

    var token = req.cookies[nconf.get('webapi:cookieName')];
    authenticationService.verifyToken(token).then(
        function(result) {
            res.status(200).json(result);
        },
        function(err) {
            res.status(400).json(err);
        }
    );
};

module.exports = {
    signIn: signIn,
    getIdentity: getIdentity,
    signOut : signOut
};
