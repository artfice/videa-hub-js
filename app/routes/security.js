"use strict";

var services = require('../config/services.js');
var securityService = services.securityService;

var getPermissions = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        userId = req.securityResponse.identity.id;

    securityService.getPermissions(accountId, userId).then(function (result) {

        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};

module.exports = {
    getPermissions: getPermissions
};
