"use strict";

var services = require('../config/services.js');
var schemaService = services.schemaService;
var fileManagerService = services.fileManagerService;

var getSchema = function (req, res) {
    var schemaId = decodeURIComponent(req.swagger.params.schemaId.value);

    schemaService.getSchema(schemaId).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        console.log(e);
        res.status(400).json(e);
    });
};

var getSchemaFile = function(req, res){
	var assetKey = req.swagger.params.assetKey.value;
	var settings = req.swagger.params.delimiter.value;

	schemaService.getSchemaCsvFile(fileManagerService.getFile(assetKey), settings).then(function (result) {
		res.status(200).json(result);
	}).catch(function (e) {
		res.status(400).json(e.message);
	});
};

var getSchemaFromJSONs = function(req, res){
	var data = req.swagger.params.data.value;
	
	schemaService.getSchemaFromJSONs(data).then(function(results) {
		res.status(200).json(results);
	}).catch(function(e){
		res.status(400).json(e.message);
	});
};

module.exports = {
    getSchema: getSchema,
	getSchemaFile :getSchemaFile,
	getSchemaFromJSON : getSchemaFromJSONs
};

