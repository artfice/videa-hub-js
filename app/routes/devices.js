"use strict";

var Device = require('videa/dto/Device');
var deviceService = require('../config/services.js').deviceService;

var getDevices = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        brandId = decodeURIComponent(req.swagger.params.brandId.value),
        searchOptions = {};
    searchOptions.offset = req.swagger.params.offset.value;
    searchOptions.setSize = req.swagger.params.setSize.value;
    searchOptions.sort = decodeURIComponent(req.swagger.params.sort.value);
    searchOptions.ascending = req.swagger.params.ascending.value;

    deviceService.list(accountId, brandId, searchOptions).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};

var postDevice = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        brandId = decodeURIComponent(req.swagger.params.brandId.value),
        device = req.swagger.params.device.value;

    if (device) {
        device.brandId = brandId;
    }

    deviceService.add(accountId, device).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};

var getDevice = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        brandId = decodeURIComponent(req.swagger.params.brandId.value),
        deviceId = decodeURIComponent(req.swagger.params.deviceId.value);

    deviceService.getById(accountId, { brandId: brandId, deviceId: deviceId}).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};

var putDevice = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        brandId = decodeURIComponent(req.swagger.params.brandId.value),
        deviceId = decodeURIComponent(req.swagger.params.deviceId.value),
        device = req.swagger.params.device.value;

    deviceService.update(accountId, { brandId: brandId, deviceId: deviceId}, device).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};

var removeDevice = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        brandId = decodeURIComponent(req.swagger.params.brandId.value),
        deviceId = decodeURIComponent(req.swagger.params.deviceId.value);

    deviceService.remove(accountId, { brandId: brandId, deviceId: deviceId}).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};

module.exports = {
    getDevices: getDevices,
    postDevice: postDevice,
    getDevice: getDevice,
    putDevice: putDevice,
    removeDevice: removeDevice
};
