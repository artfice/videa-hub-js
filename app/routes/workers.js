"use strict";

var services = require('../config/services.js');
var workerService = services.workerService;

var getWorkers = function (req, res) {
	var searchOptions = {};
	searchOptions.offset = req.swagger.params.offset.value;
	searchOptions.setSize = req.swagger.params.setSize.value;
	searchOptions.sort = req.swagger.params.sort.value;
	searchOptions.ascending = req.swagger.params.ascending.value;

	workerService.search(searchOptions).then(function (result) {
		res.status(200).json(result);
	}).catch(function (e) {
		res.status(400).json(e);
	});
};

var postWorker = function(req, res) {
	var worker = req.swagger.params.worker.value;
	
	workerService.add(worker).then(
		function (result) {
			res.status(200).json(result);
		},
		function (err) {
			res.status(400).json(err);
		}
	);
};

var getWorker = function (req, res) {
	var workerId = req.swagger.params.workerId.value;

	workerService.getById(workerId).then(function (result) {
		res.status(200).json(result);
	}).catch(function (e) {
		res.status(400).json(e);
	});
};


module.exports = {
	getWorkers : getWorkers,
	postWorker: postWorker,
	getWorker : getWorker
};
