"use strict";

var streamifier = require('streamifier');
var fileManagerService = require('../config/services.js').fileManagerService;

var uploadFile = function (req, res) {
	var file = req.swagger.params.file.value;
	// TODO: Code review. What if file is not defined?
//	console.log("FILE", file);
	fileManagerService.uploadFile(file.originalname, streamifier.createReadStream(file.buffer)).then(function (result) {
		res.status(200).json(result);
	}).catch(function (e) {
		res.status(400).json(e);
	});
};


module.exports = {
	uploadFile: uploadFile
};
