"use strict";

var workflowService = require('../config/services.js').workflowService;

var getAllWorkflowStates = function (req, res) {
    workflowService.getAllWorkflowStates().then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};

var getAllowedTriggers = function (req, res) {
    workflowService.getAllowedTriggers(req.swagger.params.state.value).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};

module.exports = {
    getAllWorkflowStates: getAllWorkflowStates,
    getAllowedTriggers: getAllowedTriggers
};
