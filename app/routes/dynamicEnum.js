"use strict";
/**
 * Created by ryanwong on 2015-10-21.
 */

var configService = require('../config/services.js').configService;
var screenService = require('../config/services.js').screenServiceIntegration;

var getAnalytics = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        configId = decodeURIComponent(req.swagger.params.configId.value);

    configService.getDynamicEnumAnalytics(accountId, configId).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};

var getScreens = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        configId = decodeURIComponent(req.swagger.params.configId.value),
        type = decodeURIComponent(req.swagger.params.type.value);

    screenService.getDynamicEnumScreen(accountId, configId, type).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};

module.exports = {
    getAnalytics: getAnalytics,
    getScreens: getScreens
};
