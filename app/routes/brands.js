"use strict";

var Brand = require('videa/dto/appstudio/brand/IBrand');
var brandService = require('../config/services.js').brandService;

var getBrands = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        searchOptions = {};
    searchOptions.offset = req.swagger.params.offset.value;
    searchOptions.setSize = req.swagger.params.setSize.value;
    searchOptions.sort = decodeURIComponent(req.swagger.params.sort.value);
    searchOptions.ascending = req.swagger.params.ascending.value;
    brandService.search(accountId, searchOptions).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};

var postBrand = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        brandField     = req.swagger.params.brand.value,
        imageFile = null;

    if (req.swagger.params.image.value) {
        imageFile = req.swagger.params.image.value;
    }

    brandService.create(accountId, brandField, imageFile).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};

var getBrand = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        brandId = decodeURIComponent(req.swagger.params.brandId.value);

    brandService.getBrandById(accountId, brandId).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};

var putBrand = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        brandId = decodeURIComponent(req.swagger.params.brandId.value),
        brandField = req.swagger.params.brand.value,
        imageFile = null;

    if (req.swagger.params.image.value) {
        imageFile = req.swagger.params.image.value;
    }
    brandService.updateBrand(accountId, brandId, brandField, imageFile).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};

var removeBrand = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        brandId = decodeURIComponent(req.swagger.params.brandId.value);

    brandService.removeBrand(accountId, brandId).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};

var getGallery = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        brandId = decodeURIComponent(req.swagger.params.brandId.value);

    brandService.getGallery(accountId, brandId).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};


var addGalleryImage = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        brandId   = decodeURIComponent(req.swagger.params.brandId.value),
        imageName = req.swagger.params.name.value,
        imageFile = req.swagger.params.image.value;

    brandService.addGalleryImage(accountId, brandId, "f599e673-ff65-4b48-bfcb-35a1b74100f2", imageName, imageFile).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        console.log(e);
        res.status(400).json(e);
    });
};

var removeGalleryImage = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        brandId   = decodeURIComponent(req.swagger.params.brandId.value),
        galleryImageId = decodeURIComponent(req.swagger.params.galleryImageId.value);

    brandService.removeGalleryImage(accountId, brandId, galleryImageId).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        console.log(e);
        res.status(400).json(e);
    });
};

var replaceGalleryImage = function (req, res) {
    var accountId = decodeURIComponent(req.swagger.params.accountId.value),
        brandId   = decodeURIComponent(req.swagger.params.brandId.value),
        galleryImageId = decodeURIComponent(req.swagger.params.galleryImageId.value),
        imageName = req.swagger.params.name.value,
        imageFile = req.swagger.params.image.value;

    brandService.replaceGalleryImage(accountId, brandId, galleryImageId, imageName, imageFile).then(function (result) {
        res.status(200).json(result);
    }).catch(function (e) {
        console.log(e);
        res.status(400).json(e);
    });
};

module.exports = {
    getBrands: getBrands,
    postBrand: postBrand,
    getBrand: getBrand,
    putBrand: putBrand,
    removeBrand: removeBrand,
    getGallery: getGallery,
    addGalleryImage: addGalleryImage,
    replaceGalleryImage: replaceGalleryImage,
    removeGalleryImage: removeGalleryImage
};
