"use strict";
const App = require('videa/dto/appstudio/IEdition');
const appService = require('../config/services.js').appService;

var _ = require('lodash');

const getApp = function(req, res) {
    const accountId = decodeURIComponent(req.swagger.params.accountId.value);
    const brandId = decodeURIComponent(req.swagger.params.brandId.value);
    const appId = decodeURIComponent(req.swagger.params.appId.value);

    appService.getById(accountId, appId).then(function(result) {
        res.status(200).json(result);
    }).catch(function(e) {
        console.log(e);
        res.status(400).json(e);
    });

};

const getAppByBrand = function(req, res) {
    const accountId = decodeURIComponent(req.swagger.params.accountId.value);
    const brandId = decodeURIComponent(req.swagger.params.brandId.value);

    appService.getByBrand(accountId, brandId).then(function(result) {
        res.status(200).json(result);
    }).catch(function(e) {
        console.log(e);
        res.status(400).json(e);
    });

};

const createApp = function(req, res) {
    const accountId = decodeURIComponent(req.swagger.params.accountId.value);
    const brandId = decodeURIComponent(req.swagger.params.brandId.value);
    const app = req.swagger.params.app.value;

    appService.create(accountId, brandId, app).then(function(result) {
        res.status(200).json(result);
    }).catch(function(e) {
        console.log(e);
        res.status(400).json(e);
    });
};

const updateApp = function(req, res) {
    const accountId = decodeURIComponent(req.swagger.params.accountId.value);
    const brandId = decodeURIComponent(req.swagger.params.brandId.value);
    const appId = decodeURIComponent(req.swagger.params.appId.value);
    const app = req.swagger.params.app.value;

    appService.update(accountId, brandId, appId, app).then(function(result) {
        res.status(200).json(result);
    }).catch(function(e) {
        console.log(e);
        res.status(400).json(e);
    });
};

const deleteApp = function(req, res) {
    const accountId = decodeURIComponent(req.swagger.params.accountId.value);
    const brandId = decodeURIComponent(req.swagger.params.brandId.value);
    const appId = decodeURIComponent(req.swagger.params.appId.value);

    appService.remove(accountId, brandId, appId).then(function(result) {
        res.status(200).json(result);
    }).catch(function(e) {
        console.log(e);
        res.status(400).json(e);
    });
};

const duplicateApp = function(req, res) {
    const fromAccountId = decodeURIComponent(req.swagger.params.fromAccountId.value);
    const fromBrandId = decodeURIComponent(req.swagger.params.fromBrandId.value);
    const appDuplicateTo = req.swagger.params.appDuplicateTo.value;
    const appId = decodeURIComponent(req.swagger.params.appId.value);

    appService.duplicateApp(fromAccountId, fromBrandId, _.get(appDuplicateTo, 'toAccountId'), _.get(appDuplicateTo, 'toBrandId'), appId).then((result) => {
        res.status(200).json(result);
}).catch((e) => {
        console.error(e);
    res.status(400).json(e);
})
};

module.exports = {
    createApp: createApp,
    updateApp: updateApp,
    deleteApp: deleteApp,
    duplicateApp: duplicateApp,
    getApp: getApp,
    getAppByBrand: getAppByBrand
};
