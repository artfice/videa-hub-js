/**
 * Created by bardiakhosravi on 2015-11-29.
 */
"use strict";

var screenServiceIntegration = require('../config/services.js').screenServiceIntegration;

function print(j) {
    console.log(JSON.stringify(j, null, 4));
}

var createScreen = function (req, res) {

    screenServiceIntegration.createScreen(
        req.swagger.params.accountId.value,
        req.swagger.params.configId.value,
        req.swagger.params.screen.value
    ).then(
        function (screenDto) {
            res.status(200).json(screenDto);
        },
        function (e) {
            res.status(400).json(e);
        }
    );

};

var getScreens = function (req, res) {
    screenServiceIntegration.getScreens(
        decodeURIComponent(req.swagger.params.accountId.value),
        decodeURIComponent(req.swagger.params.configId.value)
    ).then(
        function (screens) {
            res.status(200).json({
                data: screens
            });
        },
        function (e) {
            console.log('error ----');
            console.log(e);
            res.status(400).json(e);
        }
    );

};

var updateScreen = function (req, res) {

    screenServiceIntegration.updateScreen(
        decodeURIComponent(req.swagger.params.accountId.value),
        decodeURIComponent(req.swagger.params.configId.value),
        req.swagger.params.screen.value
    ).then(
        function (screenDto) {
            res.status(200).json(screenDto);
        },
        function (e) {
            console.log('error ----');
            console.log(e);
            res.status(400).json(e);
        }
    );

};

var deleteScreen = function (req, res) {
    screenServiceIntegration.deleteScreen(
        decodeURIComponent(req.swagger.params.accountId.value),
        decodeURIComponent(req.swagger.params.configId.value),
        decodeURIComponent(req.swagger.params.screenId.value)
    ).then(
        function (screenDto) {
            res.status(200).json(screenDto);
        },
        function (e) {
            console.log('error ----');
            console.log(e);
            res.status(400).json(e);
        }
    );
};

module.exports = {
    createScreen: createScreen,
    getScreens: getScreens,
    updateScreen: updateScreen,
    deleteScreen: deleteScreen
};