"use strict";
var services = require('../config/services.js');
var aclService = services.aclService;

var getUserRoles = function (req, res) {
	var roles = aclService.getAvailableRoles("user");
	
	res.status(200).json(roles);
};

var getAccountRoles = function (req, res) {
	var roles = aclService.getAvailableRoles("account");

	res.status(200).json(roles);
};


module.exports = {
	getAccountRoles: getAccountRoles,
	getUserRoles: getUserRoles
};
