"use strict";
var services = require('../config/services.js');

var SpecService = require('videa/services/common/SpecService');
var specService = services.specService;

var getJson = function (req, res) {

	specService.getUserApiJSon(req.securityResponse.identity.id).then(function (result) {;
        res.status(200).json(result);
    }).catch(function (e) {
        res.status(400).json(e);
    });
};

module.exports = {
	getJson: getJson
};
