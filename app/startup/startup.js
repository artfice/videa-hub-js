/**
 * The purpose of this code is to startup videa including applying migrations or setup default data
 */

var Promise = require('bluebird');
var nconf = require('../config/index.js');
var Logger = require('../node_modules/videa/core/logger/Logger');
var versionService = require('../config/version.js');
var evolutions = require('../config/evolutions.js');
var services = require('../config/services.js');
var videaErrors = require('../node_modules/videa/dto/VideaError');
var InvalidArgumentError = videaErrors.InvalidArgumentError;

var pjson = require('../../package.json');
var bjson = require('../../build.json');

module.exports = new Promise(function (resolve, reject) {
	
	// override console log to use our Logger
	console.log = function() {
		Logger.info(arguments[0], arguments[1]);
	};
	console.info = function(){
		Logger.info(arguments[0], arguments[1]);
	};
	console.warn = function(){
		Logger.warn(arguments[0], arguments[1]);
	};
	console.error = function(){
		Logger.error(arguments[0], arguments[1]);
	};
	console.debug = function(){
		Logger.debug(arguments[0], arguments[1]);
	};

    var currentVersion,
        newVersion = versionService.parseVersion(pjson.version),
        error;
    
    newVersion.build = bjson.build;
    
    return versionService.get().then(function(version) {
        console.log("VIDEA VERSION", version);
        currentVersion = version;
    }).catch(function(err) {
        if (err && err.name === 'ItemNotFoundError') {
            currentVersion = undefined;
        } else {
            error = err;
        }
    }).finally(function (){

        if (error) {
            console.error("Error loading Videa version from DB", error);
			return reject(error);
		}
		
		var skipEvolutions = nconf.get('evolutions:skipEvolutions');
        if (skipEvolutions) {
            console.log('SKIPPING EVOLUTIONS PROCESS.')
            require('./startWebAPI.js');
            return resolve(undefined);
        }

        if (!currentVersion) {
            currentVersion = {
                label: "0.0.0",
                major:0,
                minor:0,
                patch:0,
                build: 0
            };
        } else if (currentVersion.label && !currentVersion.major) {
            var v = versionService.parseVersion(currentVersion.label);
            if (!v) {
                return reject(new InvalidArgumentError("Error parsing current installed version label " + currentVersion.label));
            }
            v.build = currentVersion.build;
            currentVersion = v;
        }

        if (versionService.compareVersion(currentVersion, newVersion) < 0) {
            console.log("MIGRATING VIDEA TO " + newVersion.label + " BUILD " + newVersion.build);
            return evolutions.applyEvolutions(currentVersion, newVersion).then(function() {
                return versionService.set(newVersion).then(function(result){
                    require('./startWebAPI.js');
                    return resolve(undefined);
                }).catch(function(err){
                    console.log("ERROR SAVING NEW VIDEA VERSION", err);
                    return reject(err);
                });
            }).catch(function (err) {
                console.log("ERROR MIGRATING VIDEA VERSION", err);
                return reject(err);
            });
        }
        else {
            require('./startWebAPI.js');
            return resolve(undefined);
        }
    });
});
