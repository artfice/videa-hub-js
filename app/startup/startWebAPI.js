"use strict";

var services = require('../config/services.js');
var modules = require('../framework/Modules.js');
var path = require("path");
var fs = require("fs");
var securityService = services.securityService;
var specService = services.specService;

var nconf = require('nconf');
var express = require('express');
var cookieParser = require('cookie-parser');
//var passport = require('passport');

var pmx = require('pmx');
var probe = pmx.probe();
// The counter will start at 0
var conCounter = probe.counter({
    name : 'Connections count'
});
var reqCounter = probe.meter({
    name      : 'Req/sec',
    samples   : 1,
    timeframe : 10
});
var requestDuration = probe.histogram({
    name        : 'Request duration',
    measurement : 'mean'
});

var app = express();

// setup request body limit
var bodyParser = require('body-parser');
var REQUEST_BODY_LIMIT = '10mb';
app.use(bodyParser.json({limit: REQUEST_BODY_LIMIT}));
app.use(bodyParser.urlencoded({limit: REQUEST_BODY_LIMIT, extended: true}));

app.use(function(req, res, next){

    var url = req.url;
    var hrstart = process.hrtime();
    reqCounter.mark();

    if (res._responseTime) return next();
    res._responseTime = true;

    res.on('finish', function(){
        var hrend = process.hrtime(hrstart);
        requestDuration.update(hrend[1]/1000000);
    });

    // var start = new Date;
    //
    // if (res._responseTime) return next();
    // res._responseTime = true;
    //
    // res.on('header', function(){
    //     var duration = new Date - start;
    //     res.setHeader('X-Response-Time', duration + 'ms');
    // });

    next();
});

var port = normalizePort(process.env.PORT || '3000');
app.set('port', port);

app.use(function(req, res, next) {

	var whitelist = ['http://videa-dev-frontend.us-east-1.elasticbeanstalk.com',
					 'https://videa-dev-frontend.us-east-1.elasticbeanstalk.com',
					 'http://videa-dev.digiflare.com',
					 'https://videa-dev.digiflare.com',
					 'http://videa-deva.digiflare.com',
					 'https://videa-deva.digiflare.com',
					 'http://videa-devb.digiflare.com',
					 'https://videa-devb.digiflare.com',
					 'http://videa-demo.digiflare.com',
					 'https://videa-demo.digiflare.com',
					 'http://videa-stage.digiflare.com',
					 'https://videa-stage.digiflare.com',
					 'http://videa-staging.digiflare.com',
					 'https://videa-staging.digiflare.com',
					 'http://videa.digiflare.com',
					 'https://videa.digiflare.com',
					 'http://localhost:4000',
					 'https://localhost:4000',
                     'http://10.10.10.5:4000',
					 'http://videa-preview.s3.amazonaws.com',
					 'http://videa-preview.digiflare.com',
					 'http://videa-preprod.digiflare.com',
					 'https://videa-preprod.digiflare.com',
					 'http://192.168.99.100:4000'];

	var whitelistUrlIndex = whitelist.indexOf(req.headers.origin),
		whitelistUrl = '';

	if (whitelistUrlIndex >= 0) {
		whitelistUrl = whitelist[whitelistUrlIndex];
	}

	res.header('Access-Control-Allow-Origin', whitelistUrl);
	res.header('Access-Control-Allow-Headers', 'accept, content-type, cookie');
	res.header('Access-Control-Max-Age', '3628800');
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	res.header('Access-Control-Allow-Credentials', 'true');
    res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
    res.header('Pragma', 'no-cache');
    res.header('Expires', '0');

	if (req.method == 'OPTIONS') {
		res.status(200).end();
	} else {
		next();
	}

});

var debug = require('debug')('untitled:server');
var http = require('http');

var swaggerTools = require('swagger-tools');

// build an array with all the existing controllers folders
var swaggerControllers = ['./app/routes'];
console.log("Loading controllers from " + swaggerControllers[0]);
for (var m=0; m<modules.length;m++) {
    var module = modules[m];
    var folder = path.join('./app/node_modules/videa/', module.id + '/routes');
    try {
        fs.accessSync(folder, fs.F_OK);
        swaggerControllers.push(folder);
        console.log("Loading controllers from " + folder);
    } catch (e) {
        // It isn't accessible
    }
}

// swaggerRouter configuration
var options = {
	controllers: swaggerControllers,
	useStubs: false //process.env.NODE_ENV === 'development' ? true : false // Conditionally turn on stubs (mock mode)
	//useStubs: true
};

// The Swagger document (require it, build it programmatically, fetch it from a URL, ...)
var swaggerDoc = specService.getAPIJson();

// Initialize the Swagger middleware
swaggerTools.initializeMiddleware(swaggerDoc, function(middleware) {
	var metadata = middleware.swaggerMetadata();

	// Interpret Swagger resources and attach metadata to request - must be first in swagger-tools middleware chain
	app.use(metadata);

	// Validate Swagger requests
	app.use(middleware.swaggerValidator({
		validateResponse: false
	}));

	app.use(cookieParser());

	// Provide the security handlers
	app.use(middleware.swaggerSecurity({

		videa_auth: function(req, def, headerData, callback) {

			var apiKey = req.headers[nconf.get('webapi:apiKeyName')];

			// we decided to read the cookie directly from the request because the swagger spec gives the entire
            // un-parsed cookie header
			var token = req.cookies[nconf.get('webapi:cookieName')];
			var resource = req.swagger.operation["x-videa-resource"];
			var permissions = req.swagger.operation["x-videa-permissions"];
			var accountId = (req.swagger.params.accountId ? decodeURIComponent(req.swagger.params.accountId.value) : undefined);
			securityService.verifyAccess(token, apiKey, accountId, resource, permissions).then(
				function(obj) {

					req.securityResponse = obj;

					callback();
					return null;
				},

				function(err) {
					err.statusCode = 401;
					callback(err);
					return null;
				}
			);
		}
	}));
	// Route validated requests to appropriate controller
	app.use(middleware.swaggerRouter(options));

	// Serve the Swagger documents and Swagger UI
	var swaggerUiOptions = {
		"apiDocs": "/api/v1/spec",
        "swaggerUi": "/"
	};
	app.use(middleware.swaggerUi(swaggerUiOptions));

	app.use(function(err, req, res, next) {

		// skip errors of un-authenticated users
		if (err.statusCode != 401)
			console.error("ERROR", err);

		var statusCode = err.statusCode || 500;
		res.status(statusCode).json(err);
	});

	/**
	 * Event listener for HTTP server "error" event.
	 */

	function onError(error) {
	    console.error("ERROR starting the server", error);
		if (error.syscall !== 'listen') {
			throw error;
		}

		var bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;

		// handle specific listen errors with friendly messages
		switch (error.code) {
			case 'EACCES':
				console.error(bind + ' requires elevated privileges');
				process.exit(1);
				break;
			case 'EADDRINUSE':
				console.error(bind + ' is already in use');
				process.exit(1);
				break;
			default:
				throw error;
		}
	}

	/**
	 * Event listener for HTTP server "listening" event.
	 */

	function onListening() {
		var addr = server.address();
		var bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
		console.log('Listening on ' + bind);
	}

	function onConnection (socket) {
        conCounter.inc();
        socket.on('close', function(){
            conCounter.dec();
        });
    }


    // Start the server
    console.log("Starting the server");
    var server = http.createServer(app);

    /**
     * Listen on provided port, on all network interfaces.
     */

    server.listen(port);
    server.on('error', onError);
    server.on('listening', onListening);
    server.on('connection', onConnection);

});

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
	var port = parseInt(val, 10);

	if (isNaN(port)) {
		// named pipe
		return val;
	}

	if (port >= 0) {
		// port number
		return port;
	}

	return false;
}
