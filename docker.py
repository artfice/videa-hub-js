#!/usr/bin/python
import json
from subprocess import call

#Extracting the Build and Version values from package.json
with open('package.json') as package:    
    data = json.load(package)

build = data["build"]
version = data["version"]

#Casting Unicode values as strings
build = str(build)
version = str(version)

#Opening Dockerrun.aws.json file
with open('Dockerrun.aws.json', 'r') as dockerrun:    
    dockerData = json.load(dockerrun)

#Extracting Url and casting as a string
tmp = dockerData['Image']['Name']
url = str(tmp) 

def setUrl(url, build, version):
	"Appends url + version + build"
	newUrl = url + version + "." + build
	return newUrl;
	
url = setUrl(url, build, version)

#Reassign json value, write to Dockerrun.aws.json with updated URL
dockerData['Image']['Name'] = url
with open('Dockerrun.aws.json', 'w') as jsonFile:
    jsonFile.write(json.dumps(dockerData, sort_keys = True, indent = 4))

#Build, tag and push Docker Image
call(["docker", "build", "-t", "videa-platform", "."])
call(["docker", "tag", "videa-platform", url])
call(["docker", "push", url])

