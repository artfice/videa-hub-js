***
#### Copyright
Videa Hub Digiflare © 2017

***

### Prerequisites for running videa

Follow the instructions on README.md on https://github.com/Digiflare/videa-dev-provisioning

### Running Videa-hub backend

- Checkout the videa hub repository from https://github.com/Digiflare/videa-hub-js
````
git clone https://github.com/Digiflare/videa-hub-js.git
````
- Open the terminal and navigate to the videa-hub-js directory and run
````
sudo npm install
````
- Navigate to <videa-hub-js project>/app folder and install typescript
````
tsd install -s
````
- Create development config.

  Inside app/config create a file called config.development.json which is a copy of config.json. Make sure the ports for mongodb and elasticsearch match your settings
- Compile typescript.
````
gulp typescript:all
````
- Run all tests
````
gulp test:all
````

Check https://drive.google.com/open?id=12EOrc60tMId8qbwDa1cOhG3A3RiflhIygB83sShL9w0&authuser=0 for further instructions.

### TSlint

- We use tslint to lint our code base and maintain our code integrity. Please refer to the link : https://github.com/Microsoft/tslint-microsoft-contrib to view further materials on linting rules.

- tslint.json on the parent folder contains all the rules configuration for the project. It is based on : https://github.com/Microsoft/tslint-microsoft-contrib/blob/master/recommended_ruleset.js

- If you use an IDE or test editor that supports in-editor linting, you need to point to the tslint-microsoft-contrib for the custom rules provided by microsoft. The path might look like `/Users/path_to_videa/videa-hub-js/node_modules/tslint-microsoft-contrib`

- To lint all files:
````
gulp tslint
````

-----------------------------
Setup WebStorm IDE Version 10
-----------------------------
- Setup a new project on Webstorm that points to the videa folder

- Setup Typescript compiler
- Open preferences panel (File -> Settings on Windows or WebStorm -> Preferences on OSX)
- Go to "Languages & Frameworks" -> "Typescript"
- Be sure that "Enable Typescript Compiler" is checked
- Enable "Track Changes"
- Set the "Scope" to "Project Files"
- Change the "Command Line Options" to use the CommonJS system:
    --module commonjs --target es5

Add a new Run/Debug configuration
- Open "Run" -> "Edit Configurations..."
- Add a new "Node.js" configuration
- Be sure that the working directory is set to the videa project base folder
- "Javascript File" should be "bin/www"
- Add the following environment variables:
````
DEBUG=SOURCE:*;NODE_ENV=development
````
- Save and then hit run to start nodejs from Webstorm

---------------------------------
### Building and Running videa docker(Optional)
---------------------------------

To run and test videa on an environment similar to the production environment anyone can use the current docker setup that is the same used to deploy videa on the production environment.

This setup assumes that you will have a mongodb and elasticsearch running on your native OS.
It also assumes that you have Docker installing and running on your system.
Visit https://www.docker.com/ to see more details about setup the docker on your local machine.

1) On OSX run the "docker Quick Terminal.app"

2) Docker runs inside a VM on Virtualbox. We need to know the IP to connect to mongodb and elasticsearch from inside the VM.
2.a) Open VirtualBox and locate the docker VM that is running
2.b) Open the console and run "netstat -rn"
2.c) Copy the ip for the Gateway (Docker will run with the flag --net=host which means that will share the network with the host, that's the VM in this case, this will be the IP that Docker can use to connect to the native host)

3) Configure mongodb to accept connections from everywhere
3.1) Update mongod.conf with bindIp: 0.0.0.0

4) Update app/config/config.localdocker.json to override the mongodb and elasticsearch connection string with the IP you got from step 2)

5) Build the docker image
5.1) gulp docker:build

6) Run the docker container with NODE_END=localdocker
6.1) gulp docker:run

7) The Dockerfile is prepared to cache the npm and bower packages and avoid downloading them everytime you rebuild the docker image.
If you find problems caused by missing packages you may build the image ignoring cache running: gulp docker:build-nocache


-----------------------------------------
Deploy a package on AWS Elastic Beanstalk
-----------------------------------------
1) Get the latest update from git repository

2) Run the grunt task that creates the zip ready to be deployed on Beanstalk. The zip with the current timestamp will be created inside the folder "dist".
    grunt publish

3) Upload the package to AWS EB. The next instructions assume that you will upload using the AWS Web interface.

3.a) Select the Videa app

3.b) If the package requires new NPM packages to be installed it's better to create a new environment. If that's the case proceed to step 4)

3.c) Select the desired environment (e.g.: Staging or Production)

3.d) On the Environment Dashboard click on "Upload and Deploy" and select the package created by the grunt task.

3.e) Wait for the upload to complete and for AWS deploy process to complete. Check the Logs section for more information about the deployment.

3.e.1) The deploy process will run any pending migrations which may take while depending on the size of the content stored on MongoDB or ElasticSearch.

4) These are instructions to deploy Videa on a new environment. For quick updates, please go back to step 3)

4.a) Select the Videa app

4.b) Click on the link "Create New Environment"

4.c) Click on "Create web server"

4.d) Select "videahub-stating-docker1" from the "Saved configuration" dropdown box and click "Next".

4.e) Select one of the existing application version or upload a new one. Click "Next".

4.f) Name your environment and check if the URL is available. Click "Next".

4.g) Make sure that "Create this environment inside a VPC" is checked. Click "Next".

4.h) You may want to select a more powerful instance type from the "Instance Type" dropdown. The "m1.small" is a good start. Click "Next"

4.i) Setup any Environment Tags if you want, or just leave them empty. Click "Next".

4.j) Make sure that the slected "VPC security group" is the "default--XX-YYYYYYYY". Click "Next".

4.k) The permission should be already ok and you may skip this step. Click "Next".

4.l) Review your data and click "Launch".

-------------------------
Design Decisions & Issues
-------------------------
Onion architecture

--------------------------------
How to create a new Account
--------------------------------
Sign in using,
username: admin
password: test

1. go to “Admin” -> “Accounts”
2. click on “Create Account”
3. insert the account name and click “Create”
4. select the new account from the account dropdown located on the header (you may need to refresh the page first)
5. go to “Accounts” -> “Users”
6. click on “New User” to create a new user and associate him to the current account or click on “Add Existing User” to select one of the existing users.

Additional Steps (HACK):

1. go to localhost:3000/docs (aka swaggerUI)
2. under "Default Data", add your account username
3. Click "Try it out!" button (may take a few minutes "patience young Padawan")

--------------------------------
How to create a new Domain Model
--------------------------------



---------------------------------------------
How to create a new Service with Unit Testing
---------------------------------------------


---------------------------------------------
E2E protractor testing
---------------------------------------------

pre-requisite:

1. Run server 'gulp server'
2. Create an account called e2e

To run test:

1. From the command line 'cd videa-hub-js/frontend'
2. run 'gulp e2e' or 'protractor e2e.conf.js'

To change the page you want to test:

1. Open e2e.conf.js
2. change the 'specs' field to the path of your test `(i.e. 'client/test/e2e/appConfiguration/analyticsView/*.spec.js')``

NOTE: all e2e tests are located in client/test/e2e/ path

---------------------------------------------
App Studio API Design
---------------------------------------------

Schema DTO

    id: "sdfskdfksdjf"

Screen Type DTO

    name: string
    description: string
    image: ImageDTO

Screen DTO

    name: string
    items: Component[]

DetailesScreen DTO (extends Screen DTO)

    contentType


Get Screen Types

    Method: GET
    Endpoint: /account/{account_id}/appType/{type_id}/screens
    Return: ScreenType DTO []

Get Screen Types

Create Template Screen

    Method: Post
    Endpoint: /account/{account_id}/config/{config_id}/screen


Get Component List

Save Screen

    Method: Put
    Endpoint: /account/{account_id}/screen/{screen_id}
    Body: ScreenDTO

Get Component Properties

    Method: GET
    Endpoint: /account/{account_id}/schema/xxx.yyy.zzz
    Response: SchemaDTO

Get Object Schema

    Method: GET
    Endpoint:


---------------------------------------------
Cloudinary Accounts
---------------------------------------------

Development

    email: guilherme.santos@digiflare.com
    password: videa123#

Staging

    email: nathan.ng@digiflare.com
    password: D1g1FlarE2015
