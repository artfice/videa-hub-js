"use strict";

const gulp = require('gulp');
const mocha = require('gulp-mocha');
const zip = require('gulp-zip');
const bump = require('gulp-bump');
const jeditor = require("gulp-json-editor");
const shell = require('gulp-shell');
const ts = require('gulp-typescript');
const sourcemaps = require('gulp-sourcemaps');
const gls = require('gulp-live-server');
const template = require('gulp-template');
const data = require('gulp-data');
const clean = require('gulp-clean');
const fs = require('fs');
const _ = require('underscore');
const fileNames = require('gulp-filenames');
const request = require('supertest');
const runSequence = require('run-sequence');
const moment = require('moment');
const tslint = require('gulp-tslint');
const istanbul = require('gulp-istanbul');

function yamlIndent(text, indentation) {
    let indent = "\n" + Array(indentation + 1).join(" ");
    return text.split('\n').join(indent);
}

let typescriptBackendSrc = [
    './app/node_modules/videa/appstudio/**/*.ts',
    './app/node_modules/videa/cms/**/*.ts',
    './app/node_modules/videa/core/**/*.ts',
    './app/node_modules/videa/dto/**/*.ts',
    './app/node_modules/videa/factory/**/*.ts',
    './app/node_modules/videa/schema/**/*.ts',
    './app/node_modules/videa/services/**/*.ts',
    './app/node_modules/videa/utils/**/*.ts',
    './app/node_modules/videa/test/*.ts',
    './app/node_modules/videa/test/api/**/*.ts',
    './app/node_modules/videa/test/appstudio/**/*.ts',
    './app/node_modules/videa/test/core/**/*.ts'
]

let paths = {
    clientTest: ['app/public/test/**/*.test.js'],
    typescriptFrameworkSrc: './app/framework/**/*.ts',
    typescriptBackendSrc: './app/node_modules/videa/**/*.ts',
    typescriptCMSSrc: './app/node_modules/videa/cms/**/*.ts',
    typescriptDtoSrc: './app/node_modules/videa/dto/**/*.ts',
    typescriptDefaultDataSrc: './app/node_modules/videa-default-data/**/*.ts',
    jsFrameworkSrc: './app/framework/**/*.js',
    jsBackendSrc: './app/node_modules/videa/**/*.js',
    jsDefaultDataSrc: './app/node_modules/videa-default-data/**/*.js',
    mapFrameworkSrc: './app/framework/**/*.map',
    mapBackendSrc: './app/node_modules/videa/**/*.map',
    mapDefaultDataSrc: './app/node_modules/videa-default-data/**/*.js'
};

function onError(err) {
    console.log(err.toString());
    process.exit(1);
}

const tslintOptions = {
    configuration: 'tslint.json',
    formatter: 'prose',
    rulesDirectory: 'node_modules/tslint-microsoft-contrib'
};

gulp.task('tslint', () =>
    gulp.src('./app/**/*.ts')
    .pipe(tslint(tslintOptions))
    .pipe(tslint.report({
        summarizeFailureOutput: true
    }))
);

gulp.task('tslint:cms', () =>
    gulp.src('./app/**/cms/lambdas/**/AddImage.ts')
    .pipe(tslint(tslintOptions))
    .pipe(tslint.report({
        summarizeFailureOutput: true
    }))
);

gulp.task('test:remove', shell.task([
    'rm -rf app/node_modules/videa/test/appstudio/**/*.js',
    'rm -rf app/node_modules/videa/test/cms/**/*.js',
    'rm -rf app/node_modules/videa/test/core/**/*.js'
]));

gulp.task('test:translation', ['pre-test', 'node_env:test'], function () {
    return gulp.src('app/node_modules/videa/test/appstudio/**/*Test.js', {
            read: false
        })
        .pipe(mocha({
            require: ['./app/config/index.js'],
            globals: {
                recursive: true,
                timeout: 15000
            },
            reporter: 'mochawesome'
        }))
        .pipe(istanbul.writeReports())
        .on("error", onError);
});

gulp.task('test:translation:fixture', ['node_env:test'], function () {
    return gulp.src('app/node_modules/videa/test/**/*.test.js', {
            read: false
        })
        .pipe(mocha({
            require: ['./app/config/index.js'],
            globals: {
                recursive: true,
                timeout: 15000
            },
            reporter: 'spec'
        }))
        .on("error", onError);
});

gulp.task('test:server', function () {
    let server = gls('bin/www', {
        env: {
            NODE_ENV: 'test'
        }
    }, false);
    server.start();
});

gulp.task('test:core', ['pre-test', 'node_env:test'], function () {
    return gulp.src('app/node_modules/videa/test/core/**/*.js', {
            read: false
        })
        .pipe(mocha({
            require: ['./app/config/index.js'],
            globals: {
                recursive: true,
                timeout: 5000
            },
            reporter: 'mochawesome'
        }))
        .pipe(istanbul.writeReports())
        .on("error", onError);
});

gulp.task('test:end', function () {
    process.exit();
});


gulp.task('test:cms:api', ['node_env:test'], function () {
    return gulp.src('app/node_modules/videa/cms/test/api/**/*.js', {
            read: false
        })
        .pipe(mocha({
            require: ['./app/config/index.js'],
            globals: {
                recursive: true,
                timeout: 5000
            },
            reporter: 'mochawesome'
        })).once('end', function () {
            process.exit();
        });
});

gulp.task('test:cms:services', ['node_env:test'], function () {
    return gulp.src('app/node_modules/videa/cms/test/services/**/*.js', {
            read: false
        })
        .pipe(mocha({
            require: ['./app/config/index.js'],
            globals: {
                recursive: true,
                timeout: 5000
            },
            reporter: 'mochawesome'
        }))
        .on("error", onError);
});

gulp.task('test:cms:repository', ['node_env:test'], function () {
    return gulp.src('app/node_modules/videa/cms/test/repository/**/*.js', {
            read: false
        })
        .pipe(mocha({
            require: ['./app/config/index.js'],
            globals: {
                recursive: true,
                timeout: 5000
            }
            // , reporter: 'dot'
        })).once('end', function () {
            process.exit();
        });
});

gulp.task('test:cms:helper', ['node_env:test'], function () {
    return gulp.src('app/node_modules/videa/cms/test/helper/**/*.js', {
            read: false
        })
        .pipe(mocha({
            require: ['./app/config/index.js'],
            globals: {
                recursive: true,
                timeout: 5000
            }
            // , reporter: 'dot'
        })).once('end', function () {
            process.exit();
        });
});

gulp.task('test:schema', ['node_env:test'], function () {
    return gulp.src('app/node_modules/videa/test/services/schema/SchemaServiceTest.js', {
            read: false
        })
        .pipe(mocha({
            require: ['./app/config/index.js'],
            globals: {
                recursive: true,
                timeout: 5000
            },
            reporter: 'mochawesome'
        })).once('end', function () {
            process.exit();
        });
});

gulp.task('test:api', function () {
    return gulp.src('app/node_modules/videa/test/api/*.js', {
            read: false
        })
        .pipe(mocha({
            require: ['./app/config/index.js'],
            globals: {
                recursive: true,
                timeout: 50000
            },
            reporter: 'mochawesome'
        })).once('end', function () {
            process.exit();
        });
});

gulp.task('test:all', function () {
    runSequence(
        'test:core',
        //                'test:translation:fixture',
        'test:translation',
        'test:cms:services',
        'test:end'
    );
});

gulp.task('node_env:test', function () {
    return process.env.NODE_ENV = 'test';
});


gulp.task('generate-config', function () {
    return gulp.src('config-templates/ebextensions/01-nginx-proxy.config')
        .pipe(data(function () {
            return {
                "certificateChain": yamlIndent(fs.readFileSync('certificates/default-ssl.crt', 'utf8'), 6),
                "certificateKey": yamlIndent(fs.readFileSync('certificates/default-ssl.pem', 'utf8'), 6)
            };
        }))
        .pipe(template())
        .pipe(gulp.dest('.ebextensions'));
});

gulp.task('server', function () {
    let server = gls('bin/www', {
        env: {
            NODE_ENV: 'development'
        }
    }, false);
    server.start();
});

gulp.task('compress', function () {
    return gulp.src(['.ebextensions/**',
            'app/**',
            'frontend/**',
            'bin/**',
            '.dockerignore',
            'build.json',
            'Dockerfile',
            'Dockerrun.aws.json',
            'main.js',
            'package.json',
            'README.txt'
        ], {
            base: "."
        })
        .pipe(zip('V' + moment().format('YYYYMMDDHHmmss') + '.zip'))
        .pipe(gulp.dest('dist'));
});

gulp.task('version:patch', function () {
    gulp.src(['package.json', 'frontend/bower.json'])
        .pipe(bump({
            type: 'patch'
        }))
        .pipe(gulp.dest('./'));
});

gulp.task('version:minor', function () {
    gulp.src(['package.json', 'frontend/bower.json'])
        .pipe(bump({
            type: 'minor'
        }))
        .pipe(gulp.dest('./'));
});

gulp.task('version:major', function () {
    gulp.src(['package.json', 'frontend/bower.json'])
        .pipe(bump({
            type: 'major'
        }))
        .pipe(gulp.dest('./'));
});

gulp.task('inc-build', function () {
    return gulp.src("./build.json")
        .pipe(jeditor(function (json) {
            json.build = Number(json.build) + 1;
            return json;
        }))
        .pipe(gulp.dest("./"));
});


gulp.task('get-build-num', function () {
    var fileContent = fs.readFileSync("../nextBuildNumber", "utf8");
    return gulp.src("./package.json")
        .pipe(jeditor(function (json) {
            json.build = Number(fileContent);
            json.build = Number(json.build) - 1;
            return json;
        }))
        .pipe(gulp.dest("./"));
});

gulp.task('typescript:framework', function () {
    return gulp.src(paths.typescriptFrameworkSrc, {
            base: "."
        })
        .pipe(sourcemaps.init())
        .pipe(ts({
            noExternalResolve: false,
            noImplicitAny: false,
            noLib: false,
            module: 'commonjs',
            target: 'ES5',
            removeComments: true,
            moduleResolution: "classic"
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('.'));
});

gulp.task('typescript:backend', function () {
    return gulp.src(typescriptBackendSrc, {
            base: "."
        })
        .pipe(sourcemaps.init())
        .pipe(ts({
            noExternalResolve: false,
            noImplicitAny: false,
            noLib: false,
            module: 'commonjs',
            target: 'ES5',
            removeComments: true,
            moduleResolution: "classic"
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('.'));
});

gulp.task('typescript:cms', function () {
    return gulp.src(paths.typescriptCMSSrc, {
            base: "."
        })
        .pipe(sourcemaps.init())
        .pipe(ts({
            noExternalResolve: false,
            noImplicitAny: false,
            noLib: false,
            module: 'commonjs',
            target: 'ES5',
            removeComments: true,
            moduleResolution: "classic"
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('.'));
});

gulp.task('typescript:dto', function () {
    return gulp.src(paths.typescriptDtoSrc, {
            base: "."
        })
        .pipe(sourcemaps.init())
        .pipe(ts({
            noExternalResolve: false,
            noImplicitAny: false,
            noLib: false,
            module: 'commonjs',
            target: 'ES5',
            removeComments: true,
            moduleResolution: "classic"
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('.'));
});

gulp.task('typescript:defaultdata', function () {
    return gulp.src(paths.typescriptDefaultDataSrc, {
            base: "."
        })
        .pipe(sourcemaps.init())
        .pipe(ts({
            noExternalResolve: false,
            noImplicitAny: false,
            noLib: false,
            module: 'commonjs',
            target: 'ES5',
            removeComments: true,
            moduleResolution: "classic"
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('.'));
});

gulp.task('typescript:all', ['typescript:framework', 'typescript:backend', 'typescript:defaultdata']);

gulp.task('typescript:clean', function () {
    return gulp.src([paths.mapFrameworkSrc, paths.mapBackendSrc, paths.mapDefaultDataSrc, paths.jsFrameworkSrc, paths.jsBackendSrc, paths.jsDefaultDataSrc], {
            base: ".",
            read: false
        })
        .pipe(clean());
});

gulp.task('typescript:clean-with-ts', function () {
    return gulp.src([paths.typescriptFrameworkSrc, paths.typescriptBackendSrc, paths.typescriptDefaultDataSrc], {
        base: "."
    }).pipe(fileNames('removeFiles')).on('end', function () {
        let filesToRemove = [];
        let files = fileNames.get('removeFiles');
        _.each(files, function (fileName) {
            let cleanFileName = fileName.replace('.ts', '.');
            filesToRemove.push(cleanFileName + 'map');
            filesToRemove.push(cleanFileName + 'js');
        });
        console.log("# FILES TO REMOVE: ", filesToRemove.length);
        return gulp.src(filesToRemove).pipe(clean());
    });
});

gulp.task('typescript:recompile', function () {
    runSequence('typescript:clean',
        'typescript:all');
});

gulp.task('typescript:recompile-all', ['typescript:clean-with-ts', 'typescript:all']);

gulp.task('frontend-prod', shell.task([
    'npm run-script frontend-prod'
]));

gulp.task('frontend-version:patch', shell.task([
    'npm run-script frontend-version-patch'
]));

gulp.task('backend:dev', ['typescript:framework', 'typescript:backend']);

gulp.task('publish', ['generate-config', 'version:patch', 'inc-build', 'typescript:all', 'frontend-prod', 'compress']);

gulp.task('republish', ['generate-config', 'typescript:all', 'frontend-prod', 'compress']);

gulp.task('pull', shell.task([
    'sudo npm install',
    'sudo gulp backend:dev',
    'sudo gulp typescript:defaultdata'
]));

gulp.task('docker:build', shell.task([
    'docker build -t videa-hub .'
]));

gulp.task('docker:build-nocache', shell.task([
    'docker build --no-cache -t videa-hub .'
]));

gulp.task('docker:run', shell.task([
    'docker run --net=host --env NODE_ENV=localdocker videa-hub'
]));

gulp.task('docker:run:elasticsearch', shell.task([
    'docker run --net=host -d elasticsearch'
]));

gulp.task('docker:run:mongo', shell.task([
    'docker run --net=host -d mongo'
]));

gulp.task('docker:delete-all-containers', shell.task([
    'docker rm $(docker ps -a -q)'
]));

gulp.task('docker:delete-all-images', shell.task([
    'docker rmi $(docker images -q)'
]));

gulp.task('start', shell.task([
    'make run'
]));

gulp.task('first-time', shell.task([
    './setup.sh'
]));

gulp.task('pre-test', function () {
    return gulp.src(['app/node_modules/videa/services/translation/**/*.js',
            'app/node_modules/videa/services/mongodb/*.js'
        ])
        // Covering files
        .pipe(istanbul())
        // Force `require` to return covered files
        .pipe(istanbul.hookRequire());
});

gulp.task('code-test', ['pre-test'], function () {
    return gulp.src(['app/node_modules/videa/test/appstudio/translation/*.js'])
        .pipe(mocha())
        // Creating the reports after tests ran
        .pipe(istanbul.writeReports())
        // Enforce a coverage of at least 90%
        .pipe(istanbul.enforceThresholds({
            thresholds: {
                global: 90
            }
        }));
});

gulp.task('licenses', shell.task([
    'npm run-script licenses'
]));
