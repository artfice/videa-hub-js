"use strict";

module.exports = function (grunt) {

    grunt.registerTask('inc-build', 'Increments the build number and updates the build number file.', function() {

        var filePath = 'build.json';
        var buildNumber = grunt.file.readJSON(filePath);
        buildNumber.build++;

        grunt.file.write(filePath, JSON.stringify(buildNumber, null, 2));
    });
};

