FROM node:4.6.0

RUN npm config set unsafe-perm true

RUN npm install -g bower

# Provides cached layer for node_modules
ADD package.json /tmp/package.json
RUN cd /tmp && npm install --production
RUN mkdir -p /usr/local/videa && cp -a /tmp/node_modules /usr/local/videa/
 
WORKDIR /usr/local/videa

# Bundle app source
COPY app /usr/local/videa/app
COPY bin /usr/local/videa/bin
COPY build.json /usr/local/videa/build.json
COPY package.json /usr/local/videa/package.json
COPY licenses.json /usr/local/videa/licenses.json

EXPOSE  3000
VOLUME ["/var/log"]
CMD ["node", "./bin/www", "-p"]
