TESTS = test/*.js

test: test-videa

test-videa:

	@NODE_ENV=test NODE_TLS_REJECT_UNAUTHORIZED=0 \
		./node_modules/.bin/mocha \
	    --require ./app/config/index.js \
	    --recursive \
		--timeout 5000 \
		app/node_modules/videa/test/cms/**/*.js \

run:
	@NODE_ENV=development \
	node bin/www

setup:
	@NODE_ENV=development \
	sudo npm cache clean -f \
	sudo npm install -g -n stable \
	sudo npm install . \
	tsd install -s \
	sudo npm install -g bower \
	bower install

docker-build:
	docker build -t videa-hub .

docker-build-no-cache:
	docker build --no-cache -t videa-hub .

docker-run:
	docker run -it --net=host --env NODE_ENV=localdocker videa-hub

docker-delete-all-containers:
	docker rm $(docker ps -a -q)

docker-delete-all-images:
	docker rmi $(docker images -q)

DOCKERZIP = `date +V%Y%m%d%H%M%S.zip`

zip-aws:
	zip -r $(DOCKERZIP) ./app ./bin bower.json Dockerfile Dockerrun.aws.json e2e.conf.js Gruntfile.js karma.conf.js main.js package.json README.txt .dockerignore

.PHONY: setup run test test-videa docker-build docker-build-no-cache docker-run docker-delete-all-containers docker-delete-all-images zip-aws
