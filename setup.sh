ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew update
brew install mongodb
brew install Caskroom/cask/java
brew install elasticsearch
brew install node
sudo npm cache clean -f
sudo npm install -g n
sudo n 4.1.1
sudo gem install sass
sudo npm install -g protractor typescript jasmine-given karma-cli karma grunt-karma phantomjs bower gulp gulp-cli tsd selenium-standalone@latest
sudo npm install
cd app
tsd install -s
ln -sfv /usr/local/opt/elasticsearch/*.plist ~/Library/LaunchAgents
ln -sfv /usr/local/opt/mongodb/*.plist ~/Library/LaunchAgents
sudo selenium-standalone install
sudo npm install
sudo gulp typescript:backend
sudo gulp typescript:defaultdata
cd frontend
bower install
sudo npm install
gulp vendor:dev
gulp server